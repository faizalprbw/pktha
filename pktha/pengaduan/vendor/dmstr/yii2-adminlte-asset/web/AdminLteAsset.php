<?php
namespace dmstr\web;

use yii\base\Exception;
use yii\web\AssetBundle;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class AdminLteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte';
    public $css = [
        'dist/css/AdminLTE.min.css',
        'plugins/datepicker/datepicker3.css'
    ];
    public $js = [
        'dist/js/app.min.js',
        'plugins/datepicker/bootstrap-datepicker.js',        
        'plugins/input-mask/inputmask.js',
        'plugins/input-mask/jquery.inputmask.js',
        'plugins/input-mask/inputmask.numeric.extensions.js',
        
        // 'plugins/input-mask/jquery.inputmask.extensions.js',
        // 'plugins/input-mask/jquery.inputmask.numeric.extensions.js',
        // 'plugins/input-mask/jquery.inputmask.phone.extensions.js',
        // 'plugins/input-mask/jquery.inputmask.regex.extensions.js',

        
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /**
     * @var string|bool Choose skin color, eg. `'skin-blue'` or set `false` to disable skin loading
     * @see https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html#layout
     */
    public $skin = '_all-skins';

    /**
     * @inheritdoc
     */
    public function init()
    {
        // Append skin color file if specified
        if ($this->skin) {
            if (('_all-skins' !== $this->skin) && (strpos($this->skin, 'skin-') !== 0)) {
                throw new Exception('Invalid skin specified');
            }

            $this->css[] = sprintf('dist/css/skins/%s.min.css', $this->skin);
        }

        parent::init();
    }
}
