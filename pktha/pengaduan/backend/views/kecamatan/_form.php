<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Kecamatan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kecamatan-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
    $id_kota_kabupaten = $model->id_kota_kabupaten;
    $kota_kabupaten = common\models\KotaKabupaten::findOne(['id' => $id_kota_kabupaten]);
    $id_provinsi = $kota_kabupaten->id_provinsi;
    ?>

    <div class="form-group">
        <label class="control-label">Provinsi</label>
        <?= Html::dropDownList('id_provinsi', $id_provinsi, yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), ['class' => 'form-control', 'id' => 'select-provinsi']) ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'id_kota_kabupaten')->label('Kota / Kabupaten')->dropDownList(yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->all(), 'id', 'nama_kota_kabupaten'), ['id' => 'select-kota-kabupaten']) ?>

    <?= $form->field($model, 'nama_kecamatan')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'longitude')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
