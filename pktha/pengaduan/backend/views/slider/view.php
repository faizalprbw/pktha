<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\slider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        echo Html::a(
                'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
                ]
        )
        ?>
        <?php echo Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-warning']); ?>
    </p>

    <?php
    echo DetailView::widget(
            [
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'desc:ntext',
                        [
                        'attribute' => 'created_at',
                        'value' => date("Y-m-d H:i:s", $model->created_at),
                    ],
                        [
                        'attribute' => 'updated_at',
                        'value' => date("Y-m-d H:i:s", $model->updated_at),
                    ],
                    'img',
                ],
            ]
    )
    ?>
    <img src="<?php echo Yii::getAlias('@web') . '/../../uploads/slider/' . $model->img; ?>" alt="image" class="img-responsive" />
</div>
