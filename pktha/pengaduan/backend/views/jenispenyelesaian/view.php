<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Jenispenyelesaian */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Jenis penyelesaian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?=
                Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>
        </div>
    </div>
    <div class="jenispenyelesaian-view box-body">  
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'nama',
                'keterangan',
                'created_at:datetime',
                'updated_at:datetime',
                'user_id',
            ],
        ])
        ?>
    </div>
</div>
