<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jenispenyelesaian */

$this->title = 'Create Jenis penyelesaian';
$this->params['breadcrumbs'][] = ['label' => 'Jenis penyelesaian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenispenyelesaian-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
