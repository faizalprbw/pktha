<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengaduanPraMediasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengaduan Pra Mediasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-pra-mediasi-index">
    <?php Pjax::begin(); ?>    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Kode Pengaduan',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::find()->getById($model->id_pengaduan)->kode_pengaduan;
                }
            ],
            [
                'label' => 'Nama Pengadu',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan])->nama_pengadu;
                }
            ],
            'data_para_pihak:ntext',
            'pokok_permasalahan:ntext',
            'objek_mediasi:ntext',
            // 'created_date',
            // 'modified_date',
            [
                'label' => 'Status',
                'format' => 'html',
                'value' => function($model, $index, $widget) {
                    return $model->finished ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
