<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanPraMediasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-pra-mediasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'data_para_pihak')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pokok_permasalahan')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'objek_mediasi')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
