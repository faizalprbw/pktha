<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<?php
Yii::$app->view->registerCSSFile('css/map/leaflet.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.ZoomBar.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.Locate.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet.groupedlayercontrol.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.BetterScale.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/iconLayers.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.Default.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet_geosearch.css', ['position' => yii\web\View::POS_HEAD]);

Yii::$app->view->registerJsFile('js/map/leaflet-src.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.ZoomBar.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.Locate.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-color-markers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.groupedlayercontrol.min.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.BetterScale.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/iconLayers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-providers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.markercluster.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet_renderer.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/jquery.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/feature_group.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet_geosearch2.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-fill-Pattern.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/panggil_peta_backend.js', ['position' => yii\web\View::POS_END]);
Yii::$app->view->registerJs('var link_lokasi_pengaduan = ' . json_encode(yii\helpers\Url::to(['/site/lokasipengaduan'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_pencantuman_hutan_adat = ' . json_encode(yii\helpers\Url::to(['/site/pencantumanhutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_potensi_hutan_adat = ' . json_encode(yii\helpers\Url::to(['/site/potensihutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2016 = ' . json_encode(yii\helpers\Url::to(['/site/kasus2016'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2017 = ' . json_encode(yii\helpers\Url::to(['/site/kasus2017'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_provinsi = ' . json_encode(yii\helpers\Url::to(['/site/produkhukumprovinsi'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_kabupaten = ' . json_encode(yii\helpers\Url::to(['/site/produkhukumkabupaten'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_pencantuman = ' . $jumlah_pencantuman . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_potensi = ' . $jumlah_potensi . ';', yii\web\View::POS_HEAD);
$url = \yii\helpers\Url::toRoute(['site/peta', true]);
?>


<style type="text/css">
    #map-canvas { height: 340px; }
</style>

<div class="site-index row">
    <section class="content">
        <h4>Pengaduan Konflik</h4>
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#pengaduanTersisa">Progress Kasus</a></li>
        </ul>
        <div class="tab-content">
            <div id="pengaduanTersisa" class="tab-pane fade in active">
                <div class="row">
                    <?php
                    foreach ($tahapPengaduan as $key => $val) {
                        if ($key != 'Registrasi' && $key != 'Dialihkan' && $key != 'Tanda Tangan MoU') {
                            $state_name = ($key == 'Selesai') ? 'Tanda Tangan MoU' : $key;
                            echo '<div class="col-lg-2 col-xs-6" style="width: 20%">
                            <!-- small box -->
                            <div class="small-box bg-' . $val['warna'] . '">
                                <div class="inner">
                                <h3>' . $val['count'] . '</h3>
                                <p>' . $state_name . '</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>' . Html::a(
                                    'More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute(['/pengaduan-registrasi/index', 'tahap' => $key]), ['class' => "small-box-footer"]
                            ) . '
                            </div>
                        </div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Total Pengaduan</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4><?php echo $tahapPengaduan['Registrasi']['count'] ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Dialihkan</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4><?php echo $tahapPengaduan['Dialihkan']['count'] ?></h4>
            </div>
        </div>
    </section>

    <section class="content">
        <h4>Sistem Registrasi Nasional Hutan Adat</h4>
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#pengusulanTersisa">Progress Pengusulan</a></li>
        </ul>
        <div class="tab-content">
            <div id="pengusulanTersisa" class="tab-pane fade in active">
                <div class="row">
                    <?php
                    foreach ($tahapPengusulan as $key => $val) {
                        if ($key != 'Registrasi' && $key != 'Dikembalikan' && $key != 'Selesai') {
                            echo'<div class="col-lg-2 col-xs-6" style="width: 20%">
                            <!-- small box -->
                            <div class="small-box bg-' . $val['warna'] . '">
                                <div class="inner">
                                <h3>' . $val['count'] . '</h3>
                                <p>' . $key . '</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>' . Html::a(
                                    'More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute(
                                            [
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            ) . '
                            </div>
                        </div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Total Pengusulan</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4><?php echo $tahapPengusulan['Registrasi']['count'] ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Dikembalikan</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4><?php echo $tahapPengusulan['Dikembalikan']['count'] ?></h4>
            </div>
        </div>
    </section>


    <section class="content">
        <h4>Sistem Registrasi Nasional Masyarakat Hukum Adat</h4>
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#pengusulanTersisa">Proses Administrasi</a></li>
        </ul>
        <div class="tab-content">
            <div id="pengusulanTersisa" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Registrasi</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-purple">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Verifikasi</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Validasi</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-orange">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Pencantuman dan Penetapan</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Monev dan Pemberdayaan</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-6" style="width: 20%">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>0</h3>
                                <p>Selesai</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div><?php
                            Html::a('More Info <i class="fa fa-arrow-circle-right"></i>', Url::toRoute([
                                                '/pengusulan-registrasi/index',
                                                'tahap' => $key,
                                            ]
                                    ), ['class' => "small-box-footer"]
                            )
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Total Masyarakat Hukum Adat</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4>0</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <h4>Berkas Harus Diperbaiki</h4>
            </div>
            <div class="col-sm-2 col-md-2">
                <h4>0</h4>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">

                <div class="box box-solid bg-green-gradient">
                    <div class="box-header">
                        <i class="fa fa-map"></i>
                        <h3 class="box-title">MAP</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <!-- button with a dropdown -->

                            <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!--The calendar -->
                        <div id="calendar" style="width: 100%"></div>
                    </div><!-- /.box-body -->
                    <div class="map_wrapper">
                        <div id="map" style="width:100%;height:500px;background:white"></div>
                    </div>

                </div><!-- /.col -->
            </section>
        </div>
    </section>

</div><!-- /.box -->
