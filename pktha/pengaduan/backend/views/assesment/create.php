<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Assesment */

$this->title = 'Create Verifikasi Kelengkapan Data dan Penapisan Awal';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assesment-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'idx' => $idx
    ])
    ?>
</div>
