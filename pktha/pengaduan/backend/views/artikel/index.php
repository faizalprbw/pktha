<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Artikel';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?php echo $this->title; ?></h3>
        <div class="box-tools pull-right">
            <p>
                <?php echo Html::a('Tambah Artikel', ['create'], ['class' => 'btn btn-success']) ?>
                <?php echo Html::a('Artikel Recycle bin', ['recyclebin'], ['class' => 'btn btn-danger']) ?>
            </p>
        </div>
    </div>

    <div class="artikel-index box-body">
        <?php echo
        GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'judul',
                    [
                    'attribute' => 'headline',
                    'format' => 'text',
                    'label' => 'Headline',
                    'value' => function ($dataProvider) {
                        return yii\helpers\BaseStringHelper::truncateWords($dataProvider->headline, 10, null, true);
                    },
                    ],
                    [
                    'attribute' => 'konten',
                    'format' => 'text',
                    'label' => 'Isi',
                    'value' => function ($dataProvider) {
                        return yii\helpers\BaseStringHelper::truncateWords($dataProvider->konten, 10, null, true);
                    },
                    ],
                    // 'image',
                    [
                    'attribute' => 'publish',
                    'format' => 'text',
                    'label' => 'Published',
                    'value' => function ($model) {
                        return $model->publish == 1 ? 'True' : 'False';
                    }
                    ],
                    [
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                    ],
                    // 'created_at',
                    // 'updated_at',
                    [
                    'class' => 'yii\grid\ActionColumn',
                    // 'template' => '{publish} {view} {update} {delete}',
                    'template' => \Yii::$app->user->identity->role >= 30 ? Helper::filterActionColumn('{publish} {view} {update} {delete}') : Helper::filterActionColumn('{view} {update} {delete}'),
                    'contentOptions' => ['style' => 'min-width: 95px;'],
                    'buttons' => [
                        'publish' => function ($url) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-upload"></span> &nbsp;', $url, [
                                        'title' => 'Publish',
                                        'data-pjax' => '0',
                                            ]
                            );
                        }
                    ]
                    ],
                ],
                ]
        );
        ?>
    </div>
</div>
