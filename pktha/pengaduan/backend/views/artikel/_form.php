<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Artikel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="artikel-form box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <?php //$form->field($model, 'kategori_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Kategori::find()->all(),'id','kategori')); ?>
        <?= $form->field($model, 'kategori_id')->dropDownList(['10' => 'Kegiatan', '15' => 'Cerita Sukses', '23' => 'Peraturan dan Perundangan', '35' => 'Buku', '43' => 'Peta']); ?>
        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'headline')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'konten')->widget(CKEditor::className(), [
            'editorOptions' => ['height' => '300px']
        ])
        ?>
        <label id="filetx" class="control-label" for="artikel-imagefile">Image File</label>
        <?= $form->field($model, 'imageFile')->fileInput()->label(false) ?>
        <?= $form->field($model, 'caption')->textInput() ?>
        <?php
        //Yii::$app->user->identity->username
        //var_dump(\Yii::$app);
        ?>
        
        <!-- 
            Check if current user is Admin Pusat
        -->
        <?php 
            $role = \Yii::$app->user->identity->role;
            if ($role >= 30) {
                echo $form->field($model, 'publish')->dropDownList(array('1' => 'Publish', '0' => 'Unpublished'));
            }
        ?>
        
        <!-- <?= $form->field($model, 'publish')->dropDownList(array('1' => 'Publish', '0' => 'Unpublished')) ?> -->
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cancel', yii\web\Request::getReferrer(), ['class' => 'btn btn-danger']); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php
$js = <<< JS
    
    $( "#artikel-kategori_id" ).change(function() {
        var id = $(this).val();
        if(id == 23){
            $('#filetx').html('PDF File');
        }else{
            $('#filetx').html('Image File');
        }


        // alert( "Handler for .change() called." + id);
    });
JS;

$this->registerJs($js, yii\web\View::POS_READY);
?>