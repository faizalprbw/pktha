<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tahapan */

$this->title = 'Update Tahapan: ' . ' ' . $model->tahapan;
$this->params['breadcrumbs'][] = ['label' => 'Tahapan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tahapan, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tahapan-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
