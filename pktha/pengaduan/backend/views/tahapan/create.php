<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tahapan */

$this->title = 'Create Tahapan';
$this->params['breadcrumbs'][] = ['label' => 'Tahapan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tahapan-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
