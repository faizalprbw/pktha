<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tahapan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?=$this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <?= Html::a('Create Tahapan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>

<div class="tahapan-index box-body">



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'tahapan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>