<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Provinsi */

$this->title = 'Update Provinsi: ' . ' ' . $model->nama_provinsi;
$this->params['breadcrumbs'][] = ['label' => 'Provinsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_provinsi, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>          
        <div class="box-tools pull-right">    
        </div>
    </div>

    <div class="provinsi-update box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
