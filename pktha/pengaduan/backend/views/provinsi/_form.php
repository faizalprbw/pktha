<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Provinsi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provinsi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_wilayah')->dropDownList(yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah')) ?>

    <?= $form->field($model, 'nama_provinsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['type' => 'numeric']) ?>

    <?= $form->field($model, 'longitude')->textInput(['type' => 'numeric']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', yii\web\Request::getReferrer(), ['class' => 'btn btn-warning']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
