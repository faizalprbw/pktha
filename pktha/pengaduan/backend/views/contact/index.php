<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?php echo $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <!-- <?php echo Html::a('Tambah Contact', ['create'], ['class' => 'btn btn-success']) ?> -->
            </p>
        </div>
    </div>
    <div class="contact-index box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?php echo
        GridView::widget(
            [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                            'nama:ntext',
                    'email:ntext',
                    'prihal:ntext',
                    'isi:ntext',
                    // 'created_at',
                    // 'updated_at',
                    // ['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'min-width: 70px;'],
                    ],
                ],
            ]
        );
        ?>
    </div>
