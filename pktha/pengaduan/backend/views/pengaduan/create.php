<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = 'Create Pengaduan';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    
<!-- /.box-header -->

<div class="pengaduan-create box-body">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
