<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengaduan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-index">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">List Pengaduan</h3>          
            <div class="box-tools pull-right">
                <p>

                    <?= Html::a('<i class="fa fa-file-excel-o"></i> Export Excel', ['downloadxls'], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('<i class="fa fa-file-excel-o"></i> Import Excel', ['importxls/importxls'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Create Pengaduan', ['create'], ['class' => 'btn btn-success']) ?>
                    <?= Html::a('<i class="fa fa-trash-o"></i> Pengaduan Recycle Bin', ['recyclebin'], ['class' => 'btn btn-danger']) ?>
                </p>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'kode',
                    'pihak_berkonflik',
                        [
                        'attribute' => 'tutuntan_pengaduan',
                        'label' => 'Tuntutan Pengaduan',
                    ],
                        [
                        'attribute' => 'id_tahapan',
                        'value' => 'tahapan.tahapan',
                        'label' => 'Tahapan',
                        'filter' => array(1 => "Registrasi", 2 => "Assesment", 3 => "Analisa Permasalahan", 4 => "Rekomendasi"),
                    ],
                        [
                        'attribute' => 'created_at',
                        'label' => 'Created',
                        'value' => 'created_at',
                        'filter' => false,
                        'format' => ['date', 'php:d-m-Y h:i:s'],
                        'contentOptions' => ['style' => 'min-width: 90px;'],
                    ],
                        [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{replay}{timeline}{delete}',
                        'contentOptions' => ['style' => 'min-width: 90px;'],
                        'buttons' => [
                            'timeline' => function ($url) {
                                return Html::a(
                                                '<span class="glyphicon glyphicon-inbox"></span> &nbsp;&nbsp;&nbsp;', $url, [
                                            'title' => 'Pengaduan',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                            'replay' => function ($model, $key, $index) {
                                $url = Url::to(['replay/index', 'id' => $index]);
                                return Html::a(
                                                '<span class="glyphicon glyphicon-share-alt"></span> &nbsp;&nbsp;&nbsp;', $url, [
                                            'title' => 'Reply',
                                            'data-pjax' => '0',
                                                ]
                                );
                            }
                        ]
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
