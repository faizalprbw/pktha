<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
Yii::$app->view->registerJsFile('http://maps.googleapis.com/maps/api/js?v=3.exp&callback=initMap',['position' => yii\web\View::POS_END]);
?>
<style type="text/css">
    #map-canvas {
        text-align: center;
        width:570px;
        height:480px;
    }
</style>

<script type="text/javascript">
    var map;
    var markers = [];

    function initMap() {
      var jakarta = {lat: -6.218484, lng: 106.835354};
      map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: jakarta,
        zoom: 8
    });


      map.addListener('click', function(event) {
        clearMarkers();

    // alert("uhuuyyyy");

    var myLatLng = event.latLng;
    var lat = myLatLng.lat();
    var lng = myLatLng.lng();

    var ll = lat;
    var lo = lng;

    $("#pengaduan-lot_perkiraaan_lokasi").val(ll);
    $("#pengaduan-lang_perkiraan_lokasi").val(lo);

    addMarker(event.latLng);
});  

      function clearMarkers() {
        setMapOnAll(null);
    }

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
}
}

  // Adds a marker to the map and push to the array.
  function addMarker(location) {
    var marker = new google.maps.Marker({
      position: location,
      map: map
  });
    markers.push(marker);
}  


}

</script>




<div class="pengaduan-form">
    <div class="row">
    <?php 
    $form = ActiveForm::begin(); 
    // $model->kode =$model->getKode(1);
    ?>

    <div class="col-md-6">


    <?= $form->field($model, 'sarana_pengaduan')->dropDownList(array('1' => 'Website','2' => 'Surat','3' => 'Loket','4' => 'Sms/Email'),[
                'prompt'=>'-- sarana pengaduan--',
                'onchange'=>
                '$.get("' . yii::$app->urlManager->createUrl('pengaduan/getkode') . '",
                { id: $(this).val() }
                ).done(function( data ) {
                    console.log(data);
                    $("#pengaduan-kode").val(data);
                })',

    ])  ?>

    <?= $form->field($model, 'kode')->textInput(['disabled'=>true,'maxlength' => true]) ?>



    <?= $form->field($model, 'id_tahapan')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Tahapan::find()->all(),'id','tahapan'))->label('Tahapan'); ?>

    <?= $form->field($model, 'status')->dropDownList(array('0' => 'Tidak Layak','1' => 'Layak'))  ?>

    <?= $form->field($model, 'tipe_identitas')->dropDownList(array('1' => 'Kementerian/Lembaga','2' => 'Individu','3' => 'Perusahaan','4' => 'Kelompok/Organisasi Masyaraka'))  ?>

    <?= $form->field($model, 'nama_identitas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telepon_identitas')->textInput(['maxlength' => true,'type'=>'tel']) ?>

    <?= $form->field($model, 'email_identitas')->textInput(['maxlength' => true,'type'=>'email']) ?>

    <?= $form->field($model, 'alamat_identitas')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nomor_surat')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-md-6">

    <?= $form->field($model,'tgl_surat')->widget(yii\jui\DatePicker::className('form-control'),['options' => ['class' => 'form-control'],'language' => 'id','clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>

    <?= $form->field($model,'tgl_penerima_pengaduan')->widget(yii\jui\DatePicker::className('form-control'),['options' => ['class' => 'form-control'],'language' => 'id','clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>                                  

    <?= $form->field($model,'tgl_penerima_pktha')->widget(yii\jui\DatePicker::className('form-control'),['options' => ['class' => 'form-control'],'language' => 'id','clientOptions' => ['dateFormat' => 'yy-mm-dd']]) ?>                                  





    <?= $form->field($model, 'penerima_pengaduan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pihak_berkonflik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lokasi_konflik')->textInput(['maxlength' => true]) ?>





    <?= $form->field($model, 'provinsi_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(),'proid','provinsi'),
        [
        'prompt'=>'--Provinsi--',

        'onchange'=>'
        $.get(
            "' . yii::$app->urlManager->createUrl('pengaduan/test') . '",
            { id: $(this).val() }
            )
    .done(function( data ) {
        console.log(data);
        $("select#pengaduan-kabkota_konflik").html(data);
    })',
    ]) ?>

    <?= $form->field($model, 'kabkota_konflik')->dropDownList($model->getKabkotList($model->provinsi_konflik),['prompt'=>'--Pilih Kabupaten / Kota--',]) ?>

    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myMapModal">
        Buka Map
    </button>
    
    <?= $form->field($model, 'lot_perkiraaan_lokasi')->textInput() ?>

    <?= $form->field($model, 'lang_perkiraan_lokasi')->textInput() ?>

    <?= $form->field($model, 'fungsi_kawasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'luasan_kawasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipologi_kasus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_kawasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rentang_waktu')->textInput() ?>

    <?= $form->field($model, 'resume')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tutuntan_pengaduan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'upaya')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pihak_terlibat')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


    <!-- Modal -->
    <div class="modal fade" id="myMapModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal title</h4>

                </div>
                <div class="modal-body">
                    
                    <div id="map-canvas" class=""></div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    
    <!-- Modal -->



</div>

