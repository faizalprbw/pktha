<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = "Registrasi Wizard";
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">


    <div class="pengaduan-view box-body">
        <div class="box-header with-border">
          <h3 class="box-title"><?=$this->title;?></h3>          
        </div>
        
<?php
$wizard_config = [
    'id' => 'stepwizard',
    'steps' => [
        1 => [
            'title' => 'Identitas Pendaftar',
            'icon' => 'glyphicon glyphicon-cloud-download',
            'content' => $this->render('_form_identitas', ['model' => $model, 'form' => '']),
            'buttons' => [
                'next' => [
                    'title' => 'Next', 
                    'options' => [
                        'class' => 'btn btn-default '
                    ],
                 ],
             ],
        ],
        2 => [
            'title' => 'Pengaduan',
            'icon' => 'glyphicon glyphicon-cloud-upload',
            'content' => $this->render('_form_pengaduan', ['model' => $model]),
            'skippable' => false,
        ],
        
    ],
    'complete_content' => "You are done!", // Optional final screen
    'start_step' => 1, // Optional, start with a specific step
];
?>

<?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>

        <?php




//         // var_dump($model->tahapan);
//         echo Tabs::widget([
//     'items' => [
//         [
//             'label' => 'Registrasi',
//             'content' => $this->render('_formx', ['model' => $model, 'form' => '']),
//             'options' => ['id' => 'registrasi'],            
//             'active' => true
//         ],        
//         [
//             'label' => 'Assesment Lapangan',
//             'content' => 'Anim pariatur cliche...',
//             'options' => ['id' => 'assesment','class'=>'disabled'],
//         ],
//         [
//             'label' => 'Analisa Permasalahan',
//             'content' => 'Anim pariatur cliche...',
//         ],
//         [
//             'label' => 'Tahap Rekomendasi',
//             'content' => 'Anim pariatur cliche...',
//         ],

//     ],
// ]);

        ?>

  </div>
</div>
