<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = $model->kode;
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['timeline', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Detail'; //$this->title;
$tipe_identitas = array('1' => 'Kementerian/Lembaga', '2' => 'Individu', '3' => 'Perusahaan', '4' => 'Kelompok/Organisasi Masyarakat');
?>
<div class="box box-info">


    <div class="pengaduan-view box-body">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title; ?></h3>          
        </div>

        <?php
        $url = Url::to(['replay/index', 'id' => $model->id]);
        ?>


        <p>

            <?= Html::a('<span class="glyphicon glyphicon-share-alt"></span> &nbsp;&nbsp;&nbsp; Kirim email', $url, ['class' => 'btn btn-success']) ?>


            <?= Html::a('Back', ['timeline', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'kode',
                    ['label' => 'Tipe Identitas',
                    'value' => $tipe_identitas[$model->tipe_identitas],
                ],
                'nama_identitas',
                'telepon_identitas',
                'email_identitas:email',
                'alamat_identitas:ntext',
                'sarana_pengaduan',
                'nomor_surat',
                'tgl_surat',
                'tgl_penerima_pengaduan',
                'tgl_penerima_pktha',
                'penerima_pengaduan',
                'pihak_berkonflik',
                'lokasi_konflik',
                'kabkota_konflik',
                'provinsi_konflik',
                'lot_perkiraaan_lokasi',
                'lang_perkiraan_lokasi',
                'fungsi_kawasan',
                'luasan_kawasan',
                'tipologi_kasus',
                'status_kawasan',
                'rentang_waktu',
                'resume',
                'tutuntan_pengaduan',
                'upaya',
                'pihak_terlibat',
                    [
                    'label' => 'Tahapan',
                    'value' => $model->tahapan->tahapan,
                ],
                    [
                    'label' => 'status',
                    'value' => $model->status == 0 ? 'Layak' : 'Tidak Layak'
                ],
                'created_at:datetime',
                'updated_at:datetime',
            // 'user_id',
            ],
        ])
        ?>
    </div>
</div>
