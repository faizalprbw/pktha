<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanTandaTanganMou */

$this->title = Yii::t('app', 'Create Pengaduan Tanda Tangan Mou');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Tanda Tangan Mous'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-tanda-tangan-mou-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
