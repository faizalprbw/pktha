<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengaduan Assesmen');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-assesmen-index">


    <!-- <p>
    <?= Html::a(Yii::t('app', 'Create Pengaduan Assesmen'), ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i> Export Excel', ['downloadxls'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i> Import Excel', ['importxls/importxls'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Kode Pengaduan',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan])->kode_pengaduan;
                }
            ],
            [
                'label' => 'Nama Pengadu',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan])->nama_pengadu;
                }
            ],
            'potensi_konflik_dan_potensi_damai:ntext',
            // 'sejarah_konflik:ntext',
            // 'akar_konflik:ntext',
            // 'pemicu_konflik:ntext',
            // 'akselerator_konflik:ntext',
            // 'pemetaan_dinamika_aktor:ntext',
            // 'sistem_representasi:ntext',
            // 'tawaran_tertinggi',
            // 'tawaran_terendah',
            // 'rekomendasi:ntext',
            // 'surat_kesediaan_mediasi',
            // 'id',
            // 'created_date',
            // 'modified_date',
            // 'finished:boolean',
            [
                'attribute' => 'finished',
                'label' => 'Status',
                'format' => 'html',
                'value' => function($model, $index, $widget) {
                    return $model->finished ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
