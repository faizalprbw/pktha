<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanAssesmen */

$this->title = \common\models\PengaduanRegistrasi::find()->getById($model->id_pengaduan)->kode_pengaduan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Assesmens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-assesmen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pengaduan',
            'potensi_konflik_dan_potensi_damai:ntext',
            'sejarah_konflik:ntext',
            'akar_konflik:ntext',
            'pemicu_konflik:ntext',
            'akselerator_konflik:ntext',
            'pemetaan_dinamika_aktor:ntext',
            'sistem_representasi:ntext',
            'tawaran_tertinggi',
            'tawaran_terendah',
            'rekomendasi:ntext',
            // 'surat_kesediaan_mediasi',
            // 'id',
            'created_date',
            'modified_date',
            'finished:boolean',
        ],
    ]) ?>

</div>
