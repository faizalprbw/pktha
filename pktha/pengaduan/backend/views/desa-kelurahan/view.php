<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DesaKelurahan */

$this->title = $model->nama_desa_kelurahan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Desa Kelurahan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desa-kelurahan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_kecamatan',
            [
                'attribute' => 'id_kecamatan',
                'label' => 'Kecamatan',
                'value' => \common\models\Kecamatan::findOne(['id' => $model->id_kecamatan])->nama_kecamatan,
            ],
            'nama_desa_kelurahan:ntext',
            'id',
            'latitude',
            'longitude',
        ],
    ]) ?>

</div>
