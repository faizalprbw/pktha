<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanPermohonan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-permohonan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'id_pengusulan')->textInput() ?>

    <?= $form->field($model, 'dikembalikan')->checkbox() ?>

    <?= $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
