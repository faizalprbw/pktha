<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanDraftingMou */

$this->title = Yii::t('app', 'Create Pengaduan Drafting Mou');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Drafting Mous'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-drafting-mou-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
