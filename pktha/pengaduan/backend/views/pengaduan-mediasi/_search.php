<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanMediasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengaduan-mediasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pengaduan') ?>

    <?= $form->field($model, 'data_para_pihak') ?>

    <?= $form->field($model, 'berita_acara') ?>

    <?= $form->field($model, 'foto') ?>

    <?= $form->field($model, 'pokok_permasalahan') ?>

    <?php // echo $form->field($model, 'objek_mediasi') ?>

    <?php // echo $form->field($model, 'surat_usulan_mediator') ?>

    <?php // echo $form->field($model, 'sk_mediator') ?>

    <?php // echo $form->field($model, 'proses_pertemuan') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
