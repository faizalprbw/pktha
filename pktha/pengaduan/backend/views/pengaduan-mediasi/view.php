<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanMediasi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Mediasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-mediasi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pengaduan',
            'data_para_pihak:ntext',
            'berita_acara_data',
            'foto_data',
            'pokok_permasalahan:ntext',
            'objek_mediasi:ntext',
            'surat_usulan_mediator',
            'sk_mediator',
            'proses_pertemuan:ntext',
            'id',
            'created_date',
            'modified_date',
            'finished:boolean',
        ],
    ]) ?>

</div>
