<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'IMPORT XLS';
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">List <?= $this->title; ?></h3>          
        <div class="box-tools pull-right">
            <p>
                <?php
                $url = yii\helpers\Url::to(["listview"]);
                echo Html::a('<i class="fa  fa-file-excel-o "></i> Import XLS', $url, ['class' => "btn btn-success pull-right", 'data' => ['method' => 'post']]);
                ?>
            </p>
        </div>
    </div>
    <div class="jenispenyelesaian-index box-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'nama_identitas',
                // 'tipe_identitas',   
                // 'sarana_pengaduan'  ,
                // 'nomor_surat'  ,
                // 'tgl_penerima_pengaduan',
                // 'tgl_penerima_pktha'  ,
                'penerima_pengaduan',
                'pihak_berkonflik',
                'lokasi_konflik',
            // 'fungsi_kawasan', 
            // 'luasan_kawasan',   
            // 'tipologi_kasus',   
            // 'status_kawasan'    ,
            // 'rentang_waktu',   
            // 'resume'    ,
            // 'tutuntan_pengaduan',  
            // 'upaya' ,
            // 'pihak_terlibat'
            // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
