<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usulan';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?=$this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <!-- <?= Html::a('Tambah Contact', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    </div>
</div>

<div class="contact-index box-body">



 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'nama:ntext',
            'email:ntext',
            'tlp:ntext',
            'isi:ntext',
            [
                'class' => 'yii\grid\ActionColumn', 
                            'template' => '{download}',

                'contentOptions'=>['style'=>'min-width: 10px;text-align:center;'], 
            'buttons' => [
                'download' => function ($url,$model) {

                    $urlx = Url::to(['usulan/download', 'id' => $model->id]);
                    return Html::a(
                        '<span class="glyphicon glyphicon-download"></span>',
                        $urlx, 
                    [
                        'title' => 'Download '.$model->upload,
                        'data-pjax' => '0',
                    ]
                    );
                },

            ],
            ]



            // [
            //     'update' => function ($url, $model, $key) {
            //         return $model->status === 'editable' ? Html::a('Update', $url) : '';
            //     },
            // ],
            // 'created_at',
            // 'updated_at',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
