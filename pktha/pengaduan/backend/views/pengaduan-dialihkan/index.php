<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengaduan Dialihkan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-assesmen-index">
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Kode Pengaduan',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan])->kode_pengaduan;
                }
            ],
            [
                'label' => 'Nama Pengadu',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengaduanRegistrasi::findOne(['id' => $model->id_pengaduan])->nama_pengadu;
                }
            ],
            'pesan_pengalihan_status',
            // 'sejarah_konflik:ntext',
            // 'akar_konflik:ntext',
            // 'pemicu_konflik:ntext',
            // 'akselerator_konflik:ntext',
            // 'pemetaan_dinamika_aktor:ntext',
            // 'sistem_representasi:ntext',
            // 'tawaran_tertinggi',
            // 'tawaran_terendah',
            // 'rekomendasi:ntext',
            // 'surat_kesediaan_mediasi',
            // 'id',
            // 'created_date',
            // 'modified_date',
            // 'finished:boolean',
            [
                'attribute' => 'finished',
                'label' => 'Status',
                'format' => 'html',
                'value' => function($model, $index, $widget) {
                    return $model->finished && $model->kasus_dialihkan ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
