<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiV */

$this->title = \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Update Pengusulan Hutan Adat Tahap V'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-v-view">

    <h1>Pengusulan Hutan Adat Tahap V</h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'id_pengusulan',
            [
                'attribute' => 'id_pengusulan',
                'label' => 'Kode Pengusulan',
                'value' => isset($model->id_pengusulan) ? \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan : ''
            ],
            'modified_date',
            'created_date',
            'perkembangan_hutan_adat:ntext',
            'tanggal_monitoring_dan_evaluasi',
            'komoditi_hutan_adat:ntext',
            'komoditi_unggulan_hutan_adat:ntext',
            'update_kelembagaan_mha:ntext',
            'finished:boolean',
        ],
    ]) ?>

</div>
