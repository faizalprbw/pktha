<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanMonevSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-hutan-adat-v-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_pengusulan') ?>

    <?= $form->field($model, 'modified_date') ?>

    <?= $form->field($model, 'created_date') ?>

    <?= $form->field($model, 'perkembangan_hutan_adat') ?>

    <?php // echo $form->field($model, 'tanggal_monitoring_dan_evaluasi') ?>

    <?php // echo $form->field($model, 'komoditi_hutan_adat') ?>

    <?php // echo $form->field($model, 'komoditi_unggulan_hutan_adat') ?>

    <?php // echo $form->field($model, 'update_kelembagaan_mha') ?>

    <?php // echo $form->field($model, 'finished')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
