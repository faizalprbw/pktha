<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdatV */

$this->title = Yii::t('app', 'Create Pengusulan Hutan Adat V');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat Vs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-v-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
