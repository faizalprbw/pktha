<?php

use yii\helpers\Html;
use common\models\PengusulanRegistrasi;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiV */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pengusulan Hutan Adat V',
]) . PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat'), 'url' => ['pengusulan-registrasi/index']];
$this->params['breadcrumbs'][] = ['label' => PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan, 'url' => ['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pengusulan-registrasi-v-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
