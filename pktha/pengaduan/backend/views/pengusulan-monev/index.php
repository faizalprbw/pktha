<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengusulanRegistrasiVSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengusulan Hutan Adat: Monev dan Pemberdayaan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-v-index">
    <p>
        <!-- <?= Html::a(Yii::t('app', 'Create Pengusulan Hutan Adat V'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            [
                'attribute' => 'id_pengusulan',
                'label' => 'Kode Pengusulan',
                'value' => function($model, $index, $widget) {
                    return isset($model->id_pengusulan) ? \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan : '';
                }
            ],
            [
                'label' => 'Nama Pengusul',
                'value' => function($model, $index, $widget) {
                    return \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->nama_pengusul;
                }
            ],
            // 'modified_date',
            // 'created_date',
            // 'perkembangan_hutan_adat:ntext',
            // 'tanggal_monitoring_dan_evaluasi',
            // 'komoditi_hutan_adat:ntext',
            // 'komoditi_unggulan_hutan_adat:ntext',
            // 'update_kelembagaan_mha:ntext',
            // 'finished:boolean',
            [
                'label' => 'Status',
                'format' => 'html',
                'value' => function($model, $index, $widget) {
                    return $model->finished ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
