<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rekomendasi */

$this->title = 'Update Hasil dan Rekomendasi: ' . ' ' . $model->hasil;
// $this->params['breadcrumbs'][] = ['label' => 'Rekomendasis', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-success">
    <div class="box-header with-border">
        <div class="box-tools pull-right">    
        </div>
    </div>
    <div class="rekomendasi-create box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
