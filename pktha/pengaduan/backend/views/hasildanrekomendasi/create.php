<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rekomendasi */

$this->title = 'Tambah Hasil dan Rekomendasi';
// $this->params['breadcrumbs'][] = ['label' => 'Rekomendasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['pengaduan/timeline', 'id' => $idx]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>          
        <div class="box-tools pull-right">    
        </div>
    </div>
    <div class="rekomendasi-create box-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
