<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengusulanValidasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pengusulan Hutan Adat: Validasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-validasi-index">
    <p>
        <!-- <?php echo Html::a(Yii::t('app', 'Create Pengusulan Validasi'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?php Pjax::begin(); ?>    <?php echo
    GridView::widget(
        [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_pengusulan',
                'label' => 'Kode Pengusulan',
                'value' => function ($model, $index, $widget) {
                    return isset($model->id_pengusulan) ? \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan : '';
                }
            ],
            [
                'label' => 'Nama Pengusul',
                'value' => function ($model, $index, $widget) {
                    return \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->nama_pengusul;
                }
            ],
            // 'id',
            // 'id_pengusulan',
            // 'created_date',
            // 'modified_date',
            // 'finished:boolean',
            [
                'label' => 'Status',
                'format' => 'html',
                'value' => function ($model, $index, $widget) {
                    return $model->finished ? Html::a('Finished', '', ['class' => 'label bg-green']) : Html::a('Not Finished', '', ['class' => 'label bg-red']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'],
        ],
        ]
    );
    ?>
    <?php Pjax::end(); ?></div>
