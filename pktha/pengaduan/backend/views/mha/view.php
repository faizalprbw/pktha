<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Mha */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mhas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mha-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_komunitas:ntext',
            'id_provinsi',
            'id_kota_kabupaten',
            'id_kecamatan',
            'luas_wilayah_adat',
            'batas_utara_wilayah_adat:ntext',
            'batas_selatan_wilayah_adat:ntext',
            'batas_timur_wilayah_adat:ntext',
            'batas_barat_wilayah_adat:ntext',
            'satuan_wilayah_adat:ntext',
            'kondisi_fisik_wilayah:ntext',
            'jumlah_kepala_keluarga',
            'jumlah_pria',
            'jumlah_wanita',
            'mata_pencaharian_utama:ntext',
            'sejarah_singkat_masyarakat_adat:ntext',
            'pembagian_ruang_menurut_aturan_adat:ntext',
            'sistem_penguasaan_pengelolaan_wilayah:ntext',
            'nama_lembaga_adat:ntext',
            'struktur_lembaga_adat:ntext',
            'struktur_lembaga_adat_filename:ntext',
            'tugas_fungsi_pemangku_adat:ntext',
            'tugas_fungsi_pemangku_adat_filename:ntext',
            'mekanisme_pengambilan_keputusan:ntext',
            'aturan_adat_terkait_wilayah_sda:ntext',
            'aturan_adat_terkait_wilayah_sda_filename:ntext',
            'aturan_adat_terkait_pranata_sosial:ntext',
            'aturan_adat_terkait_pranata_sosial_filename:ntext',
            'contoh_putusan_hukum_adat:ntext',
        ],
    ]) ?>

</div>
