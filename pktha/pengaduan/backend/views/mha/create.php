<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Mha */

$this->title = Yii::t('app', 'Create Mha');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mhas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mha-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
