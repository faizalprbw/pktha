<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MhaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Masyarakat Hukum Adat');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mha-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Masyarakat Hukum Adat'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'nama:ntext',
            'nama_komunitas:ntext',
            [
                'attribute' => 'id_provinsi',
                'value' => function($model, $index, $widget) {
                    isset($model->id_provinsi) ? \common\models\Provinsi::findOne(['id' => $model->id_provinsi])->nama_provinsi : null;
                }
            ],
            [
                'attribute' => 'id_kota_kabupaten',
                'value' => function($model, $index, $widget) {
                    isset($model->id_kota_kabupaten) ? \common\models\KotaKabupaten::findOne(['id' => $model->id_kota_kabupaten])->nama_kota_kabupaten : null;
                }
            ],
            [
                'attribute' => 'id_kecamatan',
                'value' => function($model, $index, $widget) {
                    isset($model->id_kecamatan) ? \common\models\Kecamatan::findOne(['id' => $model->id_kecamatan])->nama_kecamatan : null;
                }
            ],
            // 'luas_wilayah_adat',
            // 'batas_utara_wilayah_adat:ntext',
            // 'batas_selatan_wilayah_adat:ntext',
            // 'batas_timur_wilayah_adat:ntext',
            // 'batas_barat_wilayah_adat:ntext',
            // 'satuan_wilayah_adat:ntext',
            // 'kondisi_fisik_wilayah:ntext',
            // 'jumlah_kepala_keluarga',
            // 'jumlah_pria',
            // 'jumlah_wanita',
            // 'mata_pencaharian_utama:ntext',
            // 'sejarah_singkat_masyarakat_adat:ntext',
            // 'pembagian_ruang_menurut_aturan_adat:ntext',
            // 'sistem_penguasaan_pengelolaan_wilayah:ntext',
            // 'nama_lembaga_adat:ntext',
            // 'struktur_lembaga_adat:ntext',
            // 'struktur_lembaga_adat_filename:ntext',
            // 'tugas_fungsi_pemangku_adat:ntext',
            // 'tugas_fungsi_pemangku_adat_filename:ntext',
            // 'mekanisme_pengambilan_keputusan:ntext',
            // 'aturan_adat_terkait_wilayah_sda:ntext',
            // 'aturan_adat_terkait_wilayah_sda_filename:ntext',
            // 'aturan_adat_terkait_pranata_sosial:ntext',
            // 'aturan_adat_terkait_pranata_sosial_filename:ntext',
            // 'contoh_putusan_hukum_adat:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
