<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MhaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mha-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_komunitas') ?>

    <?= $form->field($model, 'id_provinsi') ?>

    <?= $form->field($model, 'id_kota_kabupaten') ?>

    <?= $form->field($model, 'id_kecamatan') ?>

    <?php // echo $form->field($model, 'luas_wilayah_adat') ?>

    <?php // echo $form->field($model, 'batas_utara_wilayah_adat') ?>

    <?php // echo $form->field($model, 'batas_selatan_wilayah_adat') ?>

    <?php // echo $form->field($model, 'batas_timur_wilayah_adat') ?>

    <?php // echo $form->field($model, 'batas_barat_wilayah_adat') ?>

    <?php // echo $form->field($model, 'satuan_wilayah_adat') ?>

    <?php // echo $form->field($model, 'kondisi_fisik_wilayah') ?>

    <?php // echo $form->field($model, 'jumlah_kepala_keluarga') ?>

    <?php // echo $form->field($model, 'jumlah_pria') ?>

    <?php // echo $form->field($model, 'jumlah_wanita') ?>

    <?php // echo $form->field($model, 'mata_pencaharian_utama') ?>

    <?php // echo $form->field($model, 'sejarah_singkat_masyarakat_adat') ?>

    <?php // echo $form->field($model, 'pembagian_ruang_menurut_aturan_adat') ?>

    <?php // echo $form->field($model, 'sistem_penguasaan_pengelolaan_wilayah') ?>

    <?php // echo $form->field($model, 'nama_lembaga_adat') ?>

    <?php // echo $form->field($model, 'struktur_lembaga_adat') ?>

    <?php // echo $form->field($model, 'struktur_lembaga_adat_filename') ?>

    <?php // echo $form->field($model, 'tugas_fungsi_pemangku_adat') ?>

    <?php // echo $form->field($model, 'tugas_fungsi_pemangku_adat_filename') ?>

    <?php // echo $form->field($model, 'mekanisme_pengambilan_keputusan') ?>

    <?php // echo $form->field($model, 'aturan_adat_terkait_wilayah_sda') ?>

    <?php // echo $form->field($model, 'aturan_adat_terkait_wilayah_sda_filename') ?>

    <?php // echo $form->field($model, 'aturan_adat_terkait_pranata_sosial') ?>

    <?php // echo $form->field($model, 'aturan_adat_terkait_pranata_sosial_filename') ?>

    <?php // echo $form->field($model, 'contoh_putusan_hukum_adat') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
