<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasiIV */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengusulan-hutan-adat-iv-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <div class="form-group field-pengusulanhutanadativ-tanggal_sk_penetapan">
        <label class="control-label">Kode Pengusulan</label>
        <input type="text" class="form-control" value="<?php echo \common\models\PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan])->kode_pengusulan ?>" disabled="true">
        <div class="help-block"></div>
    </div>
    
    <!-- <?php echo $form->field($model, 'id_pengusulan')->textInput(['disabled' => true]) ?> -->
    
    <?php echo $form->field($model, 'no_sk_penetapan')->textInput() ?>

    <?php echo $form->field($model, 'tanggal_sk_penetapan')->widget(yii\jui\DatePicker::className(), [
        'language' => 'en'
    ]) ?>
    
    <?php echo $form->field($model, 'perihal_sk_penetapan')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'luas_sk_penetapan')->textInput() ?>

    <?php echo $form->field($model, 'no_sk_pencantuman')->textInput(['rows' => 6]) ?>

    <?php echo $form->field($model, 'tanggal_sk_pencantuman')->widget(yii\jui\DatePicker::className()) ?>

    <?php echo $form->field($model, 'perihal_sk_pencantuman')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'luas_sk_pencantuman')->textInput() ?>
    
    <?php echo $form->field($model, 'latitude')->textInput(['type' => 'number', 'step' => '0.0000000001']) ?>
    
    <?php echo $form->field($model, 'longitude')->textInput(['type' => 'number', 'step' => '0.0000000001']) ?>

    <?php echo $form->field($model, 'form_verifikasi_data')->fileInput() ?>
    <?php if ($model->form_verifikasi_filename) : ?>
        <?php echo Html::a($model->form_verifikasi_filename, ['download', 'id' => $model->id], ['class' => 'btn bg-yellow']) ?>
    <?php endif ?>

    <?php echo $form->field($model, 'finished')->checkbox() ?>


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
