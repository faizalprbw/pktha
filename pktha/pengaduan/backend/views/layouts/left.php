<aside class="main-sidebar">
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                </span>
                <!-- <img src="<?php //$directoryAsset          ?>/img/avatar5.png" class="img-circle" alt="User Image"/> -->
            </div>
            <div class="pull-left info">
                <p><?php echo Yii::$app->user->identity->username; ?></p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <?php
        $ix = Yii::$app->user->identity->role;
        // var_dump($ix)
        if ($ix >= 30) {
            $admin = true;
        } else {
            $admin = false;
        }
        ?>

        <?php echo
        dmstr\widgets\Menu::widget(
            [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                            ['label' => 'Main Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/site/index'],],
                        // ['label' => 'Pengaduan', 'icon' => 'fa fa-list', 'url' => ['/pengaduan/index'],],
                            ['label' => 'Pengaduan Konflik', 'icon' => 'fa fa-legal', 'url' => ['/pengaduan-registrasi/index'],],
                            ['label' => 'SRN Hutan Adat', 'icon' => 'fa fa-tree', 'url' => ['/pengusulan-registrasi/index'],],
                            ['label' => 'SRN Masyarakat Hukum Adat', 'icon' => 'fa fa-tree', 'url' => ['/mha/index'],],
                            ['label' => 'Artikel', 'icon' => 'fa fa-file-code-o', 'url' => ['/artikel/index']],
                            ['label' => 'Slider', 'icon' => 'fa fa-sliders', 'url' => ['/slider/index'],],
                            ['label' => 'Contact us', 'icon' => 'fa fa-list', 'url' => ['/contact/index'],],
                        // ['label' => 'List Usulan', 'icon' => 'fa fa-list', 'url' => ['/usulan/index'],],
                            [
                                'label' => 'Tentang', 'visible' => $admin, 'icon' => 'fa fa-question', 'url' => '#',
                                'items' => [
                                    ['label' => 'Direktorat PKTHA', 'icon' => 'fa fa-list', 'url' => ['/artikel/direktorat-pktha'],],
                                    ['label' => 'Struktur Organisasi', 'icon' => 'fa fa-list', 'url' => ['/artikel/struktur-organisasi'],],
                                    ['label' => 'Tugas Pokok dan Fungsi', 'icon' => 'fa fa-list', 'url' => ['/artikel/tugas-pokok-dan-fungsi'],],
                                    // ['label' => 'Peraturan dan Perundangan Terkait', 'icon' => 'fa fa-list', 'url' => ['/artikel/peraturan-dan-perundangan-terkait'],],
                                    // ['label' => 'Lain-lain', 'icon' => 'fa fa-list', 'url' => ['/artikel/lain-lain'],],
                                ],
                            ],
                            [
                                'label' => 'Tata Cara', 'visible' => $admin, 'icon' => 'fa fa-newspaper-o', 'url' => '#',
                                'items' =>[
                                    ['label' => 'Pengaduan Konflik', 'icon' => 'fa fa-list', 'url' => ['/artikel/tatacara-pengaduan-konflik'],],
                                    ['label' => 'Pengusulan Hutan Adat', 'icon' => 'fa fa-list', 'url' => ['/artikel/tatacara-pengusulan-hutan-adat'],],
                                ]
                            ],
                            [
                                'label' => 'Kontak', 'visible' => $admin, 'icon' => 'fa fa-phone', 'url' => '#',
                                'items' =>[
                                    ['label' => 'Direktorat PKTHA', 'icon' => 'fa fa-list', 'url' => ['/artikel/kontak-direktorat-pktha'],],
                                    ['label' => 'Dirjen PSKL', 'icon' => 'fa fa-list', 'url' => ['/artikel/kontak-dirjen-pskl'],],
                                ]
                            ],
                            [
                                'label' => 'Change Password', 'visible' => !$admin, 'icon' => 'fa fa-cog', 'url' => ['/user/change-password'],
                            ],
                            [
                                'label' => 'Setting', 'visible' => $admin, 'icon' => 'fa fa-cog', 'url' => '#',
                                'items' => [
                                    ['label' => 'Tentang', 'icon' => 'fa fa-list', 'url' => ['/artikel/tentang'],],
                                    // ['label' => 'Tata Cara', 'icon' => 'fa fa-list', 'url' => ['/artikel/tatacara'],],
                                    ['label' => 'List Provinsi', 'icon' => 'fa fa-list', 'url' => ['/provinsi/index'],],
                                    ['label' => 'List Kota / Kabupaten', 'icon' => 'fa fa-list', 'url' => ['/kabkota/index'],],
                                    ['label' => 'List Kecamatan', 'icon' => 'fa fa-list', 'url' => ['/kecamatan/index'],],
                                    ['label' => 'List Desa / Kelurahan', 'icon' => 'fa fa-list', 'url' => ['/desa-kelurahan/index'],],
                                    ['label' => 'List Jenis Penyelesaian', 'icon' => 'fa fa-list', 'url' => ['/jenispenyelesaian/index'],],
                                    ['label' => 'List User', 'icon' => 'fa fa-list', 'url' => ['/user/index'],]
                                ],
                            ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
