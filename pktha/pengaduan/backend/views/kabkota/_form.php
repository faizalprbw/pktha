<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Kabkota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <!-- /.box-header -->
    <div class="kabkota-form box-body">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'id_provinsi')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi')); ?>
        <?= $form->field($model, 'nama_kota_kabupaten')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'latitude')->textInput() ?>
        <?= $form->field($model, 'longitude')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cancel', yii\web\Request::getReferrer(), ['class' => 'btn btn-warning']); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>