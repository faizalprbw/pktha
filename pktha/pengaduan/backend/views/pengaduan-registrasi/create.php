<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */

$this->title = Yii::t('app', 'Create Pengaduan Registrasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Registrasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengaduan-registrasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_create', [
        'model' => $model,
    ]) ?>

</div>
