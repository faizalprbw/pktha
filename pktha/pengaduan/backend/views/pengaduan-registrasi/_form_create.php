<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
Yii::$app->view->registerCSSFile('css/map/leaflet.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.ZoomBar.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.Locate.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet.groupedlayercontrol.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.BetterScale.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/iconLayers.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.Default.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet_geosearch.css', ['position' => yii\web\View::POS_HEAD]);

Yii::$app->view->registerJsFile('js/map/leaflet-src.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.ZoomBar.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.Locate.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-color-markers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.groupedlayercontrol.min.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.BetterScale.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/iconLayers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-providers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.markercluster.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/jquery.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/feature_group.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet_geosearch2.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-fill-Pattern.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/panggil_peta_form.js', ['position' => yii\web\View::POS_END]);
?>

<script type="text/javascript">
  $(document).ready(function () {
      $(".map_marking").hide();
      $(".show_map").click(function () {
          $(".map_marking").toggle("slow");
      })
  });
</script>

<div class="pengaduan-registrasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- Custom Case Code Generator -->
    <div class="row">
        <div>
            <div class="col-md-3">
                <div class="form-group field-pengaduanregistrasi-id_wilayah_konflik has-success">
                    <label class="control-label">Kode</label>
                    <select class="form-control" name="kode_huruf_pengaduan">
                        <option value="">Pilih Kode Pengaduan</option>
                        <?php if (Yii::$app->user->identity->role) : ?>
                          <option value="P">P (Admin Pusat)</option>
                        <?php endif ?>
                        <option value="D">D (Admin Daerah)</option>
                        <option value="T">T (Pesan Teks)</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END -->

    <?php echo $form->field($model, 'nama_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'no_telepon_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'alamat_lengkap_pengadu')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'email_pengadu')->textInput() ?>

    <?php echo $form->field($model, 'id_jenis_pelapor')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\JenisPelapor::find()->all(), 'id', 'jenis_pelapor'))->label('Jenis Pelapor') ?>

    <?php echo $form->field($model, 'id_tipe_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\TipeKonflik::find()->all(), 'id', 'nama_konflik'))->label('Jenis Konflik') ?>

    <?php echo $form->field($model, 'pihak_berkonflik_1')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pihak_berkonflik_2')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'pihak_berkonflik_3')->textarea(['rows' => 6]) ?>

    <?php
    echo
    $form->field($model, 'id_wilayah_konflik', ['options' => ['id' => 'select-wilayah-konflik']])->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
        'prompt' => 'Pilih Wilayah Konflik',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_provinsi_konflik").html(data); })',
            ]
    )->label('Wilayah Konflik')
    ?>

    <?php
    echo
    $form->field($model, 'id_provinsi_konflik')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), [
        'prompt' => 'Pilih Provinsi Konflik',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kota_kabupaten_konflik").html(data) })'
            ]
    )->label('Provinsi Konflik')
    ?>
    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_kota_kabupaten_konflik')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['id_provinsi' => $model->id_provinsi_konflik])->all(), 'id', 'nama_kota_kabupaten'), [
                'prompt' => 'Pilih Kota Kabupaten Konflik',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kecamatan_konflik").html(data) })'
                    ]
            )->label('Kota / Kabupaten')
            ?>
        </div>
        <div class="col-md-5">
<?php echo $form->field($model, 'teks_kota_kabupaten_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_kecamatan_konflik')->dropdownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id_kota_kabupaten' => $model->id_kota_kabupaten_konflik])->orWhere(['id_kota_kabupaten' => 1])->all(), 'id', 'nama_kecamatan'), [
                'prompt' => 'Pilih Kecamatan Konflik',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_desa_kelurahan_konflik").html(data) })'
                    ]
            )->label('Kecamatan')
            ?>
        </div>
        <div class="col-md-5">
<?php echo $form->field($model, 'teks_kecamatan_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo $form->field($model, 'id_desa_kelurahan_konflik')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['id_kecamatan' => $model->id_kecamatan_konflik])->orWhere(['id_kecamatan' => 1])->all(), 'id', 'nama_desa_kelurahan'), [
                'prompt' => 'Pilih Desa / Kelurahan Konflik',
                    ]
            )->label('Desa / Kelurahan')
            ?>
        </div>
        <div class="col-md-5">
<?php echo $form->field($model, 'teks_desa_kelurahan_konflik')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <?php echo $form->field($model, 'latitude_daerah_konflik')->textInput(['id' => 'latitude_daerah_konflik']) ?>

<?php echo $form->field($model, 'longitude_daerah_konflik')->textInput(['id' => 'longitude_daerah_konflik']) ?>

    <div class="row">
        <button type="button" class="btn btn-default show_map pull-left">Tampilkan Peta</button>
        <div class="clear" style="border-color:white;"></div>
        <div class="map_marking">
            <p style="color:grey;">Klik pada peta atau Geser marker untuk menentukan koordinat</p>
            <div class="map_wrapper">
                <div id="map" style="width:100%;height:500px;background:white"></div>
            </div>
        </div>
    </div>

    <?php echo $form->field($model, 'fungsi_kawasan_konflik')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'luas_kawasan_konflik')->textInput() ?>

    <?php
  	echo $form->field($model, 'tanggal_pengaduan')->widget(yii\jui\DatePicker::className(), [
      'language' => 'en',
    ])
    ?>
    
    <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
