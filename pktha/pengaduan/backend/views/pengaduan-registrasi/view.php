<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $registrasiModel common\models\PengaduanRegistrasi */
/* @var $konflik common\models\TipeKonflik */


// TITLE
$this->title = "Tahapan Pengaduan"; // taken from tabel pengaduan_registrasi
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengaduan Registrasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .timeline-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .timeline-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }
    .box-header .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'FontAwesome';  /* essential for enabling glyphicon */
        content: "\f0d8";    /* adjust as needed, taken from bootstrap.css */
        float: center;        /* adjust as needed */
    }
    .box-header .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f0d7";    /* adjust as needed, taken from bootstrap.css */
    }</style>

<!-- OVERVIEW -->
<div class="box box-info">
    <div class="pengaduan-registrasi-view box-body">

        <!-- TITLE: ID row  in red box-->
        <ul class="timeline">
            <li class="time-label">
                <span class="bg-blue-active">
                    Kode Pengaduan : <?php echo $registrasiModel['model']->kode_pengaduan; ?>
                </span>
            </li>

            <!-- collapsible panel -->
            <!-- REGISTRASI -->
            <li>
                <?php
                    $deskStudyModel = common\models\PengaduanDeskStudy::findOne(['id_pengaduan' => $registrasiModel['model']->id]);
                ?>
                <?php // echo '<i class="fa fa-check bg-' . $registrasiModel['warna'] . '-active"></i>'; ?>

                <?php if (isset($deskStudyModel) && $deskStudyModel->finished) : ?>
                    <?php echo '<i class="fa fa-check bg-' . $assesmenModel['warna'] . '-active"></i>'; ?>
                <?php else : ?>
                    <i class="fa fa-ellipsis-h"></i>
                <?php endif ?>

                <div class="timeline-item">
                    <span class="time pull-right">
                        <?php
                        $url = yii\helpers\Url::to(["pengaduan-registrasi/update", 'id' => $registrasiModel['model']->id]);
                        echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                        ?>
                    </span>
                    <h3 class="timeline-header"><b>Registrasi & <i>Desk Study</i>  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse1"></a></h3>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="box-body">
                            <h5 class="box-header no-padding"><b><i class="fa fa-user"></i> Identitas Pengadu </b><a class="accordion-toggle" data-toggle="collapse" href="#collapseUser"></a></h5>
                            <div id="collapseUser" class="panel-collapse collapse">
                                <div class="col-sm-12">
                                    <div style='border-bottom:1px solid #ccc;'></div>
                                </div><br>
                                <dl class="dl-horizontal">
                                    <dt>Nama Pengadu</dt>
                                    <dd><?php echo $registrasiModel['model']->nama_pengadu ?></dd>
                                    <dt>Kode Pengaduan</dt>
                                    <dd><?php echo $registrasiModel['model']->kode_pengaduan ?></dd>
                                    <dt>No. Telepon</dt>
                                    <dd><?php echo $registrasiModel['model']->no_telepon_pengadu ?></dd>
                                    <dt>Alamat</dt>
                                    <dd><?php echo $registrasiModel['model']->alamat_lengkap_pengadu ?></dd>
                                    <dt>Email</dt>
                                    <dd><?php echo $registrasiModel['model']->email_pengadu ?></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="box-body">
                            <h5 class="box-header no-padding"><b><i class="fa fa-tree"></i> Perihal Konflik </b><a class="accordion-toggle" data-toggle="collapse" href="#collapseKonflik"></a></h5>
                            <div id="collapseKonflik" class="panel-collapse collapse">
                                <div class="col-sm-12">
                                    <div style='border-bottom:1px solid #ccc;'></div>
                                </div><br>
                                <dl class="dl-horizontal">
                                    <dt>Kode Pengaduan</dt>
                                    <dd><?php echo $registrasiModel['model']->kode_pengaduan ?></dd>
                                    <dt>Tipe Konflik</dt>
                                    <dd><?php echo $registrasiModel['model']->getTipeKonflik($registrasiModel['model']->id_tipe_konflik) ?></dd>
                                    <dt>Pihak Berkonflik</dt>
                                    <dd><?php echo $registrasiModel['model']->pihak_berkonflik_1 ?></dd>
                                    <dt>Wilayah Konflik</dt>
                                    <dd><?php echo \common\models\Wilayah::findOne(['id' => $registrasiModel['model']->id_wilayah_konflik])->nama_wilayah; ?></dd>
                                </dl>
                            </div>
                        </div>
                        <span class="timeline-footer pull-right" style="color:#999">
                            <i class="fa fa-clock-o"></i> <?php echo $registrasiModel['model']->modified_date; ?>
                        </span>
                    </div>
                </div>
            </li>

            <!-- ASSESMEN -->
            <li>
                <?php if (!isset($assesmenModel)) : ?>
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-assesmen/create", 'id_pengaduan' => $registrasiModel['model']->id]);
                            // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Assesmen  </b><a class="accordion-toggle" data-toggle="collapse" data-parrent="#accordion" href="#collapse2"></a></h3>
                    </div>
                <?php else : ?>
                    <?php if (isset($assesmenModel['model']->finished) && $assesmenModel['model']->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $assesmenModel['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif ?>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-assesmen/update", 'id' => $assesmenModel['model']->id]);
                            echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Assesmen  </b><a class="accordion-toggle" data-toggle="collapse" data-parrent="#accordion" href="#collapse2"></a></h3>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <?php //echo Html::a($assesmen->surat_kesediaan_mediasi, $url); ?>
                                    <dt>Potensi Konflik dan <br>Potensi Damai</dt>
                                    <dd><?php echo $assesmenModel['model']->potensi_konflik_dan_potensi_damai ?></dd>
                                    <dt>Sejarah Konflik</dt>
                                    <dd><?php echo $assesmenModel['model']->sejarah_konflik ?></dd>
                                    <dt>Akar Konflik</dt>
                                    <dd><?php echo $assesmenModel['model']->akar_konflik ?></dd>
                                    <dt>Pemicu Konflik</dt>
                                    <dd><?php echo $assesmenModel['model']->pemicu_konflik ?></dd>
                                    <dt>Akselerator Konflik</dt>
                                    <dd><?php echo $assesmenModel['model']->akselerator_konflik ?></dd>
                                    <dt>Dinamika Aktor</dt>
                                    <dd><?php echo $assesmenModel['model']->pemetaan_dinamika_aktor ?></dd>
                                    <dt>Dinamika Aktor File</dt>
                                    <dd>
                                        <?php if ($assesmenModel['model']->pemetaan_dinamika_aktor_filename) : ?>
                                            <?php echo Html::a($assesmenModel['model']->pemetaan_dinamika_aktor_filename, ['pengaduan-assesmen/download', 'id' => $assesmenModel['model']->id, 'file' => 'pemetaan_dinamika_aktor']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>Sistem Representasi</dt>
                                    <dd><?php echo $assesmenModel['model']->sistem_representasi ?></dd>
                                    <dt>Tawaran Tertinggi</dt>
                                    <dd><?php echo $assesmenModel['model']->tawaran_tertinggi ?></dd>
                                    <dt>Tawaran Terendah</dt>
                                    <dd><?php echo $assesmenModel['model']->tawaran_terendah ?></dd>
                                    <dt>Rekomendasi</dt>
                                    <dd><?php echo $assesmenModel['model']->rekomendasi ?></dd>
                                    <dt>Surat Kesediaan Mediasi</dt>
                                    <dd>
                                        <?php if ($assesmenModel['model']->surat_kesediaan_mediasi_filename) : ?>
                                            <?php echo Html::a($assesmenModel['model']->surat_kesediaan_mediasi_filename, ['pengaduan-assesmen/download', 'id' => $assesmenModel['model']->id, 'file' => 'surat_kesediaan_mediasi']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                </dl>
                            </div>
                            <div class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $assesmenModel['model']->modified_date; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </li>

            <!-- PRA MEDIASI -->
            <li>
                <?php if (!isset($praMediasiModel)) : ?>
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-pra-mediasi/create", 'id' => $registrasiModel['model']->id]);
                            // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Pra Mediasi  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse3"></a></h3>
                    </div>
                <?php else : ?>
                    <?php if (isset($praMediasiModel['model']->finished) && $praMediasiModel['model']->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $praMediasiModel['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif ?>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-pra-mediasi/update", 'id' => $praMediasiModel['model']->id]);
                            echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Pra Mediasi  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse3"></a></h3>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Data Para Pihak</dt>
                                    <dd><?php echo $praMediasiModel['model']->data_para_pihak ?></dd>
                                    <dt>Pokok Permasalahan</dt>
                                    <dd><?php echo $praMediasiModel['model']->pokok_permasalahan ?></dd>
                                    <dt>Objek Mediasi</dt>
                                    <dd><?php echo $praMediasiModel['model']->objek_mediasi ?></dd>
                                </dl>
                            </div>
                            <div class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $praMediasiModel['model']->modified_date; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </li>

            <!-- MEDIASI -->
            <li>
                <?php if (!isset($mediasiModel)) : ?>
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-mediasi/create", 'id_pengaduan' => $registrasiModel['model']->id]);
                            // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Mediasi  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse4"></a></h3>
                    </div>
                <?php else : ?>
                    <?php if (isset($mediasiModel['model']->finished) && $mediasiModel['model']->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $mediasiModel['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif; ?>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php if (isset($mediasiModel)) : ?>
                                <?php
                                $url = yii\helpers\Url::to(["pengaduan-mediasi/update", 'id' => $mediasiModel['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            <?php endif ?>
                        </span>
                        <h3 class="timeline-header"><b>Mediasi  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse4"></a></h3>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Data Para Pihak</dt>
                                    <dd><?php echo $mediasiModel['model']->data_para_pihak ?></dd>
                                    <dt>Berita Acara</dt>
                                    <dd>
                                        <?php if ($mediasiModel['model']->berita_acara_filename) : ?>
                                            <?php echo Html::a($mediasiModel['model']->berita_acara_filename, ['pengaduan-mediasi/download', 'id' => $mediasiModel['model']->id, 'file' => 'berita_acara']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>Foto</dt>
                                    <dd>
                                        <?php if ($mediasiModel['model']->foto_filename) : ?>
                                            <?php echo Html::a($mediasiModel['model']->foto_filename, ['pengaduan-mediasi/download', 'id' => $mediasiModel['model']->id, 'file' => 'foto']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>Pokok Permasalahan</dt>
                                    <dd><?php echo $mediasiModel['model']->pokok_permasalahan ?></dd>
                                    <dt>Objek Mediasi</dt>
                                    <dd><?php echo $mediasiModel['model']->objek_mediasi ?></dd>
                                    <dt>Surat Usulan Mediator</dt>
                                    <dd>
                                        <?php if ($mediasiModel['model']->surat_usulan_mediator_filename) : ?>
                                            <?php echo Html::a($mediasiModel['model']->surat_usulan_mediator_filename, ['pengaduan-mediasi/download', 'id' => $mediasiModel['model']->id, 'file' => 'surat_usulan_mediator']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>SK Mediator</dt>
                                    <dd>
                                        <?php if ($mediasiModel['model']->sk_mediator_filename) : ?>
                                            <?php echo Html::a($mediasiModel['model']->sk_mediator_filename, ['pengaduan-mediasi/download', 'id' => $mediasiModel['model']->id, 'file' => 'sk_mediator']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>Proses Pertemuan</dt>
                                    <dd><?php echo $mediasiModel['model']->proses_pertemuan ?></dd>
                                </dl>
                            </div>
                            <div class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $mediasiModel['model']->modified_date; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </li>

            <!-- DRAFTING MOU -->
            <li>
                <?php if (!isset($draftingMouModel)) : ?>
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-drafting-mou/create", 'id_pengaduan' => $registrasiModel['model']->id]);
                            // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Drafting MOU  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse5"></a></h3>
                    </div>
                <?php else : ?>
                    <?php if (isset($draftingMouModel['model']->finished) && $draftingMouModel['model']->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $draftingMouModel['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif; ?>
                    <div class="timeline-item">
                        <span class="time pull-right">
                            <?php if (isset($draftingMouModel)) : ?>
                                <?php
                                $url = yii\helpers\Url::to(["pengaduan-drafting-mou/update", 'id' => $draftingMouModel['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            <?php endif ?>
                        </span>
                        <h3 class="timeline-header"><b>Drafting MOU  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse5"></a></h3>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <?php $draftingMou = \common\models\PengaduanDraftingMou::find()->getByPengaduanId($registrasiModel['model']->id) ?>
                                    <dt>Berita Acara</dt>
                                    <dd>
                                        <?php if ($draftingMouModel['model']->berita_acara_filename) : ?>
                                            <?php echo Html::a($draftingMouModel['model']->berita_acara_filename, ['pengaduan-drafting-mou/download', 'id' => $draftingMouModel['model']->id, 'file' => 'berita_acara']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                                    <dt>Foto</dt>
                                    <dd>
                                        <?php if ($draftingMouModel['model']->foto_filename) : ?>
                                            <?php echo Html::a($draftingMouModel['model']->foto_filename, ['pengaduan-drafting-mou/download', 'id' => $draftingMouModel['model']->id, 'file' => 'foto']) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                            </div>
                            <div class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $draftingMouModel['model']->modified_date; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </li>

            <!-- TTD MOU -->
            <li>
                <?php if (!isset($tandaTanganMouModel)) : ?>
                    <i class="fa fa-ellipsis-h"></i>
                    <div class="timeline-item timeline-item-last">
                        <span class="time pull-right">
                            <?php
                            $url = yii\helpers\Url::to(["pengaduan-tanda-tangan-mou/create", 'id_pengaduan' => $registrasiModel['model']->id]);
                            // echo Html::a('<i class="fa fa-plus-square-o" style="font-size:20px"></i>', $url, ['title' => 'Create data']);
                            ?>
                        </span>
                        <h3 class="timeline-header"><b>Tanda Tangan MOU  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse6"></a></h3>
                    </div>
                <?php else : ?>
                    <?php if (isset($tandaTanganMouModel['model']->finished) && $tandaTanganMouModel['model']->finished) : ?>
                        <?php echo '<i class="fa fa-check bg-' . $tandaTanganMouModel['warna'] . '-active"></i>'; ?>
                    <?php else : ?>
                        <i class="fa fa-ellipsis-h"></i>
                    <?php endif; ?>
                    <div class="timeline-item timeline-item-last">
                        <span class="time pull-right">
                            <?php if (isset($tandaTanganMouModel)) : ?>
                                <?php
                                $url = yii\helpers\Url::to(["pengaduan-tanda-tangan-mou/update", 'id' => $tandaTanganMouModel['model']->id]);
                                echo Html::a('<i class="fa fa-pencil-square-o" style="font-size:20px"></i>', $url, ['title' => 'Update data']);
                                ?>
                            <?php endif ?>
                        </span>
                        <h3 class="timeline-header"><b>Tanda Tangan MOU  </b><a class="accordion-toggle" data-toggle="collapse" href="#collapse6"></a></h3>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <?php $mou = \common\models\PengaduanTandaTanganMou::find()->getByPengaduanId($registrasiModel['model']->id) ?>
                                    <dt>Dokumen MOU</dt>
                                    <dd>
                                        <?php if ($tandaTanganMouModel['model']->dokumen_mou_filename) : ?>
                                            <?php echo Html::a($tandaTanganMouModel['model']->dokumen_mou_filename, ['pengaduan-tanda-tangan-mou/download', 'id' => $tandaTanganMouModel['model']->id]) ?>
                                        <?php else : ?>
                                            <i>Tidak Tersedia</i>
                                        <?php endif ?>
                                    </dd>
                            </div>
                            <div class="timeline-footer pull-right" style="color:#999">
                                <i class="fa fa-clock-o"></i> <?php echo $tandaTanganMouModel['model']->created_date; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </li>
        </ul>

<!-- <h1><?php echo Html::encode($this->title) ?></h1> -->
        <!--
        <p>
        <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $registrasiModel['model']->id], ['class' => 'btn btn-primary']) ?>
        <?php
        echo
        Html::a(
                Yii::t('app', 'Delete'), ['delete', 'id' => $registrasiModel['model']->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
                ]
        )
        ?>
        </p>
        -->

    </div>
</div>
