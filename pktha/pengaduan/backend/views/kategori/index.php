<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?=$this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <?= Html::a('Create Kategori', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>

<div class="kategori-index box-body">

    
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'kategori',
            'created_at:datetime',
            [
                'attribute' => 'user_id',                
                'value' => 'user.username',
            ],
        //         [
    //     'attribute' => 'user_id',
    //     'label' => Yii::t('app', 'Members Alias'),
    //     'value' => 'user.full_name',
    //     // 'filter' => \common\models\User::profileDropDown(),
    //     'contentOptions' => ['style' => 'width: 150px;', 'class' => 'select2'],
    //     'filterOptions' => ['data-widget' => 'select2'],
    //     'format' => 'html',
    // ],
    //         // 'user.name',
            // 'author.Name',
            // [
            //     'attribute' => 'author.Twitter',
            //     'type' => 'html',
            //     'value' => function($model) {
            //         return Html::link('@' . $model->author->Twitter, 'http://twitter.com/' . $model->author->Twitter);
            //     }
            // ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
