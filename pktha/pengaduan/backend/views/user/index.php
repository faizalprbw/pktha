<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">List <?php echo $this->title;?></h3>          
      <div class="box-tools pull-right">
          <p>
        <?php echo Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>

<div class="provinsi-index box-body">
    <?php echo GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'role',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ' {delete} {change-password}',
                    'buttons' => [
                        'change-password' => function ($url) {
                            return Html::a(
                                '&nbsp; <span class="glyphicon glyphicon-pencil"></span> &nbsp;',
                                $url, 
                                [
                                    'title' => 'Ubah kata sandi',
                                    'data-pjax' => '0',
                                ]
                            );
                        }
                    ]
                ],
            ],
        ]
    ); ?>
    </div>
</div>
