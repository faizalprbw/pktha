<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'List User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><?=$this->title;?></h3>                
</div>




<div class="site-signup box-body" >
    

    <p>Please fill out the following fields to signup:</p>

   



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
