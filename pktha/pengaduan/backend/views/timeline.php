<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;



/* @var $this yii\web\View */
/* @var $model common\models\Pengaduan */

$this->title = $model->kode;
$this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => 'Pengaduan', 'url' => ['timeline','id'=>$model->id]];
$this->params['breadcrumbs'][] = $this->title;
$idx= $model->id;
?>
<div class="box box-info">
    <div class="pengaduan-view box-body">
    <?php //$form = yii\widgets\ActiveForm::begin(); ?>

      <?php //$form->field($model, 'sarana_pengaduan')->dropDownList(array('1' => 'Registrasi','2' => 'Assesment Lapangan','3' => 'Analisa Permasalahan','4' => 'Rekomendasi'),[
    //             'prompt'=>'-- Tahapan--',
    //             'onchange'=>
    //             '$.get("' . yii::$app->urlManager->createUrl('pengaduan/setTahap') . '",
    //             { id: $(this).val() }
    //             ).done(function( data ) {
    //                 console.log(data);                    
    //             })',

    // ])->label("");  ?>
        <?php //yii\widgets\ActiveForm::end(); ?>


        <?php
            // var_dump($model->dokumen);
            

        ?>

      <ul class="timeline">

        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-red">
                No Registrasi : <?= $this->title;?>
            </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> <?=date("d-m-y h:i:s",$model->created_at);?></span>


                <h3 class="timeline-header"><b>Registrasi</b></h3>
                <!-- CONTENT    -->
                <div class="box-body">
                  <dl class="dl-horizontal">
                  <dt>Identitas Pengadu</dt>
                    <dd><?=$model->nama_identitas?></dd>
                    <dt>Nomor Surat</dt>
                    <dd><?=$model->nomor_surat?></dd>                    
                    <dt>Pihak berkonflik</dt>
                    <dd><?=$model->pihak_berkonflik?></dd>                    
                    <dt>Lokasi</dt>
                    <dd><?=$model->lokasi_konflik?> (<?=$model->getProvinsi($model->provinsi_konflik);?> - <?=$model->getKabkota($model->kabkota_konflik);?>)</dd>
                    <dt>Tipologi Kasus</dt>
                    <dd><?=$model->tipologi_kasus?></dd>
                    <dt>Tuntutan Pengaduan</dt>
                    <dd><?=$model->tutuntan_pengaduan?></dd>
                    
              </dl>
            </div>
            


            

  <!-- CONTENT    -->
  <div class="timeline-footer">
    <?php
    $url =yii\helpers\Url::to(["pengaduan/view",'id'=>$model->id]);
    echo Html::a('Detail',$url,['class'=>"btn btn-success btn-xs"]);
    ?>
    <?php
    $url =yii\helpers\Url::to(["pengaduan/update",'id'=>$model->id]);
    echo Html::a('Update',$url,['class'=>"btn btn-primary btn-xs"]);
    ?>
    <?php
    $url =yii\helpers\Url::to(["dokumen/create",'id'=>$model->id,'idt'=>1,'idx'=>$idx]);
    echo Html::a('Add Attachment',$url,['class'=>"btn btn-primary btn-xs"]);
    ?>
    <br/>
    <br/>
    
            <ul class="mailbox-attachments clearfix">
                <?php foreach ($model->getDokumenid(1)->all() as $dk ):?>
                <li>                  
                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name wordwrap"><i class="fa fa-paperclip"></i> <?= $dk->url?></a>
                        <span class="mailbox-attachment-size">
                          &nbsp;
                          <?php 
                                $url = "../../uploads/".$dk->url;
                                echo Html::a('<i class="fa fa-cloud-download"></i>',$url,['class'=>"btn btn-default btn-xs pull-right"]);
                            ?>

                          <?php
                                $url =yii\helpers\Url::to(["dokumen/delete",'id'=>$dk->id,'idx'=>$idx]);
                                echo Html::a('<i class="fa fa-trash-o"></i>',$url,['class'=>"btn btn-default btn-xs pull-right",'data'=>['method'=>'post']]);
                            ?>
                        </span>
                  </div>
                </li>                
                <?php endforeach;?>     
            </ul>
       

    </div>
</div>
</li>
<!-- Assesment Lapangan -->
<li>
    <!-- timeline icon -->
    <?php
        $assesmentcount=count($modelAssesment);
        // var_dump($modelAnalisa['hasil_analisa']);        
        $analisacount=count($modelAnalisa);
        $rekomendasicount=count($modelRekomendasi);
    ?>
    <i class="fa fa-envelope bg-primary"></i>
    <div class="timeline-item">
        
        <h3 class="timeline-header"><b>Assesment Lapangan</b></h3>
        <?php         
        if($assesmentcount>0):
        // if ($model->id_tahapan==2):
        ?>

        <div class="timeline-body">          
                        
                        <?php
                        $url =yii\helpers\Url::to(["assesment/createid",'id'=>$model->id,'idx'=>$idx]);
                        echo Html::a('Add assesment lapangan',$url,['class'=>"btn btn-success btn-xs"]);
                        ?>
                              
                        <br/>
                        <br/>

                        

                        
            <table class="table table-hover">
                <tbody><tr>                  
                  <th>Nama Tim</th>
                  <th>Anggota Tim</th>
                  <th>Tanggal berangkat</th>
                  <th>Tanggal kembali</th>
                  <th>Analisa awal</th>
                  <th>Action</th>
                </tr>
                <?php foreach ($modelAssesment as $ma):?>
                  
                <tr>
                  <td><?=$ma->nama_tim;?></td>
                  <td><?=$ma->anggota_tim;?></td>
                  <td><?=$ma->tgl_berangkat;?></td>
                  <td><?=$ma->tgl_pulang;?></td>
                  <td><?=$ma->analisa_awal;?></td>
                  <td>
                  <?php
                        $url =yii\helpers\Url::to(["assesment/view",'id'=>$ma->id ,'idx'=>$idx]);
                        echo Html::a('View',$url,['class'=>"btn btn-success btn-xs"]);
                        ?>
                        <?php
                        $url =yii\helpers\Url::to(["assesment/update",'id'=>$ma->id,'idx'=>$idx]);
                        echo Html::a('Update',$url,['class'=>"btn btn-primary btn-xs"]);
                        ?>
                        <?php
                        $url =yii\helpers\Url::to(["assesment/deleteid",'id'=>$ma->id,'idx'=>$idx]);
                        echo Html::a('Delete',$url,['class'=>"btn btn-danger btn-xs"]);
                        ?>
                        <?php
                        $url =yii\helpers\Url::to(["dokumen/createassesment",'id'=>$model->id,'idt'=>2,'idx'=>$idx,'idassesment'=>$ma->id]);
                        echo Html::a('Add attachment',$url,['class'=>"btn btn-warning btn-xs"]);
                        ?>
                    </td>
                </tr>
                <?php  if(count($model->getDokumenidassesment(2,$ma->id)->all())>0):?>
                <tr>
                    <td colspan="6"><ul class="mailbox-attachments clearfix">

                <?php foreach ($model->getDokumenidassesment(2,$ma->id)->all() as $dk ):?>

                

                <li>                  
                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name wordwrap"><i class="fa fa-paperclip"></i> <?= $dk->url?></a>
                        
                          &nbsp;
                          <?php 
                                $url = "../../uploads/".$dk->url;
                                echo Html::a('<i class="fa fa-cloud-download"></i>',$url,['class'=>"btn btn-default btn-xs pull-right"]);
                            ?>

                          <?php
                                $url =yii\helpers\Url::to(["dokumen/delete",'id'=>$dk->id,'idx'=>$idx]);
                                echo Html::a('<i class="fa fa-trash-o"></i>',$url,['class'=>"btn btn-default btn-xs pull-right",'data'=>['method'=>'post']]);
                            ?>
                        
                  </div>
                </li>                
                <?php endforeach;?>     
            </ul>
                    </td>
                </tr>
                <<?php endif; ?>
                 <?php endforeach;?>
                </tbody>
            </table>            
        </div>
        <div class="timeline-footer">
            
        </div>
    <?php else:?>
        <div class="timeline-footer">            
            <?php
            $url =yii\helpers\Url::to(["assesment/createid",'id'=>$model->id,'idx'=>$idx]);
            echo Html::a('Tambah Assesment Lapangan',$url,['class'=>"btn btn-primary btn-xs"]);
            ?>
        </div>
    <?php endif;?>
    </div>
</li>
<!-- Assesment Lapangan -->
<li>
    <!-- timeline icon -->
    <i class="fa fa-envelope bg-primary"></i>
    <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> </span>
        <h3 class="timeline-header"><b>Analisa Permasalahan</b></h3>
       
       <?php if($analisacount>0):?>

        <div class="timeline-body">                
            <div class="box-body">
                  <dl class="dl-horizontal">
                  <dt>Hasil Analisa</dt>
                    <dd><?=$modelAnalisa['hasil_analisa']?></dd>
                    <dt>Pembahasan</dt>
                    <dd><?=$modelAnalisa['pembahasan']?></dd>                                        
              </dl>
            </div>
        </div>
        <div class="timeline-footer">
            <?php
            $url =yii\helpers\Url::to(["analisa/view",'id'=>$modelAnalisa['id']]);
            echo Html::a('View',$url,['class'=>"btn btn-success btn-xs"]);
            ?>
            <?php
            $url =yii\helpers\Url::to(["analisa/update",'id'=>$modelAnalisa['id']]);
            echo Html::a('Update',$url,['class'=>"btn btn-primary btn-xs"]);
            ?>
            <?php
                        $url =yii\helpers\Url::to(["dokumen/create",'id'=>$model->id,'idt'=>3,'idx'=>$idx]);
                        echo Html::a('Add attachment',$url,['class'=>"btn btn-primary btn-xs"]);
                        ?>      
                        <br/>
                        <br/>
            <ul class="mailbox-attachments clearfix">

                <?php foreach ($model->getDokumenid(3)->all() as $dk ):?>

                

                <li>                  
                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name wordwrap"><i class="fa fa-paperclip"></i> <?= $dk->url?></a>
                        <span class="mailbox-attachment-size">
                          &nbsp;
                          <?php 
                                $url = "../../uploads/".$dk->url;
                                echo Html::a('<i class="fa fa-cloud-download"></i>',$url,['class'=>"btn btn-default btn-xs pull-right"]);
                            ?>

                          <?php
                                $url =yii\helpers\Url::to(["dokumen/delete",'id'=>$dk->id,'idx'=>$idx]);
                                echo Html::a('<i class="fa fa-trash-o"></i>',$url,['class'=>"btn btn-default btn-xs pull-right",'data'=>['method'=>'post']]);
                            ?>
                        </span>
                  </div>
                </li>                
                <?php endforeach;?>     
            </ul>
        </div>
    <?php elseif($model->id_tahapan==2):?>
        <div class="timeline-footer">            
            <?php
            $url =yii\helpers\Url::to(["analisa/createid",'id'=>$model->id]);
            echo Html::a('Tambah Analisa Permasalahan',$url,['class'=>"btn btn-primary btn-xs"]);
            ?>
            
        </div>
    <?php else:?>        
        <div class="timeline-body">            
            <p>menunggu step sebelumnya</p>
        </div>
    <?php endif;?>


    </div>
</li>
<!-- Assesment Lapangan -->
<li>
    <!-- timeline icon -->
    <i class="fa fa-envelope bg-primary"></i>
    <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i></span>
        <h3 class="timeline-header"><b>Tahap Rekomendasi</b></h3>
        
       <?php if($rekomendasicount>0):?>

        <?php
            // var_dump($modelRekomendasi);
        ?>
        <div class="timeline-body">                
            <div class="box-body">
                  <dl class="dl-horizontal">
                  <dt>Hasil Analisa</dt>
                    <dd><?=$modelRekomendasi['jenispenyelesaian_id']?></dd>
                    <dt>Pembahasan</dt>
                    <dd><?=$modelRekomendasi['hasil_penyelesaian']?></dd>                                        
              </dl>
            </div>
            
        </div>
        <div class="timeline-footer">
            <?php
            $url =yii\helpers\Url::to(["pengaduan/view",'id'=>$model->id]);
            echo Html::a('View',$url,['class'=>"btn btn-success btn-xs"]);
            ?>
            <?php
            $url =yii\helpers\Url::to(["pengaduan/update",'id'=>$model->id]);
            echo Html::a('Update',$url,['class'=>"btn btn-primary btn-xs"]);
            ?>
            <?php
                        $url =yii\helpers\Url::to(["dokumen/create",'id'=>$model->id,'idt'=>4,'idx'=>$idx]);
                        echo Html::a('Add attachment',$url,['class'=>"btn btn-primary btn-xs"]);
                        ?>      
                        <br/>
                        <br/>
            <ul class="mailbox-attachments clearfix">

                <?php foreach ($model->getDokumenid(4)->all() as $dk ):?>

                

                <li>                  
                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name wordwrap"><i class="fa fa-paperclip"></i> <?= $dk->url?></a>
                        <span class="mailbox-attachment-size">
                          &nbsp;

                          
                            
                            <?php 
                                $url = "../../uploads/".$dk->url;
                                echo Html::a('<i class="fa fa-cloud-download"></i>',$url,['class'=>"btn btn-default btn-xs pull-right"]);
                            ?>
                          

                          <?php
                                $url =yii\helpers\Url::to(["dokumen/delete",'id'=>$dk->id,'idx'=>$idx]);
                                echo Html::a('<i class="fa fa-trash-o"></i>',$url,['class'=>"btn btn-default btn-xs pull-right",'data'=>['method'=>'post']]);
                            ?>
                        </span>
                  </div>
                </li>                
                <?php endforeach;?>     
            </ul>
        </div>
    <?php elseif($model->id_tahapan==3):?>
        <div class="timeline-footer">            
            <?php
            $url =yii\helpers\Url::to(["rekomendasi/createid",'id'=>$model->id]);
            echo Html::a('Tambah Rekomendasi Lapangan',$url,['class'=>"btn btn-primary btn-xs"]);
            ?>
        </div>
    <?php else:?>        
        <div class="timeline-body">            
            <p>menunggu step sebelumnya</p>
        </div>
    <?php endif;?>


    </div>
</li>

<!-- END timeline item -->

</ul>
</div>
</div>