<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rekomendasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekomendasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenispenyelesaian_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Jenispenyelesaian::find()->all(),'id','nama')); ?>
    
    <?= $form->field($model, 'hasil_penyelesaian')->textarea(['rows' => 6]) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
