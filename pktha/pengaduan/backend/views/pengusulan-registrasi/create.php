<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdat */

$this->title = Yii::t('app', 'Create Pengusulan Hutan Adat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengusulan-hutan-adat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_create', [
        'model' => $model,
    ]) ?>

</div>
