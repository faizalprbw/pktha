<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pengusulan Hutan Adat',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pengusulan-hutan-adat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
