<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\pengusulanregistrasi */
/* @var $form yii\widgets\ActiveForm */

/**
 * Please refer JS file to js/pengusulan-registrasi/script.js
 */
?>

<div class="pengusulan-registrasi-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!-- Custom Case Code Generator -->
    <div class="row">
        <div>
            <div class="col-md-3">
                <div class="form-group field-pengaduanregistrasi-id_wilayah_konflik has-success">
                    <label class="control-label">Kode</label>
                    <select class="form-control" name="kode_huruf_pengusulan">
                        <option value="">Pilih Kode Pengaduan</option>
                        <?php if (Yii::$app->user->identity->role >= 30) :?>
                            <option value="P">P (Admin Pusat)</option>
                        <?php endif?>
                        <option value="D">D (Admin Daerah)</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>    

    <?php echo $form->field($model, 'nama_pengusul')->textInput() ?>
    
    <?php echo $form->field($model, 'nama_mha')->textInput() ?>

    <?php echo $form->field($model, 'alamat_lengkap_pengusul')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'no_ktp_pengusul')->textInput() ?>

    <?php echo $form->field($model, 'email_pengusul')->textInput() ?>

    <?php echo $form->field($model, 'no_telepon_pengusul')->textInput() ?>

    <?php // echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php // echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>

    <?php echo
    $form->field($model, 'id_wilayah')->dropDownList(
        \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
        'prompt' => 'Pilih Wilayah',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_provinsi").html(data); })',
            ]
    )->label('Wilayah')
    ?>

    <?php echo
    $form->field($model, 'id_provinsi')->dropDownList(
        \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), [
        'prompt' => 'Pilih Provinsi',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_kota_kabupaten").html(data) })'
            ]
    )->label('Provinsi')
    ?>

    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_kota_kabupaten')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['id_provinsi' => $model->id_provinsi])->orWhere(['id_provinsi' => 1])->all(), 'id', 'nama_kota_kabupaten'), [
                'prompt' => 'Pilih Kota / Kabupaten',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_kecamatan").html(data) })'
                    ]
            )->label('Kota / Kabupaten')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kota_kabupaten')->textInput()->label('Lainnya') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_kecamatan')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id_kota_kabupaten' => $model->id_kota_kabupaten])->orWhere(['id_kota_kabupaten' => 1])->all(), 'id', 'nama_kecamatan'), [
                'prompt' => 'Pilih Kecamatan',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_desa_kelurahan").html(data) })'
                    ]
            )->label('Kecamatan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kecamatan')->textInput()->label('Lainnya') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_desa_kelurahan')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['id_kecamatan' => $model->id_kecamatan])->orWhere(['id_kecamatan' => 1])->all(), 'id', 'nama_desa_kelurahan'), [
                'prompt' => 'Pilih Desa Kelurahan'
                    ]
            )->label('Desa / Kelurahan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_desa_kelurahan')->textInput()->label('Lainnya') ?>
        </div>
    </div>
    <?php echo $form->field($model, 'dusun')->textarea() ?>
    <?php echo $form->field($model, 'keterangan')->textarea() ?>
    <?php echo $form->field($model, 'tanggal_pengusulan')->widget(yii\jui\DatePicker::className(), [
        'language' => 'en'
    ]) ?>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'form_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'form_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->form_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->form_filename]) ?>"><?php echo $model->form_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'profil_mha_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'profil_mha_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->profil_mha_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->profil_mha_filename]) ?>"><?php echo $model->profil_mha_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'surat_kuasa_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'surat_kuasa_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->surat_kuasa_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->surat_kuasa_filename]) ?>"><?php echo $model->surat_kuasa_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'surat_pernyataan_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'surat_pernyataan_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->surat_pernyataan_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->surat_pernyataan_filename]) ?>"><?php echo $model->surat_pernyataan_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'produk_hukum_daerah_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'produk_hukum_daerah_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->produk_hukum_daerah_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->produk_hukum_daerah_filename]) ?>"><?php echo $model->produk_hukum_daerah_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'peta_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'peta_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->peta_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->peta_filename]) ?>"><?php echo $model->peta_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'identitas_ketua_adat_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'identitas_ketua_adat_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->identitas_ketua_adat_filename) : ?>
                    <a class="btn bg-yellow" href="<?php echo \yii\helpers\Url::to(['pengusulan-registrasi/download', 'id' => $model->id, 'file' => $model->identitas_ketua_adat_filename]) ?>"><?php echo $model->identitas_ketua_adat_filename ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
