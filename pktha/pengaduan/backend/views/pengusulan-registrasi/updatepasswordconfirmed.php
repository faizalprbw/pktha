<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Update Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengusulan Hutan Adat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <form>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="col-md-4">ID</div>
                <div class="col-md-8"><?= $model->id ?></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="col-md-4">Kode</div>
                <div class="col-md-8"><?= $model->kode ?></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="col-md-4">Key</div>
                <div class="col-md-8"><?= sha1($model->kunci) ?></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="col-md-4">Status</div>
                <div class="col-md-8">
                    <?php
                    if (sha1($model->kunci) === sha1('') || sha1($model->kunci) === sha1(null)) {
                        echo '<a class="label bg-red">Error</a>';
                    } else {
                        echo '<a class="label bg-green">Success</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <?php echo \yii\bootstrap\Html::a('Kembali ke Index Pengusulan', \yii\helpers\Url::to(['pengusulan-registrasi/index']), ['title' => 'Index', 'class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </form>
</div>