<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanHutanAdat */
/* @var $form yii\widgets\ActiveForm */

/**
 * Please refer JS file to js/pengusulan-hutan-adat/script.js
 */
?>

<div class="pengusulan-hutan-adat-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'kode_pengusulan')->textInput(['disabled' => true]) ?>

    <?php echo $form->field($model, 'nama_pengusul')->textInput() ?>
    
    <?php echo $form->field($model, 'nama_mha')->textInput() ?>

    <?php echo $form->field($model, 'no_ktp_pengusul')->textInput() ?>

    <?php echo $form->field($model, 'alamat_lengkap_pengusul')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'email_pengusul')->textInput() ?>

    <?php echo $form->field($model, 'no_telepon_pengusul')->textInput() ?>

    <?php // echo $form->field($model, 'created_date')->textInput(['disabled' => true]) ?>

    <?php // echo $form->field($model, 'modified_date')->textInput(['disabled' => true]) ?>
    
    <?php echo $form->field($model, 'tanggal_pengusulan')->widget(yii\jui\DatePicker::className(), [
      'language' => 'en'
    ]) ?>

    <?php
    echo
    $form->field($model, 'id_wilayah')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
        'prompt' => 'Pilih Wilayah',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengusulanhutanadat-id_provinsi").html(data); })',
            ]
    )->label('Wilayah')
    ?>

    <?php
    echo
    $form->field($model, 'id_provinsi')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->all(), 'id', 'nama_provinsi'), [
        'prompt' => 'Pilih Provinsi',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengusulanhutanadat-id_kota_kabupaten").html(data) })'
            ]
    )->label('Provinsi')
    ?>

    <div class="row">
        <div class="col-md-5">
            <?php
            echo
            $form->field($model, 'id_kota_kabupaten')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['id_provinsi' => $model->id_provinsi])->orWhere(['id_provinsi' => 1])->all(), 'id', 'nama_kota_kabupaten'), [
                'prompt' => 'Pilih Kota / Kabupaten',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengusulanhutanadat-id_kecamatan").html(data) })'
                    ]
            )->label('Kota / Kabupaten')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kota_kabupaten')->textInput()->label('Lainnya') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?php
            echo
            $form->field($model, 'id_kecamatan')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id_kota_kabupaten' => $model->id_kota_kabupaten])->orWhere(['id_kota_kabupaten' => 1])->all(), 'id', 'nama_kecamatan'), [
                'prompt' => 'Pilih Kecamatan',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengusulanhutanadat-id_desa_kelurahan").html(data) })'
                    ]
            )->label('Kecamatan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_kecamatan')->textInput()->label('Lainnya') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?php
            echo
            $form->field($model, 'id_desa_kelurahan')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['id_kecamatan' => $model->id_kecamatan])->orWhere(['id_kecamatan' => 1])->all(), 'id', 'nama_desa_kelurahan'), [
                'prompt' => 'Pilih Desa Kelurahan'
                    ]
            )->label('Desa / Kelurahan')
            ?>
        </div>
        <div class="col-md-5">
            <?php echo $form->field($model, 'teks_desa_kelurahan')->textInput()->label('Lainnya') ?>
        </div>
    </div>

    <?php echo $form->field($model, 'dusun')->textarea() ?>
    <?php echo $form->field($model, 'keterangan')->textarea() ?>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'form_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'form_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->form_filename) : ?>
                    <?php echo Html::a($model->form_filename, ['download', 'id' => $model->id, 'file' => 'form'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'profil_mha_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'profil_mha_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->profil_mha_filename) : ?>
                    <?php echo Html::a($model->profil_mha_filename, ['download', 'id' => $model->id, 'file' => 'profil_mha'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'surat_kuasa_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'surat_kuasa_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->surat_kuasa_filename) : ?>
                    <?php echo Html::a($model->surat_kuasa_filename, ['download', 'id' => $model->id, 'file' => 'surat_kuasa'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'surat_pernyataan_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'surat_pernyataan_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->surat_pernyataan_filename) : ?>
                    <?php echo Html::a($model->surat_pernyataan_filename, ['download', 'id' => $model->id, 'file' => 'surat_pernyataan'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'produk_hukum_daerah_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'produk_hukum_daerah_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->produk_hukum_daerah_filename) : ?>
                    <?php echo Html::a($model->produk_hukum_daerah_filename, ['download', 'id' => $model->id, 'file' => 'produk_hukum_daerah'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'peta_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'peta_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->peta_filename) : ?>
                    <?php echo Html::a($model->peta_filename, ['download', 'id' => $model->id, 'file' => 'peta'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-3">
            <?php echo $form->field($model, 'identitas_ketua_adat_filename')->textInput(['disabled' => true]) ?>
        </div>
    </div>

    <?php echo $form->field($model, 'identitas_ketua_adat_data')->fileInput() ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php if ($model->identitas_ketua_adat_filename) : ?>
                    <?php echo Html::a($model->identitas_ketua_adat_filename, ['download', 'id' => $model->id, 'file' => 'identitas_ketua_adat'], ['class' => 'btn bg-yellow']) ?>
                <?php endif ?>
            </div>
        </div>
    </div>

    <!-- Checkbox for Finished & Dialihkan Permohonan -->
    <div class="form-group">
        <label>
            <?php
            $permohonanModel = common\models\PengusulanPermohonan::findOne(['id_pengusulan' => $model->id]);
            if (!isset($permohonanModel) || $permohonanModel->dikembalikan) {
                echo '<input type="checkbox" id="permohonan-dikembalikan" name="permohonan_dikembalikan" value="1" checked>';
            } else {
                echo '<input type="checkbox" id="permohonan-dikembalikan" name="permohonan_dikembalikan" value="1">';
            }
            ?>
            Dikembalikan
        </label>
        <div class="help-block"></div>
    </div>
    <div class="form-group">
        <label>
            <?php
            if (!isset($permohonanModel) || $permohonanModel->finished) {
                echo '<input type="checkbox" id="permohonan-finished" name="permohonan_finished" value="1" checked>';
            } else {
                echo '<input type="checkbox" id="permohonan-finished" name="permohonan_finished" value="1">';
            }
            ?>
            Finished
        </label>
        <div class="help-block"></div>
    </div>
    <!-- END -->


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app', 'Cancel'), ['cancel', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
