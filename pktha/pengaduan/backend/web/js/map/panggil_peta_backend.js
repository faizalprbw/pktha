
var map = L.map('map', {
    zoomControl: false,
    maxZoom: 20,
    minZoom: 5
}).setView([0.39550467153201946, 116.89453125000001], 5);

map.setMaxBounds([[8.100892668623654, 94.13085937500001], [-10.34033830684679, 142.47070312500003]]);

var zoom_bar = new L.Control.ZoomBar({position: 'topleft'}).addTo(map);

L.control.betterscale({
    imperial: false,
    metric: true
}).addTo(map);

var assesmen_icon = new L.Icon({
    iconUrl: 'img/map/assesmen.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var pra_mediasi_icon = new L.Icon({
    iconUrl: 'img/map/pra_mediasi.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var mediasi_icon = new L.Icon({
    iconUrl: 'img/map/mediasi.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var drafting_mou_icon = new L.Icon({
    iconUrl: 'img/map/drafting_mou.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var tanda_tangan_mou_icon = new L.Icon({
    iconUrl: 'img/map/tanda_tangan_mou.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var belum_ditangani_icon = new L.Icon({
    iconUrl: 'img/map/belum_ditangani.png',
    iconSize: [40, 41],
    iconAnchor: [20, 41],
    popupAnchor: [1, -34],
    shadowSize: [60, 41]
});

var ditangani_icon = new L.Icon({
    iconUrl: 'img/map/ditangani.png',
    iconSize: [40, 41],
    iconAnchor: [20, 41],
    popupAnchor: [1, -34],
    shadowSize: [60, 41]
});



var google_hybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_streetmap = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_terrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_satellite = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var iconLayersControl = new L.Control.IconLayers(
        [
            {
                title: 'Google Hybrid',
                layer: google_hybrid,
                icon: 'img/map/google_hybrid2.png'
            },
			{
                title: 'Google Streets',
                layer: google_streetmap,
                icon: 'img/map/Google_Street.png'
            },
            {
                title: 'Google Satellite',
                layer: google_satellite,
                icon: 'img/map/esri_Satellite.png'
            },
            {
                title: 'Google Terrain',
                layer: google_terrain,
                icon: 'img/map/google_terrain.png'
            },
        ], {
    position: 'topright',
    maxLayersInRow: 5
}
);

iconLayersControl.addTo(map);

var mcg = L.markerClusterGroup().addTo(map);

var lokasiSub = L.featureGroup.subGroup(mcg).addTo(map);
var kasus_2017Sub = L.featureGroup.subGroup(mcg);
var kasus_2016Sub = L.featureGroup.subGroup(mcg);


var stripes = new L.StripePattern({
    color: '#1E90FF',
    opacity: 1.0,
    angle: 135
});
stripes.addTo(map);

var greenstripes = new L.StripePattern({
    color: '#3CEF06',
    opacity: 1.0,
    angle: 135
});
greenstripes.addTo(map);

var shape = new L.PatternRect({
    width: 20,
    height: 20,
    color: 'red',
    opacity: 0.4,
    fill: false
});

var pattern = new L.Pattern({width: 20, height: 20, angle: 45});
pattern.addShape(shape);
pattern.addTo(map);


var lokasi = L.geoJson(null, {

    pointToLayer: function (feature, latlng) {
        if (feature.properties.finished_assesmen === false || feature.properties.finished_assesmen === null) {
            return L.marker(latlng, {icon: assesmen_icon});
        }
        if (feature.properties.finished_pra_mediasi === false || feature.properties.finished_pra_mediasi === null) {
            return L.marker(latlng, {icon: pra_mediasi_icon});
        }
        if (feature.properties.finished_mediasi === false || feature.properties.finished_mediasi === null) {
            return L.marker(latlng, {icon: mediasi_icon});
        }
        if (feature.properties.finished_drafting_mou === false || feature.properties.finished_drafting_mou === null) {
            return L.marker(latlng, {icon: drafting_mou_icon});
        }
        if (feature.properties.finished_tanda_tangan_mou === false || feature.properties.finished_tanda_tangan_mou === null) {
            return L.marker(latlng, {icon: tanda_tangan_mou_icon});
        }
        if (feature.properties.finished_assesmen === true && feature.properties.finished_pra_mediasi === true && feature.properties.finished_mediasi === true && feature.properties.finished_drafting_mou === true && feature.properties.finished_tanda_tangan_mou === true) {
            return L.marker(latlng, {icon: ditangani_icon});
        }
    },

    onEachFeature: function (feature, layer) {
		var selesai_assesmen = feature.properties.finished_assesmen;
		var selesai_pra_mediasi = feature.properties.finished_pra_mediasi;
		var selesai_mediasi = feature.properties.finished_mediasi;
		var selesai_drafting_mou = feature.properties.finished_drafting_mou;
		var selesai_tanda_tangan_mou = feature.properties.finished_tanda_tangan_mou;
		var proses_pengaduan = null;
				
			if (selesai_tanda_tangan_mou === false || selesai_tanda_tangan_mou === null) {
					var proses_pengaduan = 'Tahap Tanda Tangan MOU';
			}
			if (selesai_drafting_mou === false || selesai_drafting_mou === null) {
					var proses_pengaduan = 'Tahap Drafting MOU';
			}
			if (selesai_mediasi === false || selesai_mediasi === null) {
					var proses_pengaduan = 'Tahap Mediasi';
			}			
			if (selesai_pra_mediasi === false || selesai_pra_mediasi === null) {
					var proses_pengaduan = 'Tahap Pra Mediasi';
			}			
			if (selesai_assesmen === false || selesai_assesmen === null) {
					var proses_pengaduan = 'Tahap Assesmen';
			}			
			if (selesai_tanda_tangan_mou === true) {
					var proses_pengaduan = 'Selesei Diproses';
			}
		
		var nama_desa_kelurahan = feature.properties.nama_desa_kelurahan;
		var nama_kecamatan = feature.properties.nama_kecamatan;
		var nama_kota_kabupaten = feature.properties.nama_kota_kabupaten;
		var nama_provinsi = feature.properties.nama_provinsi;
		var teks_desa_kelurahan = feature.properties.teks_desa_kelurahan_konflik;
		var teks_kecamatan = feature.properties.teks_kecamatan_konflik;
		var teks_kota_kabupaten = feature.properties.teks_kota_kabupaten_konflik;
		var teks_provinsi = feature.properties.teks_provinsi_konflik;
		var desa_kelurahan = null;
		var kecamatan = null;
		var kota_kabupaten = null;
		var provinsi = null;
		var pihak_berkonflik_1 = feature.properties.pihak_berkonflik_1;
		var pihak_berkonflik_2 = feature.properties.pihak_berkonflik_2;
		var pihak_berkonflik_3 = feature.properties.pihak_berkonflik_3;
		var pihak_konflik_1 = null;
		var pihak_konflik_2 = null;
		var pihak_konflik_3 = null;
		
			if (nama_desa_kelurahan === null) {
				var desa_kelurahan = teks_desa_kelurahan;
			} else {
				var desa_kelurahan = nama_desa_kelurahan;
			}
			if (nama_kecamatan === null) {
				var kecamatan = teks_kecamatan;
			} else {
				var kecamatan = nama_kecamatan;
			}
			if (nama_kota_kabupaten === null) {
				var kota_kabupaten = teks_kota_kabupaten;
			} else {
				var kota_kabupaten = nama_kota_kabupaten;
			}			
			if (nama_provinsi === null) {
				var provinsi = teks_provinsi;
			} else {
				var provinsi = nama_provinsi;
			}
			if (pihak_berkonflik_1 === null) {
				var pihak_berkonflik_1 = " ";
			} 
			if (pihak_berkonflik_2 === null) {
				var pihak_berkonflik_2 = " ";
			}
			if (pihak_berkonflik_3 === null) {
				var pihak_berkonflik_3 = " ";
			}
			
        layer.bindPopup('<h5>Keterangan</h5>' +
                '<table style="font-size:95%" width="400">' +
                '<tr><td width="150">Lokasi Konflik</td><td>:</td><td width="250">' + desa_kelurahan + ', ' + kecamatan + ', ' + kota_kabupaten + ', ' + provinsi + ' </tr></td>' +
                '<tr><td width="150">Tipologi Konflik</td><td>:</td><td width="250">' + feature.properties.nama_konflik + '</tr></td>' +
                '<tr><td width="150">Pihak Berkonflik</td><td>:</td><td width="250">' + pihak_berkonflik_1 + '-' + pihak_berkonflik_2 + '-' + pihak_berkonflik_3 + ' </tr></td>' +
                '<tr><td width="150">Proses Pengaduan</td><td>:</td><td width="250">' + proses_pengaduan + '</tr></td>' +
				'</table>', {maxWidth: 450});
    }

});

$.getJSON(link_lokasi_pengaduan, function (data) {
lokasi.addData(data);
lokasi.eachLayer(function (layer) {
layer.addTo(lokasiSub);
});
});

lokasiSub.addTo(map);

var kasus_2017 = L.geoJson(null, {

    pointToLayer: function (feature, latlng) {
        if (feature.properties.proses === "Ditangani") {
            return L.marker(latlng, {icon: ditangani_icon});
        } else {
            return L.marker(latlng, {icon: belum_ditangani_icon});
        }
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h5>' + feature.properties.kasus + '</h5>' +
                '<table style="font-size:95%" width="400">' +
                '<tr><td width="150">Lokasi Konflik</td><td>:</td><td width="150">' + feature.properties.lokasi + '</tr></td>' +
                '<tr><td width="150">Tipologi Konflik</td><td>:</td><td width="150">' + feature.properties.tipologi + '</tr></td>' +
                '<tr><td width="150">Keterangan</td><td>:</td><td width="350">' + feature.properties.keterangan + '</tr></td>' +
                '<tr><td width="150">BPSKL</td><td>:</td><td width="250">' + feature.properties.bpskl + '</tr></td>' +
                '<tr><td width="150">Proses</td><td>:</td><td width="250">' + feature.properties.proses + '</tr></td>' +
                '</table>', {maxWidth: 400});

    }


});




var kasus_2016 = L.geoJson(null, {

    pointToLayer: function (feature, latlng) {
        if (feature.properties.proses === "Ditangani") {
            return L.marker(latlng, {icon: ditangani_icon});
        } else {
            return L.marker(latlng, {icon: belum_ditangani_icon});
        }
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h5>' + feature.properties.kasus + '</h5>' +
                '<table style="font-size:95%" width="400">' +
                '<tr><td width="150">Lokasi Konflik</td><td>:</td><td width="150">' + feature.properties.lokasi + '</tr></td>' +
                '<tr><td width="150">Tipologi Konflik</td><td>:</td><td width="150">' + feature.properties.tipologi + '</tr></td>' +
                '<tr><td width="150">Keterangan</td><td>:</td><td width="350">' + feature.properties.keterangan + '</tr></td>' +
                '<tr><td width="150">BPSKL</td><td>:</td><td width="250">' + feature.properties.bpskl + '</tr></td>' +
                '<tr><td width="150">Proses</td><td>:</td><td width="250">' + feature.properties.proses + '</tr></td>' +
                '</table>', {maxWidth: 400});

    }


});





var pencantuman_penetapan_HA = L.featureGroup();
var pencantumanSub = L.featureGroup.subGroup(pencantuman_penetapan_HA).addTo(map);
var penetapanSub = L.featureGroup.subGroup(pencantuman_penetapan_HA).addTo(map);

var potensi_hutan_adat = L.geoJson(null, {

    style: function (feature) {
        return {fillPattern: stripes,
            color: '#1E90FF',
            opacity: 1.0}
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h4>Keterangan :</h4>' +
                '<table style="font-size:95%" width="300">' +
                '<tr><td width="150">Wilayah</td><td>:</td><td width="150">' + feature.properties.wilayah_ad + '</tr></td>' +
                '<tr><td width="150">Provinsi</td><td>:</td><td width="150">' + feature.properties.prov + '</tr></td>' +
                '<tr><td width="150">Kabupaten / Kota</td><td>:</td><td width="150">' + feature.properties.kab + '</tr></td>' +
                '<tr><td width="150">Luas</td><td>:</td><td width="150">' + feature.properties.luas_ha + ' ha</tr></td>' +
                '</table>', {maxWidth: 300});

    }


});



var pencantuman_hutan_adat = L.geoJson(null, {

    style: function (feature) {
        return {fillPattern: greenstripes,
            color: '#3CEF06',
            opacity: 1.0}
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h4>Keterangan Hutan Adat</h4>' +
                '<table style="font-size:100%" width="300">' +
                '<tr><td width="100">Kode Provinsi</td><td>:</td><td width="200">' + feature.properties.kode_prov + '</tr></td>' +
                '<tr><td>Masyarakat Adat</td><td>:</td><td width="200">' + feature.properties.masy_adat + '</tr></td>' +
                '<tr><td>Fungsi Kawasan</td><td>:</td><td width="200">' + feature.properties.fungsi_kws + '</tr></td>' +
                '<tr><td>SK Adat</td><td>:</td><td width="200">' + feature.properties.sk_adat + '</tr></td>' +
                '<tr><td>Tanggal Adat</td><td>:</td><td width="200">' + feature.properties.tgl_adat + '</tr></td>' +
                '<tr><td>Luas Hutan Adat</td><td>:</td><td width="200">' + feature.properties.ls_adat + '</tr></td>' +
                '<tr><td>Keternagan</td><td>:</td><td width="200">' + feature.properties.keterangan + '</tr></td>' +
                '</table>', {maxWidth: 300});

    }


});



function getColor(d) {
    return d > 30 ? '#800026' :
            d > 25 ? '#BD0026' :
            d > 20 ? '#E31A1C' :
            d > 15 ? '#FC4E2A' :
            d > 10 ? '#FD8D3C' :
            d > 5 ? '#FEB24C' :
            d >= 1 ? '#FED976' :
            '#FFFFFF';
}

function getColor2(d) {
    return d > 20 ? '#800026' :
            d > 15 ? '#BD0026' :
            d > 12 ? '#E31A1C' :
            d > 9 ? '#FC4E2A' :
            d > 6 ? '#FD8D3C' :
            d > 3 ? '#FEB24C' :
            d >= 1 ? '#FED976' :
            '#FFFFFF';
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();

        info.update(layer.feature.properties);
    }
}

function highlightFeature2(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();

        info2.update(layer.feature.properties);
    }
}

function resetHighlight(e) {
    produk_hukum_provinsi.resetStyle(e.target);
    info.update();
}

function resetHighlight2(e) {
    produk_hukum_kabupaten.resetStyle(e.target);
    info2.update();
}

var produk_hukum = L.featureGroup();
var produk_kabupaten = L.featureGroup.subGroup(produk_hukum);
var produk_provinsi = L.featureGroup.subGroup(produk_hukum);

var produk_hukum_provinsi = L.geoJson(null, {

    style: function (feature) {
        return {fillColor: getColor(feature.properties.total_provinsi),
            opacity: 1,
            weight: 2,
            dashArray: '3',
            color: 'black',
            fillOpacity: 0.7}
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h5>Provinsi ' + feature.properties.provinsi + ':</h5>' +
                '<table style="font-size:95%" width="250">' +
                '<tr><td width="175">Instruksi Gubernur</td><td>: </td><td width="75">' + feature.properties.instruksi_gubernur + ' Produk</tr></td>' +
                '<tr><td width="175">Keputusan Bersama</td><td>: </td><td width="75">' + feature.properties.keputusan_bersama + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Daerah</td><td>: </td><td width="75">' + feature.properties.peraturan_daerah + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Daerah Provinsi</td><td>: </td><td width="75">' + feature.properties.peraturan_daerah_provinsi + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Gubernur</td><td>: </td><td width="75">' + feature.properties.peraturan_gubernur + ' Produk</tr></td>' +
                '<tr><td width="175" style="font-weight:bold" "font-size:100%">Total Produk (Tingkat Provinsi)</td><td>: </td><td width="75" style="font-weight:bold" "font-size:100%">' + feature.properties.total_provinsi + ' Produk</tr></td>' +
                '</table>', {maxWidth: 300});

        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight
        });
    }


});

var produk_hukum_kabupaten = L.geoJson(null, {

    style: function (feature) {
        return {fillColor: getColor2(feature.properties.total_kabupaten),
            opacity: 1,
            weight: 2,
            dashArray: '3',
            color: 'black',
            fillOpacity: 0.7}
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h5>Kabupaten ' + feature.properties.kabupaten + ':</h5>' +
                '<table style="font-size:95%" width="250">' +
                '<tr><td width="175">Keputusan Bupati</td><td>: </td><td width="75">' + feature.properties.keputusan_bupati + ' Produk</tr></td>' +
                '<tr><td width="175">Keputusan Walikota</td><td>: </td><td width="75">' + feature.properties.keputusan_walikota + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Daerah</td><td>: </td><td width="75">' + feature.properties.peraturan_daerah + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Daerah Kabupaten</td><td>: </td><td width="75">' + feature.properties.peraturan_daerah_kabupaten + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Daerah Kota</td><td>: </td><td width="75">' + feature.properties.peraturan_daerah_kota + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Bupati</td><td>: </td><td width="75">' + feature.properties.peraturan_bupati + ' Produk</tr></td>' +
                '<tr><td width="175">Peraturan Walikota</td><td>: </td><td width="75">' + feature.properties.peraturan_walikota + ' Produk</tr></td>' +
                '<tr><td width="175" style="font-weight:bold" "font-size:100%">Total Produk (Kabupaten/Kota)</td><td>: </td><td width="75" style="font-weight:bold" "font-size:100%">' + feature.properties.total_kabupaten + ' Produk</tr></td>' +
                '</table>', {maxWidth: 300});

        layer.on({
            mouseover: highlightFeature2,
            mouseout: resetHighlight2
        });
    }


});



var info = L.control({position: 'topleft'});

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};

var info2 = L.control({position: 'topleft'});

info2.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info2');
    this.update();
    return this._div;
};


info.update = function (props) {
    this._div.innerHTML = '<h4>Total Produk Hukum</h4>' + (props ?
            '<b>' + props.provinsi + '</b><br />' + props.total_provinsi + ' Produk Hukum'
            : 'Arahkan mouse pada lokasi');
};

info2.update = function (props) {
    this._div.innerHTML = '<h4>Total Produk Hukum</h4>' + (props ?
            '<b>' + props.kabupaten + '</b><br />' + props.total_kabupaten + ' Produk Hukum'
            : 'Arahkan mouse pada lokasi');
};

var legend = L.control({position: 'topleft'});
var khLegend = L.control({position: 'bottomleft'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
            grades = [1, 5, 10, 15, 20, 25, 30],
            labels = [];


    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
                '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
                grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};

var legend2 = L.control({position: 'topleft'});

legend2.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 1, 3, 6, 9, 12, 15],
            labels = [];


    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
                '<i style="background:' + getColor2(grades[i] + 1) + '"></i> ' +
                grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};
var kawasan_hutan = L.esri.tiledMapLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/KLHK/Kawasan_Hutan_0917/MapServer"

});

var hutan_adat = L.esri.featureLayer({
    url: 'http://geoportal.menlhk.go.id/arcgis/rest/services/test/Penetapan_Hutan_Adat2/FeatureServer/0',
    style: function (feature) {
        return {fillPattern: greenstripes,
            color: '#3CEF06',
            opacity: 1.0}
    }

}).addTo(penetapanSub);


hutan_adat.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Hutan Adat</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Kode Provinsi</td><td>:</td><td>{kode_prov}</tr></td>' +
            '<tr><td>Masyarakat Adat</td><td>:</td><td>{masy_adat}</tr></td>' +
            '<tr><td>Fungsi Kawasan</td><td>:</td><td>{fungsi_kws}</tr></td>' +
            '<tr><td>SK Adat</td><td>:</td><td>{sk_adat}</tr></td>' +
            '<tr><td>Tanggal Adat</td><td>:</td><td>{tgl_adat}</tr></td>' +
            '<tr><td>Luas Hutan Adat</td><td>:</td><td>{ls_adat}</tr></td>' +
            '<tr><td>Keternagan</td><td>:</td><td>{keterangan}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sebaran_konflik = L.layerGroup();
var jabalnus = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Jabalnus2/FeatureServer/0",
    style: function (feature) {
        return {fillPattern: pattern,
            color: 'red',
            opacity: 0.7};
    }
}).addTo(sebaran_konflik);

jabalnus.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var kalimantan = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Kalimantan2/FeatureServer/0",
    style: function (feature) {
        return {fillPattern: pattern,
            color: 'red',
            opacity: 0.7};
    }
}).addTo(sebaran_konflik);

kalimantan.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var maluku_papua = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Maluku_Papua2/FeatureServer/0",
    style: function (feature) {
        return {fillPattern: pattern,
            color: 'red',
            opacity: 0.7};
    }
}).addTo(sebaran_konflik);

maluku_papua.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sulawesi = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sulawesi2/FeatureServer/0",
    style: function (feature) {
        return {fillPattern: pattern,
            color: 'red',
            opacity: 0.7};
    }
}).addTo(sebaran_konflik);

sulawesi.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sumatra = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sumatera2/FeatureServer/0",
    style: function (feature) {
        return {fillPattern: pattern,
            color: 'red',
            opacity: 0.7};
    }
}).addTo(sebaran_konflik);

sumatra.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

/*var overlayLayers = {
    "<span style='color: black; font-size:13px;'>Data Konflik</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Lokasi Pengaduan</span>": lokasiSub,
        "<span style='color: black; font-size:12px; font-weight:normal;'>Data Pengaduan</span>": kasus_2017Sub,
        "<span style='color: black; font-size:12px; font-weight:normal;'>Indikatif Potensi Konflik</span>": sebaran_konflik
    },
    "<span style='color: black; font-size:13px;'>Data Hutan Adat</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Penetapan dan Pencantuman Hutan Adat</span>": pencantuman_penetapan_HA,
		"<span style='color: black; font-size:12px; font-weight:normal;'>Potensi Hutan Adat</span>": potensi_hutan_adat
    },
    "<span style='color: black; font-size:13px;'>Data Kawasan Hutan</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Fungsi Kawasan Hutan</span>": kawasan_hutan,
        "<span style='color: black; font-size:12px; font-weight:normal;'>Produk Hukum Daerah</span>": produk_hukum
    }
};*/

var overlayLayers = {
    "<span style='color: black; font-size:13px;font-weight:bold;'>Data Konflik</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Lokasi Pengaduan</span>": lokasiSub
    },
    "<span style='color: black; font-size:13px;font-weight:bold;'>Data Hutan Adat</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Penetapan dan Pencantuman Hutan Adat</span>": pencantuman_penetapan_HA
    },
    "<span style='color: black; font-size:13px;font-weight:bold;'>Data Kawasan Hutan</span>": {
        "<span style='color: black; font-size:12px; font-weight:normal;'>Fungsi Kawasan Hutan</span>": kawasan_hutan,
        "<span style='color: black; font-size:12px; font-weight:normal;'>Produk Hukum Daerah</span>": produk_hukum
    }
};

var options = {
    collapsed: true,
    groupCheckboxes: true
};

L.control.groupedLayers(null, overlayLayers).addTo(map);

// Removing geolocation feature since it requires through HTTPS connection. It does not necessary to be added into that web.
// var lc = L.control.locate({
//     position: 'topleft',
//     icon: 'fa fa-map-marker fa-2x',
//     iconLoading: 'fa fa-spinner fa-spin fa-2x',
//     locateOptions: {
//         enableHighAccuracy: true
//     },
//     strings: {
//         title: "Cari Lokasi Anda Saat Ini",
//         popup: "Anda Berada Disekitar Sini",
//         outsideMapBoundsMsg: "Anda Berada di Luar Wilayah Negara Republik Indonesia"
//     }}).addTo(map);

map.on('overlayadd', function (eventLayer) {
    if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Produk Hukum Daerah</span>") {
        for (var i = 1; i <= 34; i++) {
			$.getJSON(link_produk_hukum_provinsi + '&id=' + i, function (data) {
			produk_hukum_provinsi.addData(data);
			produk_hukum_provinsi.eachLayer(function (layer) {
				layer.addTo(produk_provinsi);
			});
			});
			};
		for (var i = 1; i <= 34; i++) {
			$.getJSON(link_produk_hukum_kabupaten + '&id=' + i, function (data) {
			produk_hukum_kabupaten.addData(data);
			produk_hukum_kabupaten.eachLayer(function (layer) {
			layer.addTo(produk_kabupaten);
			});
			});
			};
		if (map.getZoom() < 7) {
			map.addLayer(produk_provinsi);
            legend.addTo(this);
            info.addTo(this);
        }
        if (map.getZoom() >= 7) {
            map.addLayer(produk_kabupaten);
            legend2.addTo(this);
            info2.addTo(this);
        }
    }
	
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Potensi Hutan Adat</span>") {
        for (var i = 1; i <= jumlah_potensi; i++) {
			$.getJSON(link_potensi_hutan_adat + '&id=' + i, function (data) {
				potensi_hutan_adat.addData(data);
			});
		}
	}
	
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Penetapan dan Pencantuman Hutan Adat</span>") {
        for (var i = 1; i <= jumlah_pencantuman; i++) {
			$.getJSON(link_pencantuman_hutan_adat + '&id=' + i, function (data) {
				pencantuman_hutan_adat.addData(data);
					pencantuman_hutan_adat.eachLayer(function (layer) {
						layer.addTo(pencantumanSub);
					});
			});
		}
	}
	
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Data Pengaduan</span>") {
		$.getJSON(link_kasus2016, function (data) {
			kasus_2016.addData(data);
				kasus_2016.eachLayer(function (layer) {
					layer.addTo(kasus_2017Sub);
				});
		});
		$.getJSON(link_kasus2017, function (data) {
			kasus_2017.addData(data);
				kasus_2017.eachLayer(function (layer) {
					layer.addTo(kasus_2017Sub);
				});
		});
	}

    if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Fungsi Kawasan Hutan</span>") {
        khLegend.onAdd = function (map) {
                var div = L.DomUtil.create('div', 'info-legend'),
                labels = [];

                $.ajax({
                    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/KLHK/Kawasan_Hutan_0917/MapServer/legend",
                    data: {f: 'pjson'},
                    dataType: 'jsonp',
                    success: function (data) {
                        if (typeof data.layers[0].legend != 'undefined') {
                            for (var i = 0, len = data.layers.length; i < len; i ++) {
                                div.innerHTML += '<strong style="font-size: 14px">' + data.layers[i].layerName + '</strong></br>';
                                for (var j = 0, jj = data.layers[0].legend.length; j < jj; j++) {
                                        div.innerHTML += L.Util.template('<img width="{width}" height="{height}" src="data:{contentType};base64,{imageData}"><span>{label}</span></br>', data.layers[i].legend[j]);
                                    }
                            }
                        } else {
                            div.innerHTML += '<strong style="font-size: 14px">' + "Tidak ada legenda" + '</strong></br>';
                        }
                    }
                });
                return div;
            };
        khLegend.addTo(map);
    }
});

map.on('overlayremove', function (eventLayer) {
    if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Produk Hukum Daerah</span>") {
        if (map.getZoom() < 7) {
			produk_hukum_provinsi.clearLayers();
			produk_provinsi.clearLayers();
            map.removeLayer(produk_provinsi);
            this.removeControl(legend);
            this.removeControl(info);
        }
        if (map.getZoom() >= 7) {
            produk_hukum_kabupaten.clearLayers();
			produk_kabupaten.clearLayers();
			map.removeLayer(produk_kabupaten);
            this.removeControl(legend2);
            this.removeControl(info2);
        }
    }
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Data Pengaduan</span>") {
		kasus_2016.clearLayers();
		kasus_2017.clearLayers();
		kasus_2017Sub.clearLayers();
	}
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Penetapan dan Pencantuman Hutan Adat</span>") {
		pencantuman_hutan_adat.clearLayers();
		pencantumanSub.clearLayers();
	}
	if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Potensi Hutan Adat</span>") {
		potensi_hutan_adat.clearLayers();
	}
    if (eventLayer.name === "<span style='color: black; font-size:12px; font-weight:normal;'>Fungsi Kawasan Hutan</span>")
    {
        this.removeControl(khLegend);
    }
	
});

map.on('zoomend', function () {
    if (map.getZoom() >= 7 && map.hasLayer(produk_hukum)) {
        if (map.hasLayer(produk_provinsi)) {
            map.removeLayer(produk_provinsi);
            map.addLayer(produk_kabupaten);
            this.removeControl(legend);
            this.removeControl(info);
            legend2.addTo(map);
            info2.addTo(map);
        }
    }
    if (map.getZoom() < 7 && map.hasLayer(produk_hukum)) {
        if (map.hasLayer(produk_kabupaten)) {
			map.removeLayer(produk_kabupaten);
            map.addLayer(produk_provinsi);
            this.removeControl(legend2);
            this.removeControl(info2);
            legend.addTo(map);
            info.addTo(map);
        }
    }
});



//buat peta indeks nanti
//var google_streetmap2 = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
   // maxZoom: 13,
	//minZoom: 1,
    //subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
//});
//var miniMap = new L.Control.MiniMap(google_streetmap2, {
	//width: 100,
	//height: 100,
	//toggleDisplay: true
//}).addTo(map);
