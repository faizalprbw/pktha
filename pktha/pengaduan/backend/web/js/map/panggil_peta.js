var map = L.map('map', {
zoomControl: false,
        maxZoom: 19,
        minZoom: 5
}).setView([0.301, 118.821], 5);
        map.setMaxBounds([[8.100892668623654, 94.13085937500001], [ - 10.34033830684679, 142.47070312500003]]);
        var zoom_bar = new L.Control.ZoomBar({position: 'topleft'}).addTo(map);
        var merah = new L.Icon({
        iconUrl: 'img/map/marker1.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, - 34],
                shadowSize: [41, 41]
        });
        var kuning = new L.Icon({
        iconUrl: 'img/map/marker4.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, - 34],
                shadowSize: [41, 41]
        });
        var hijau = new L.Icon({
        iconUrl: 'img/map/marker5.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, - 34],
                shadowSize: [41, 41]
        });
        var orange = new L.Icon({
        iconUrl: 'img/map/marker2.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, - 34],
                shadowSize: [41, 41]
        });
        var google_hybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
        maxZoom: 20,
                subdomains:['mt0', 'mt1', 'mt2', 'mt3']
        });
        var google_streetmap = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        maxZoom: 20,
                subdomains:['mt0', 'mt1', 'mt2', 'mt3']
        });
        var google_terrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
                subdomains:['mt0', 'mt1', 'mt2', 'mt3']
        });
        var google_satellite = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
                subdomains:['mt0', 'mt1', 'mt2', 'mt3']
        });
        var iconLayersControl = new L.Control.IconLayers(
        [
        {
        title: 'Google Streets',
                layer: google_streetmap,
                icon: 'img/map/Google_Street.png'
        },
        {
        title: 'Google Satellite',
                layer: google_satellite,
                icon: 'img/map/esri_Satellite.png'
        },
        {
        title: 'Google Hybrid',
                layer: google_hybrid,
                icon: 'img/map/google_hybrid2.png'
        },
        {
        title: 'Google Terrain',
                layer: google_terrain,
                icon: 'img/map/google_terrain.png'
        },
        ], {
        position: 'topright',
                maxLayersInRow: 5
        }
        );
        iconLayersControl.addTo(map);
        //var markers = L.markerClusterGroup();

        //var assesmen = $.getJSON("http://localhost/PKTHA/pengaduan/backend/views/site/pgsql_query_2.php", function(data) {
        //var lokasi_pengaduan = L.geoJson(data, {
        //pointToLayer: function (feature, latlng) {
        //return L.marker (latlng, {icon: merah});
        //},
        onEachFeature: function (feature, layer) {
        layer.bindPopup('<h4>' + feature.properties.kasus + '</h4>' +
                '<table style="font-size:95%" width="250">' +
                '<tr><td width="100">Tipologi Konflik</td><td>:</td><td width="150">' + feature.properties.Pengadu + '</tr></td>' +
                '<tr><td>Proses Kasus</td><td>:</td><td>' + feature.properties.Alamat + '</tr></td>' +
                '</table>');
        }
});
        markers.addLayer(lokasi_pengaduan);
        map.addLayer(markers);
        var kawasan_hutan = L.esri.tiledMapLayer({
        url: "http://139.255.83.75:6080/arcgis/rest/services/Forest/Kawasan_Hutan/MapServer/"
        });
        var hutan_adat = L.esri.featureLayer({
        url: 'http://geoportal.menlhk.go.id/arcgis/rest/services/test/Penetapan_Hutan_Adat2/FeatureServer/0'
        });
        hutan_adat.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Hutan Adat</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Kode Provinsi</td><td>:</td><td>{kode_prov}</tr></td>' +
                '<tr><td>Masyarakat Adat</td><td>:</td><td>{masy_adat}</tr></td>' +
                '<tr><td>Fungsi Kawasan</td><td>:</td><td>{fungsi_kws}</tr></td>' +
                '<tr><td>SK Adat</td><td>:</td><td>{sk_adat}</tr></td>' +
                '<tr><td>Tanggal Adat</td><td>:</td><td>{tgl_adat}</tr></td>' +
                '<tr><td>Luas Hutan Adat</td><td>:</td><td>{ls_adat}</tr></td>' +
                '<tr><td>Keternagan</td><td>:</td><td>{keterangan}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var sebaran_konflik = L.layerGroup();
        var jabalnus = L.esri.featureLayer({
        url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Jabalnus2/FeatureServer/0"
        }).addTo(sebaran_konflik);
        jabalnus.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
                '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
                '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
                '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var kalimantan = L.esri.featureLayer({
        url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Kalimantan2/FeatureServer/0"
        }).addTo(sebaran_konflik);
        kalimantan.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
                '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
                '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
                '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var maluku_papua = L.esri.featureLayer({
        url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Maluku_Papua2/FeatureServer/0"
        }).addTo(sebaran_konflik);
        maluku_papua.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
                '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
                '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
                '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var sulawesi = L.esri.featureLayer({
        url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sulawesi2/FeatureServer/0"
        }).addTo(sebaran_konflik);
        sulawesi.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
                '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
                '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
                '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var sumatra = L.esri.featureLayer({
        url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sumatera2/FeatureServer/0"
        }).addTo(sebaran_konflik);
        sumatra.bindPopup(function (layer) {
        return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
                '<table style="font-size:100%" width="250">' +
                '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
                '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
                '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
                '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
                '</table>', layer.feature.properties);
        });
        var overlayLayers = {
        "Data Hutan": {
        "Lokasi Pengaduan": markers,
                "Hutan Adat": hutan_adat,
                "Potensi Sebaran Konflik": sebaran_konflik,
                "Fungsi Kawasan Hutan": kawasan_hutan
        }
        };
        L.control.groupedLayers(null, overlayLayers).addTo(map);
});
        var GeoSearchControl = window.GeoSearch.GeoSearchControl;
        var GoogleProvider = window.GeoSearch.GoogleProvider;
        var provider = new GoogleProvider({
        params: {
        key: 'AIzaSyDW1hC9P9SLkyaynoCE-mvJhfNRKYrZcK4',
        },
        });
        var searchControl = new GeoSearchControl({
        provider: provider,
                style: 'bar',
                position: 'topright',
                showMarker: true,
                showPopup: true,
                marker: {
                icon: new L.Icon.Default(),
                        draggable: true,
                },
                popupFormat: ({ query, result }) => result.label,
                maxMarkers: 1,
                retainZoomLevel: false,
                animateZoom: true,
                autoComplete: true,
                autoCompleteDelay: 150,
                autoClose: false,
                searchLabel: 'Cari Alamat',
                keepResult: false
        }).addTo(map);
        var lc = L.control.locate({
        position: 'topleft',
                icon: 'fa fa-map-marker fa-2x',
                iconLoading: 'fa fa-spinner fa-spin fa-2x',
                locateOptions: {
                enableHighAccuracy: true
                },
                strings: {
                title: "Cari Lokasi Anda Saat Ini",
                        popup: "Anda Berada Disekitar Sini",
                        outsideMapBoundsMsg: "Anda Berada di Luar Wilayah Negara Republik Indonesia"
                }}).addTo(map);
        L.control.betterscale({
        imperial: false,
                metric: true
        }).addTo(map);