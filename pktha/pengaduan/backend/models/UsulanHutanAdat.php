<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usulan_hutan_adat".
 *
 * @property integer $id_form
 * @property string $nama
 * @property string $email
 * @property string $alamat
 * @property string $telpon
 * @property string $form
 */
class UsulanHutanAdat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usulan_hutan_adat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_form', 'nama', 'email', 'alamat', 'telpon', 'form'], 'required'],
            [['id_form'], 'integer'],
            [['nama', 'email', 'telpon', 'form'], 'string', 'max' => 20],
            [['alamat'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_form' => 'Id Form',
            'nama' => 'Nama',
            'email' => 'Email',
            'alamat' => 'Alamat',
            'telpon' => 'Telpon',
            'form' => 'Form',
        ];
    }
}
