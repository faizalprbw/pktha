<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "draft_mou".
 *
 * @property integer $id
 * @property string $berita_acara
 * @property string $foto_drafting_mou
 */
class DraftMou extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'draft_mou';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'berita_acara', 'foto_drafting_mou'], 'required'],
            [['id'], 'integer'],
            [['berita_acara', 'foto_drafting_mou'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'berita_acara' => 'Berita Acara',
            'foto_drafting_mou' => 'Foto Drafting Mou',
        ];
    }
}
