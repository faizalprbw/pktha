<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $role;



    const ROLE_USER = 10;
    const ROLE_MODERATOR = 20;
    // Admin wilayah
    const ROLE_ADMIN_WILAYAH_1 = 21;
    const ROLE_ADMIN_WILAYAH_2 = 22;
    const ROLE_ADMIN_WILAYAH_3 = 23;
    const ROLE_ADMIN_WILAYAH_4 = 24;
    const ROLE_ADMIN_WILAYAH_5 = 25;
    // Superadmin
    const ROLE_ADMIN = 30;
    const ROLE_ADMIN_1 = 31;
    const ROLE_ADMIN_2 = 32;
    const ROLE_ADMIN_3 = 33;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],


            ['role', 'required'],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [
                self::ROLE_USER,
                self::ROLE_MODERATOR,
                self::ROLE_ADMIN_WILAYAH_1,
                self::ROLE_ADMIN_WILAYAH_2,
                self::ROLE_ADMIN_WILAYAH_3,
                self::ROLE_ADMIN_WILAYAH_4,
                self::ROLE_ADMIN_WILAYAH_5,
                self::ROLE_ADMIN,
                self::ROLE_ADMIN_1,
                self::ROLE_ADMIN_2,
                self::ROLE_ADMIN_3,
                ]
            ],

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->role = $this->role;
            $user->status = 10;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            $auth = Yii::$app->authManager;
            $admin = $auth->getRole('administrator');
            $user->save(false);
            $auth->assign($admin, $user->getId());

            return $user;
        }

        return null;
    }
    public function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            $this->username = $model->username;
            $this->email = $model->email;
            $this->role = $model->role;
            return $this;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
