<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ChangePasswordFormUser extends Model
{
    public $id;        
    public $password1;
    public $password2;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [


            ['password1', 'required'],
            ['password1', 'string', 'min' => 6],

            ['password2', 'required'],
            ['password2', 'string', 'min' => 6],
            ['password2','compare','compareAttribute'=>'password1'],


        ];
    }
}
