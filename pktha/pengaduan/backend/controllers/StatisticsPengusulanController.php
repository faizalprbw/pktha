<?php

namespace backend\controllers;

class StatisticsPengusulanController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionMonevPemberdayaan() {
        return $this->render('monev-pemberdayaan', ['model' => null]);
    }

    public function actionPencantumanPenetapan() {
        return $this->render('pencantuman-penetapan', ['model' => null]);
    }

    public function actionValidasi() {
        return $this->render('validasi', ['model' => null]);
    }

    public function actionVerifikasi() {
        return $this->render('verifikasi', ['model' => null]);
    }

}
