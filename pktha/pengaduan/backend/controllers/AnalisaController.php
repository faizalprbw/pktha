<?php

namespace backend\controllers;

use Yii;
use common\models\Analisa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AnalisaController implements the CRUD actions for Analisa model.
 */
class AnalisaController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Analisa models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Analisa::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Analisa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $idx) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'idx' => $idx
        ]);
    }

    /**
     * Creates a new Analisa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Analisa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateid($id) {
        $model = new Analisa();



        if ($model->load(Yii::$app->request->post())) {
            // print_r($model->getErrors());
            $model->pengaduan_id = $id;
            if ($model->save()) {
                return $this->redirect(['pengaduan/timeline', 'id' => $id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'idx' => $id
                ]);
            }
        } else {

            return $this->render('create', [
                        'model' => $model,
                        'idx' => $id
            ]);
        }
    }

    /**
     * Updates an existing Analisa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $idx) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'idx' => $idx]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'idx' => $idx
            ]);
        }
    }

    /**
     * Deletes an existing Analisa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Analisa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Analisa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Analisa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
