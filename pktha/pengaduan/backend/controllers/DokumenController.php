<?php

namespace backend\controllers;

use Yii;
use common\models\Dokumen;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DokumenController implements the CRUD actions for dokumen model.
 */
class DokumenController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all dokumen models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => dokumen::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single dokumen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new dokumen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null, $idt = null, $idx = null) {
        $model = new Dokumen();

        if ($model->load(Yii::$app->request->post())) {

            $model->tipe_dokumen = $id;
            $model->id_tahap = $idt;
            // var_dump(Yii::$app->request->post());

            $model->url = UploadedFile::getInstance($model, 'url');

            // var_dump($model->upload());

            if ($model->save() && $model->upload()) {
                return $this->redirect(['pengaduan/timeline', 'id' => $idx]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateassesment($id = null, $idt = null, $idx = null, $idassesment = null) {
        $model = new Dokumen();

        if ($model->load(Yii::$app->request->post())) {

            $model->tipe_dokumen = $id;
            $model->id_tahap = $idt;
            $model->id_assesment = $idassesment;
            // var_dump(Yii::$app->request->post());

            $model->url = UploadedFile::getInstance($model, 'url');

            // var_dump($model->upload());

            if ($model->save() && $model->upload()) {
                return $this->redirect(['pengaduan/timeline', 'id' => $idx]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing dokumen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing dokumen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $idx) {
        $this->findModel($id)->delete();
        return $this->redirect(['pengaduan/timeline', 'id' => $idx]);
        // return $this->redirect(['index']);
    }

    /**
     * Finds the dokumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return dokumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = dokumen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
