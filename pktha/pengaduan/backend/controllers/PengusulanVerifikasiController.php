<?php

namespace backend\controllers;

use Yii;
use common\models\PengusulanRegistrasi;
use common\models\PengusulanVerifikasi;
use common\models\PengusulanVerifikasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengusulanVerifikasiController implements the CRUD actions for PengusulanVerifikasi model.
 */
class PengusulanVerifikasiController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengusulanVerifikasi models.
     *
     * @return mixed
     */
    public function actionIndex() 
    {
        $searchModel = new PengusulanVerifikasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengusulanVerifikasi model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id) 
    {
        return $this->render(
            'view', [
                    'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Creates a new PengusulanVerifikasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id_pengusulan
     * @return mixed
     */
    public function actionCreate($id_pengusulan) 
    {
        if (PengusulanVerifikasi::findOne(['id_pengusulan' => $id_pengusulan])) {
            throw new \yii\web\ServerErrorHttpException('Tahap III untuk Pengusulan ini telah dibuat');
        }

        $model = new PengusulanVerifikasi();
        $model->id_pengusulan = $id_pengusulan;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_date = \common\utils\DateHelper::GetCurrentDate();
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();

            if ($model->save() && $this->uploadModelFiles($model) && $model->save()) {
                return $this->redirect(['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]);
            }
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing PengusulanVerifikasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan]);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = \common\utils\DateHelper::GetCurrentDate();

            if ($this->uploadModelFiles($model) && $model->save()) {
                return $this->redirect(['pengusulan-registrasi/view', 'id' => $model->id_pengusulan]);
            } else {
                return new \yii\web\ServerErrorHttpException('Update Error');
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengusulan model and redirects to matching Pengusulan view.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengusulan-registrasi/view', 'id' => PengusulanRegistrasi::find()->getById($this->findModel($id)->id_pengusulan)->id]);
    }

    /**
     * Uploads files in the ActiveForm of PengusulanVerifikasi instances.
     *
     * @param  PengusulanRegistrasi $model
     * @return boolean
     * @throws ServerErrorHttpException if the upload process was not completed.
     */
    protected function uploadModelFiles($_model)
    {
        $beritaAcara = \yii\web\UploadedFile::getInstance($_model, 'berita_acara_data');
        if ($beritaAcara) {
            $mainModel = \common\models\PengusulanRegistrasi::findOne(['id' => $_model->id_pengusulan]);
            $uploadPath = $mainModel->getPath() . '/berita_acara';

            if (!is_dir($uploadPath)) {
                mkdir($uploadPath);
                chmod($uploadPath, 0775);
            }
            
            $filename = $beritaAcara->baseName . '.' . $beritaAcara->extension;
            if ($beritaAcara->saveAs($uploadPath . '/' . $filename)) {
                $_model->berita_acara_filename = $filename;
            } else {
                throw new \yii\web\ServerErrorHttpException('Upload Error');
            }
        }
        return true;
    }

    /**
     * Deletes an existing PengusulanVerifikasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id) 
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Return a file whose name is in berita_acara_filename field.
     *
     * @param type $id model ID
     */
    public function actionDownload($id) 
    {
        $model = $this->findModel($id);
        $regModel = PengusulanRegistrasi::findOne(['id' => $model->id_pengusulan]);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if (isset($regModel)) {
            $filePath = $regModel->getPath() . '/berita_acara/' . $model->berita_acara_filename;
            if (file_exists($filePath)) {
                return Yii::$app->response->sendFile($filePath);
            } else {
                throw new NotFoundHttpException('File Not Found');
            }
        }
    }

    /**
     * Finds the PengusulanVerifikasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengusulanVerifikasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) 
    {
        if (($model = PengusulanVerifikasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
