<?php

namespace backend\controllers;

use Yii;
use common\models\Artikel;
use common\models\ArtikelQuery;
use common\models\ArtikelSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArtikelController implements the CRUD actions for Artikel model.
 */
class ArtikelController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Artikel models.
     * @return mixed
     */
    public function actionRecyclebin() {
        $modelsearch = new Artikel;
        $p = Yii::$app->request->queryParams;

        // var_dump($p);



        $dataProvider = new ActiveDataProvider([
            // 'query' => $modelsearch->find()->where(['!=','kategori_id','13']),
            // $models = Book::find()->where('id != :id and type != :type', ['id'=>1, 'type'=>1])->all();
            'query' => $modelsearch->find()->where('kategori_id != :id and deleted = :did', ['id' => 13, 'did' => 1])
        ]);

        // $dataProvider = $modelsearch->search($p);


        return $this->render('recyclebin', [
                    'dataProvider' => $dataProvider,
                    'modelsearch' => $modelsearch
        ]);
    }

    /*
      public function actionIndex() {
      $modelsearch = new Artikel;
      $p = Yii::$app->request->queryParams;

      $dataProvider = new ActiveDataProvider([
      // 'query' => $modelsearch->find()->where(['!=','kategori_id','13']),
      // $models = Book::find()->where('id != :id and type != :type', ['id'=>1, 'type'=>1])->all();
      'query' => $modelsearch->find()->where('kategori_id != :id and deleted = :did', ['id' => 13, 'did' => 0])
      ]);

      // $dataProvider = $modelsearch->search($p);


      return $this->render('index', [
      'dataProvider' => $dataProvider,
      'modelsearch' => $modelsearch
      ]);
      }
     * 
     */

    public function actionIndex() {
        $searchModel = new ArtikelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artikel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Artikel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Artikel();

        // $x = \mdm\admin\models\AuthItem::find(1)->where(['user_id = 3'])->one();
        // var_dump($x);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            
            // Set published status to false or zero if current user is not Admin Pusat
            $role = \Yii::$app->user->identity->role;
            if ($role < 30) {
                // This is not Admin Pusat
                $model->publish = 0;
            }
            
            if ($model->imageFile) {
                $fname = md5(microtime()) . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(dirname(dirname(__DIR__)) . '/uploads/' . $fname);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Artikel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionPublish($id) {
        $model = $this->findModel($id);
        $model->publish = ($model->publish == 1) ? 0 : 1;
        $model->save();
        return $this->redirect(['index']);
        // var_dump($model->publish);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // var_dump(Yii::$app->authManager->getRoles);
        // $m = new mdm\admin\components\Helper;


        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                // $fname= $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $fname = md5(microtime()) . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(dirname(dirname(__DIR__)) . '/uploads/' . $fname);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTentang() {

        $model = $this->findModel(7);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $fname = md5(microtime()) . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(Yii::getAlias('@updir') . '/uploads/' . $fname);

                // $fname= $model->imageFile->baseName . '.' . $model->imageFile->extension;
                // $model->image=$fname;
                // $model->save();
                // $model->imageFile->saveAs(Yii::getAlias('@updir').'/uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);                
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['tentang']);
            } else {
                $model->save();
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['tentang']);
            }
        } else {
            return $this->render('tentang', [
                        'model' => $model,
            ]);
        }
    }

    public function actionPeraturandanperundangan() {

        $model = $this->findModel(20);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $fname = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(Yii::getAlias('@updir') . '/uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['peraturandanperundangan']);
            } else {
                // $model->id=20;
                $model->save();
                // var_dump($model->save();
                // var_dump(Yii::$app->request->post());
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['peraturandanperundangan']);
            }
        } else {
            return $this->render('tentang', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTatacara() {

        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $fname = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(Yii::getAlias('@updir') . '/uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['tatacara']);
            } else {
                $model->save();
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['tatacara']);
            }
        } else {
            return $this->render('tentang', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTatacaraPengaduanKonflik() {
        $model = $this->findModel(24);
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $fname = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(Yii::getAlias('@updir') . '/uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                return $this->redirect(['tatacara']);
            } else {
                $model->save();
                return $this->redirect(['tatacara']);
            }
        } else {
            return $this->render('tentang', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTatacaraPengusulanHutanAdat() {
        $model = $this->findModel(23);
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $fname = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->image = $fname;
                $model->save();
                $model->imageFile->saveAs(Yii::getAlias('@updir') . '/uploads/' . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                return $this->redirect(['tatacara']);
            } else {
                $model->save();
                return $this->redirect(['tatacara']);
            }
        } else {
            return $this->render('tentang', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Artikel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();


        return $this->redirect(['index']);
    }

    public function actionUndelete($id) {
        // $this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->deleted = 0;
        $model->save();

        return $this->redirect(['index']);
    }
    
    public function actionRemove($id){
        $model = $this->findModel($id);
        if (isset($model)) {
            $model->delete();
        }
        
        return $this->redirect(['index']);
    }

    // Tentang
    public function actionDirektoratPktha() {
        $model = $this->findModel(26);
        
        return $this->render('tentang', [
                    'model' => $model,
        ]);
    }

    public function actionStrukturOrganisasi() {
        $model = $this->findModel(27);
        
        return $this->render('tentang', [
                    'model' => $model,
        ]);
    }

    public function actionTugasPokokDanFungsi() {
        $model = $this->findModel(28);
        
        return $this->render('tentang', [
                    'model' => $model,
        ]);
    }

    public function actionPeraturanPerundanganTerkait() {
        return $this->render('tentang', [
                    'model' => null,
        ]);
    }

    public function actionLainLain() {
        return $this->render('tentang', [
                    'model' => null,
        ]);
    }

    // Kontak
    public function actionKontakDirektoratPktha() {
        $model = $this->findModel(25);

        return $this->render('tentang', [
                    'model' => $model,
        ]);
    }

    public function actionKontakDirjenPskl() {
        $model = $this->findModel(29);
        
        return $this->render('tentang', [
                    'model' => $model,
        ]);
    }

    /**
     * Finds the Artikel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Artikel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Artikel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
