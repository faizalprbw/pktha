<?php

namespace backend\controllers;

class StatisticsPengaduanController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAssesmen() {
        return $this->render('assesmen', ['model' => null]);
    }

    public function actionDraftingMou() {
        return $this->render('drafting-mou', ['model' => null]);
    }
    
    public function actionMediasi() {
        return $this->render('mediasi', ['model' => null]);
    }
    
    public function actionPengaduanDialihkah() {
        return $this->render('pengaduan-dialihkan', ['model' => null]);
    }
    
    public function actionPraMediasi() {
        return $this->render('pra-mediasi', ['model' => null]);
    }
    
    public function actionTandaTanganMou() {
        return $this->render('tanda-tangan-mou', ['model' => null]);
    }

}
