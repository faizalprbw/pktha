<?php
namespace backend\controllers;

use Yii;
use common\models\Replay;
use common\models\Pengaduan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\base\ErrorException;


class ReplayController extends Controller{
    public function actionIndex($id=null)
    {
        // Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        //var_dump(\Yii::$app->user);
        $model = new Replay();
        $model->pengaduan_id = $id;

        $pe = Pengaduan::findOne($id);

        $model->to = $pe['email_identitas'];
        $model->subject = $pe['tutuntan_pengaduan'];
        $xcontent = "<p>YTH : <br/><b>".$pe['nama_identitas']."</b></p>

                <p>&nbsp;</p>

                <p>&nbsp;</p>

                <p>Dear,</p>

                <p>&nbsp;</p>

                <p>&nbsp;</p>

                <p>&nbsp;</p>

                <p>Terima Kasih</p>

                <p>&nbsp;</p>

                <p>Tim Sistem Database Konflik Tenurial</p>
                ";

        
                $model->content =$xcontent;

        // $model = $this->findAll($id);
        // var_dump($model->findOne(1));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $mdl = Yii::$app->request->post();
            // var_dump($mdl['Replay']['to']);

            $s1 =$mdl['Replay']['to'];
            $s2 =$mdl['Replay']['cc'];
            $s3 =$mdl['Replay']['subject'];
            $s4 =$mdl['Replay']['content'];
            
            // var_dump(Yii::$app->request->post());
            // exit();

            // Yii::$app->mailer->compose()
            //     ->setFrom(["pengaduan.pktha@gmail.com"])
            //     ->setTo( ["vei_btx@yahoo.com"])
            //     ->setSubject("ngirim kamfrett backend " . date ("H:i:s"))
            //     ->setTextBody("Useless body")
            //     ->send();

            //  Yii::$app->mailer->compose()
            // // ->setFrom('addrs1@gmail.com')
            //  ->setFrom(["pengaduan.pktha@gmail.com"])
            // ->setTo(array($s1))
            // ->setCc(array($s2))
            // ->setSubject($s3)            
            // ->setHtmlBody($s4)
            // ->send();    

            $emailme=Yii::$app->mailer->compose();    

            $emailme->setFrom(["pengaduan.pktha@gmail.com"]);
            // $emailme->setTo( ["vei_btx@yahoo.com"]);
            // $emailme->setSubject("ngirim kamfrett backend " . date ("H:i:s"));
            // $emailme->setTextBody("Useless body");
            // $emailme->send();

            $emailme->setTo(array($s1));
            if(strlen($s2)>2){
                $emailme->setCc(array($s2));
            }
            $emailme->setSubject($s3);     
            $emailme->setHtmlBody($s4);
            $emailme->send();






            return $this->redirect(['pengaduan/index']);
        } else {
            return $this->render('replay', [
                'model' => $model,
            ]);
        }


        // if ($model->load(Yii::$app->request->post())) {
        //     $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        //     if($model->imageFile){
        //         $fname= md5(microtime()) . '.' . $model->imageFile->extension;
        //         $model->image=$fname;
        //         $model->save();
        //         $model->imageFile->saveAs(Yii::getAlias('@updir').'/uploads/' . $fname);
        //         return $this->redirect(['view', 'id' => $model->id]);
        //     }else{
        //         $model->save();
        //         return $this->redirect(['view', 'id' => $model->id]);            
        //     }                        
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }


        // return $this->render('replay', [
        //     'model' => $model,
        // ]);

    }



    protected function findModel($id)
    {
        if (($model = Replay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}