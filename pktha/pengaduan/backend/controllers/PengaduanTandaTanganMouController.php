<?php

namespace backend\controllers;

use Yii;
use common\models\PengaduanTandaTanganMou;
use common\models\PengaduanTandaTanganMouSearch;
use common\models\PengaduanRegistrasi;
use common\utils\DateHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * PengaduanTandaTanganMouController implements the CRUD actions for PengaduanTandaTanganMou model.
 */
class PengaduanTandaTanganMouController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengaduanTandaTanganMou models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengaduanTandaTanganMouSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single PengaduanTandaTanganMou model.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        return $this->render(
            'view', [
                    'model' => $model,
            ]
        );
    }

    /**
     * Creates a new PengaduanTandaTanganMou model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PengaduanTandaTanganMou();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render(
                'create', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing PengaduanTandaTanganMou model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->modified_date = DateHelper::GetCurrentDate();
            $path = $regModel->getPath();
            $uploaded_file = UploadedFile::getInstance($model, 'dokumen_mou_filename');
            $subpath = $path . '/dokumen_mou';
            if (!is_dir($subpath)) {
                mkdir($subpath, 0775, true);
            }
            if ($uploaded_file) {
                $name = $uploaded_file->baseName . '.' . $uploaded_file->extension;
                $upload = $uploaded_file->saveAs($subpath . '/' . $name);
                if ($upload) {
                    $model->dokumen_mou_filename = $name;
                } else {
                    throw new ServerErrorHttpException($name . ' could not be uploaded.');
                }
            }
            if ($model->save()) {
                return $this->redirect(['//pengaduan-registrasi/view', 'id' => $model->id_pengaduan]);
            }
        } else {
            return $this->render(
                'update', [
                        'model' => $model,
                ]
            );
        }
    }

    /**
     * Cancels changes to Pengaduan model.
     *
     * @param  integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        return $this->redirect(['pengaduan-registrasi/view', 'id' => PengaduanRegistrasi::find()->getById($this->findModel($id)->id_pengaduan)->id]);
    }

    /**
     * Deletes an existing PengaduanTandaTanganMou model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Downloads the specified file type from a model.
     *
     * @param  string $id
     * @throws ServerErrorHttpException
     */
    public function actionDownload($id)
    {
        $model = $this->findModel($id);
        $regModel = PengaduanRegistrasi::find()->getById($model->id_pengaduan);
        if (!Yii::$app->user->identity->isAdminWilayah($regModel->id_wilayah_konflik)) {
            throw new ServerErrorHttpException('You are not authorized to perform this action.');
        }

        $path = $regModel->getPath();
        $filename = $model->dokumen_mou_filename;
        $full_path = $path . '/dokumen_mou/' . $filename;
        if (is_file($full_path)) {
            Yii:: $app->response->sendFile($full_path);
        } else {
            throw new ServerErrorHttpException($full_path . ' could not be found.');
        }
    }

    /**
     * Finds the PengaduanTandaTanganMou model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  string $id
     * @return PengaduanTandaTanganMou the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PengaduanTandaTanganMou::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
