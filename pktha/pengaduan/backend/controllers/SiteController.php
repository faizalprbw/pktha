<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\Tahapan;
use yii\filters\VerbFilter;
use yii\caching\DbDependency;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                            'datamap',
                            'getkabkot',
                            'getkecamatan',
                            'getdraw',
                            'petapotensi',
                            'pencantumanhutanadat',
                            'potensihutanadat',
                            'produkhukumprovinsi',
                            'produkhukumkabupaten',
                            'lokasipengaduan',
                            'kasus2016',
                            'kasus2017'
                        ],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDatamap() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $id = 1;

            $rsp = $this->tes(1);
            return json_encode($rsp);
        }
    }

    private function datates($id) {
        $model = \common\models\Pengaduan::find()->where(['id_tahapan' => $id, 'deleted' => 0])->orderBy('id')->all();

        $rsp = "";
        $i = 0;

        $x = array();
        foreach ($model as $res) {

            $x[$i]['id_tahapan'] = $res->id_tahapan;
            $x[$i]['tutuntan_pengaduan'] = $res->tutuntan_pengaduan;
            $x[$i]['lot'] = $res->lot_perkiraaan_lokasi;
            $x[$i]['lan'] = $res->lang_perkiraan_lokasi;
            $x[$i]['tipe'] = $res->id_tahapan;
            $i++;
        }
        $rsp['success'] = 1;
        $rsp['data'] = $x;
        $rsp['total'] = count($x);
        return $rsp;
    }

    public function actionIndex() {
        // Yii::$app->cache->flush();
        $role = \Yii::$app->user->identity->role;

        // Some of these queries are huge, gotta cache em all
        $sisaPengaduan = Tahapan::countAllTahapanFirstUnfinished($role, 'pengaduan');
        $sisaPengusulan = Tahapan::countAllTahapanFirstUnfinished($role, 'pengusulan');
        $countDialihkan = Tahapan::countDialihkan($role);

        return $this->render(
                        'index', [
                    'tahapPengusulan' => $sisaPengusulan,
                    'tahapPengaduan' => $sisaPengaduan,
                    'countDialihkan' => $countDialihkan,
                    'jumlah_potensi' => \common\utils\DataPeta::getJumlahPotensiHutanAdat(),
                    'jumlah_pencantuman' => \common\utils\DataPeta::getJumlahPencantumanHutanAdat(),
                        ]
        );
    }

    public function actionGetkabkot() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $provinsi = Yii::$app->request->post('id');

            $potensikonflik = new \common\models\Potensikonflik;
            $rsp = $potensikonflik->getKabkota($provinsi);
            $rspx = "<option>--Pilih Kota/Kab--</option>";
            foreach ($rsp as $kxy) {
                $uw = $kxy['kabkot'];
                $rspx .= "<option>" . strtoupper($uw) . "</option>";
            }
            $this->actionDatamap();
            return $rspx;
        }
    }

    public function actionGetkecamatan() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $provinsi = Yii::$app->request->post('id');

            $potensikonflik = new \common\models\Potensikonflik;
            $rsp = $potensikonflik->getKecamatan($provinsi);
            $rspx = "<option>--Pilih Kecamatan--</option>";
            foreach ($rsp as $kxy) {
                $rspx .= "<option>" . $kxy['kecamatan'] . "</option>";
            }
            return $rspx;
        }
    }

    public function actionGetdraw() {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $provinsi = Yii::$app->request->post('id');

            $potensikonflik = new \common\models\Potensikonflik;
            $rs = $potensikonflik->getDraw($provinsi);

            return $rs['geojson'];
        }
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render(
                            'login', [
                        'model' => $model,
                            ]
            );
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPencantumanhutanadat($id) {
        $pencantuman = \common\utils\DataPeta::getSpasialPencantumanHutanAdat($id);

        return ($pencantuman);
    }

    public function actionPotensihutanadat($id) {
        $cache = Yii::$app->cache;
        $dep = new DbDependency(['sql' => 'select count(id) from potensi_hutan_adat']);
        $key = array('backend', 'controller', 'site', 'potensihutanadat');

        $potensi = $cache->get($key);
        if ($potensi === false) {
            $potensi = \common\utils\DataPeta::getSpasialPotensiHutanAdat($id);
            $cache->set($key, $potensi, 3000, $dep);
        }
        return ($potensi);
    }

    public function actionProdukhukumprovinsi($id) {
        $produkprovinsi = \common\utils\DataPeta::getSpasialProdukHukumProvinsi($id);

        return ($produkprovinsi);
    }

    public function actionProdukhukumkabupaten($id) {
        $produkkabupaten = \common\utils\DataPeta::getSpasialProdukHukumKabupaten($id);

        return ($produkkabupaten);
    }

    public function actionLokasipengaduan($role = null) {
        $lokasipengaduan = \common\utils\DataPeta::getSpasialLokasiPengaduan($role);

        return ($lokasipengaduan);
    }

    public function actionKasus2016() {
        $kasus2016 = \common\utils\DataPeta::getSpasialKasus2016();

        return ($kasus2016);
    }

    public function actionKasus2017() {
        $kasus2017 = \common\utils\DataPeta::getSpasialKasus2017();

        return ($kasus2017);
    }

}
