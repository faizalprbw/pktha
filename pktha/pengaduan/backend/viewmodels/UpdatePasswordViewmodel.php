<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\viewmodels;

use yii\base\Model;

/**
 * Description of UpdatePasswordViewmodel
 *
 * @author wizard
 */
class UpdatePasswordViewmodel extends Model {
    //put your code here
    
    public $id;
    public $kode;
    public $kunci;
    
    public function rules()
    {
        return [
            [['id', 'kode', 'kunci'], 'safe'],
        ];
    }
    
    public function UpdatePasswordViewmodel($_id, $_kode, $_kunci) {
        $this->id = $_id;
        $this->kode = $_kode;
        $this->kunci = $_kunci;
    }
}
