<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%statistik_pengaduan}}".
 *
 * @property string $id
 * @property integer $assesmen
 * @property integer $pra_mediasi
 * @property integer $mediasi
 * @property integer $drafting_mou
 * @property integer $tanda_tangan_mou
 * @property string $created_date
 * @property integer $dialihkan
 * @property integer $tidak_valid
 */
class StatistikPengaduan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statistik_pengaduan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'assesmen', 'pra_mediasi', 'mediasi', 'drafting_mou', 'tanda_tangan_mou', 'dialihkan', 'tidak_valid'], 'integer'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'assesmen' => Yii::t('app', 'Assesmen'),
            'pra_mediasi' => Yii::t('app', 'Pra Mediasi'),
            'mediasi' => Yii::t('app', 'Mediasi
'),
            'drafting_mou' => Yii::t('app', 'Drafting MoU'),
            'tanda_tangan_mou' => Yii::t('app', 'Tanda Tangan MoU'),
            'created_date' => Yii::t('app', 'Created Date'),
            'dialihkan' => Yii::t('app', 'Dialihkan'),
            'tidak_valid' => Yii::t('app', 'Invalid'),
        ];
    }

    /**
     * @inheritdoc
     * @return StatistikPengaduanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatistikPengaduanQuery(get_called_class());
    }
}
