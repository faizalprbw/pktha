<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pengusulan_verifikasi}}".
 *
 * @property string $id
 * @property string $id_pengusulan
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $nama_pemohon
 * @property string $no_ktp_pemohon
 * @property string $alamat_pemohon
 * @property string $jabatan_pemohon
 * @property string $nama_mha
 * @property string $desa_kelurahan
 * @property string $kecamatan
 * @property string $kota_kabupaten
 * @property string $provinsi
 * @property string $das
 * @property double $luas
 * @property boolean $hak_atas_tanah
 * @property boolean $validitas_hak_atas_tanah
 * @property boolean $tanah_berupa_hutan
 * @property boolean $surat_pernyataan_penetapan_hutan_hak
 * @property boolean $validitas_surat_pernyataan_penetapan_hutan_hak
 * @property boolean $validitas_tanah_berupa_hutan
 * @property boolean $masyarakat_adat_diakui_pemda
 * @property boolean $validitas_masyarakat_adat_diakui_pemda
 * @property boolean $wilayah_adat_berupa_hutan
 * @property boolean $validitas_wilayah_adat_berupa_hutan
 * @property boolean $surat_pernyataan_ketua_adat
 * @property boolean $validitas_surat_pernyataan_ketua_adat
 * @property string $catatan
 * @property boolean $validasi_simpulan_hasil_verifikasi
 * @property string $berita_acara_filename
 */
class PengusulanVerifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pengusulan_verifikasi}}';
    }

    public $berita_acara_data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengusulan'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished', 'hak_atas_tanah', 'validitas_hak_atas_tanah', 'tanah_berupa_hutan', 'surat_pernyataan_penetapan_hutan_hak', 'validitas_surat_pernyataan_penetapan_hutan_hak', 'validitas_tanah_berupa_hutan', 'masyarakat_adat_diakui_pemda', 'validitas_masyarakat_adat_diakui_pemda', 'wilayah_adat_berupa_hutan', 'validitas_wilayah_adat_berupa_hutan', 'surat_pernyataan_ketua_adat', 'validitas_surat_pernyataan_ketua_adat', 'validasi_simpulan_hasil_verifikasi'], 'boolean'],
            [['nama_pemohon', 'alamat_pemohon', 'jabatan_pemohon', 'nama_mha', 'desa_kelurahan', 'kecamatan', 'kota_kabupaten', 'provinsi', 'das', 'catatan', ], 'string'],
            [['berita_acara_data'], 'file', 'extensions' => 'pdf, doc, docx, odt'],
            [['berita_acara_filename'], 'string'],
            [['no_ktp_pemohon', 'luas'], 'number'],
            [['id_pengusulan'], 'exist', 'skipOnError' => true, 'targetClass' => PengusulanRegistrasi::className(), 'targetAttribute' => ['id_pengusulan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_pengusulan' => Yii::t('app', 'Id Pengusulan'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Selesai'),
            'nama_pemohon' => Yii::t('app', 'Nama Pemohon'),
            'no_ktp_pemohon' => Yii::t('app', 'no_ktp_pemohon'),
            'alamat_pemohon' => Yii::t('app', 'Alamat Pemohon'),
            'jabatan_pemohon' => Yii::t('app', 'Jabatan Pemohon'),
            'nama_mha' => Yii::t('app', 'Nama MHA'),
            'desa_kelurahan' => Yii::t('app', 'Desa / Kelurahan'),
            'kecamatan' => Yii::t('app', 'Kecamatan'),
            'kota_kabupaten' => Yii::t('app', 'Kota / Kabupaten'),
            'provinsi' => Yii::t('app', 'Provinsi'),
            'das' => Yii::t('app', 'Daerah Aliran Sungai'),
            'luas' => Yii::t('app', 'Luas'),
            'hak_atas_tanah' => Yii::t('app', 'Hak Atas Tanah yang dimiliki oleh suatu Pihak Tertentu'),
            'validitas_hak_atas_tanah' => Yii::t('app', 'Validitas Hak Atas Tanah yang dimiliki Pihak Tertentu'),
            'tanah_berupa_hutan' => Yii::t('app', 'Tanah yang sebagian / seluruhnya berupa hutan'),
            'surat_pernyataan_penetapan_hutan_hak' => Yii::t('app', 'Surat Pernyataan dari Pihak Tertentu untuk menetapkan Tanah sebagai Hutan HAK'),
            'validitas_surat_pernyataan_penetapan_hutan_hak' => Yii::t('app', 'Validitas Surat Pernyataan dari Pihak Tertentu untuk menetapkan Tanah sebagai Hutan HAK'),
            'validitas_tanah_berupa_hutan' => Yii::t('app', 'Validitas Tanah yang sebagian / seluruhnya berupa hutan'),
            'masyarakat_adat_diakui_pemda' => Yii::t('app', 'Terdapat masyarakat hukum adat atau hak ulayat yang telah diakui oleh Pemerintah Daerah melalui produk hukum daerah'),
            'validitas_masyarakat_adat_diakui_pemda' => Yii::t('app', 'Validitas Terdapat masyarakat hukum adat atau hak ulayat yang telah diakui oleh Pemerintah Daerah melalui produk hukum daerah'),
            'wilayah_adat_berupa_hutan' => Yii::t('app', 'Terdapat wilayah adat yang sebagian atau seluruhnya berupa hutan'),
            'validitas_wilayah_adat_berupa_hutan' => Yii::t('app', 'Validitas Terdapat wilayah adat yang sebagian atau seluruhnya berupa hutan'),
            'surat_pernyataan_ketua_adat' => Yii::t('app', 'Surat pernyataan dari ketua adat untuk menetapkan tanahnya sebagai hutan hak '),
            'validitas_surat_pernyataan_ketua_adat' => Yii::t('app', 'Validitas Surat pernyataan dari ketua adat untuk menetapkan tanahnya sebagai hutan hak '),
            'catatan' => Yii::t('app', 'Catatan'),
            'validasi_simpulan_hasil_verifikasi' => Yii::t('app', 'Kesimpulan hasil verifikasi'),
            'berita_acara_filename' => Yii::t('app', 'Berita Acara'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengusulanVerifikasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengusulanVerifikasiQuery(get_called_class());
    }
}
