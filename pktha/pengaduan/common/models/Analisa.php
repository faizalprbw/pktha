<?php

namespace common\models;



use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "analisa".
 *
 * @property integer $id
 * @property string $hasil_analisa
 * @property string $pembahasan
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Analisa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'analisa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hasil_analisa', 'pembahasan'], 'required'],
            [['hasil_analisa', 'pembahasan'], 'string'],
            [['created_at', 'updated_at', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hasil_analisa' => 'Hasil Analisa',
            'pembahasan' => 'Pembahasan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){ 
                //Pengaduan
                    $pengaduan = Pengaduan::findOne($this->pengaduan_id);
                    $pengaduan->id_tahapan = 3;
                    $pengaduan->save();
                //pengaduan


                // $this->kode = $this->getKode($this->sarana_pengaduan);
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }
}
