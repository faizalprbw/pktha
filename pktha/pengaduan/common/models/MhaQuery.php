<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Mha]].
 *
 * @see Mha
 */
class MhaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Mha[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Mha|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
