<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanValidasi]].
 *
 * @see PengusulanValidasi
 */
class PengusulanValidasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanValidasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanValidasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
