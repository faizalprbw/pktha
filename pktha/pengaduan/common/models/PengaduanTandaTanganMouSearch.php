<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanTandaTanganMou;

/**
 * PengaduanTandaTanganMouSearch represents the model behind the search form about `common\models\PengaduanTandaTanganMou`.
 */
class PengaduanTandaTanganMouSearch extends PengaduanTandaTanganMou
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan', 'id'], 'integer'],
            [['dokumen_mou_filename', 'created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengaduanTandaTanganMou::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengaduan' => $this->id_pengaduan,
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            // 'finished' => $this->finished,
            'finished' => false,
        ]);
        
        $query->join('LEFT JOIN', 'pengaduan_assesmen', 'pengaduan_assesmen.id_pengaduan = pengaduan_tanda_tangan_mou.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_pra_mediasi', 'pengaduan_pra_mediasi.id_pengaduan = pengaduan_tanda_tangan_mou.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_mediasi', 'pengaduan_mediasi.id_pengaduan = pengaduan_tanda_tangan_mou.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_drafting_mou', 'pengaduan_drafting_mou.id_pengaduan = pengaduan_tanda_tangan_mou.id_pengaduan');
        $query->where(['pengaduan_assesmen.finished' => true]);
        $query->where(['pengaduan_pra_mediasi.finished' => true]);
        $query->where(['pengaduan_mediasi.finished' => true]);
        $query->where(['pengaduan_drafting_mou.finished' => true]);

        $query->andFilterWhere(['like', 'dokumen_mou_filename', $this->dokumen_mou_filename]);

        return $dataProvider;
    }
}
