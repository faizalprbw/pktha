<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%pengusulan_registrasi}}".
 *
 * @property string $id
 * @property string $nama_pengusul
 * @property string $alamat_lengkap_pengusul
 * @property string $email_pengusul
 * @property string $no_telepon_pengusul
 * @property string $modified_date
 * @property string $id_wilayah
 * @property string $id_provinsi
 * @property string $id_kota_kabupaten
 * @property string $teks_kota_kabupaten
 * @property string $id_kecamatan
 * @property string $teks_kecamatan
 * @property string $id_desa_kelurahan
 * @property string $teks_desa_kelurahan
 * @property string $dusun
 * @property string $keterangan
 * @property string $form_filename
 * @property string $profil_mha_filename
 * @property string $surat_kuasa_filename
 * @property string $surat_pernyataan_filename
 * @property string $produk_hukum_daerah_filename
 * @property string $peta_filename
 * @property string $identitas_ketua_adat_filename
 * @property string $created_date
 * @property string $kode_pengusulan
 * @property string $kunci_pengusulan
 * @property string $profil_mha
 * @property string $tanggal_pengusulan 
 * @property string $nama_mha 
 */
class PengusulanRegistrasi extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pengusulan_registrasi';
    }

    public $model_name = 'pengusulan_registrasi';

    /**
     * @var
     */
    public $uploaded_file;
    public $form_data;
    public $profil_mha_data;
    public $surat_kuasa_data;
    public $surat_pernyataan_data;
    public $produk_hukum_daerah_data;
    public $peta_data;
    public $identitas_ketua_adat_data;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[
            'kode_pengusulan',
            'kunci_pengusulan',
            'nama_pengusul',
            'alamat_lengkap_pengusul',
            'email_pengusul',
            'teks_provinsi',
            'teks_kota_kabupaten',
            'teks_kecamatan',
            'teks_desa_kelurahan',
            'dusun',
            'keterangan',
            'form_filename',
            'profil_mha_filename',
            'surat_kuasa_filename',
            'surat_pernyataan_filename',
            'produk_hukum_daerah_filename',
            'peta_filename',
            'identitas_ketua_adat_filename', 
            'nama_mha'], 'string'],
            [[
            'no_ktp_pengusul',
            'no_telepon_pengusul'], 'number'],
            [[
            'id_wilayah',
            'id_provinsi',
            'id_kota_kabupaten',
            'id_kecamatan',
            'id_desa_kelurahan'], 'integer'],
            [[
            'nama_pengusul',
            'alamat_lengkap_pengusul',
            'id_provinsi',
            'no_telepon_pengusul'], 'required'],
            [[
            'form_data',
            'profil_mha_data',
            'surat_kuasa_data',
            'surat_pernyataan_data',
            'produk_hukum_daerah_data'], 'file', 'extensions' => 'pdf, doc, docx, odt'],
            [[
            'peta_data'], 'file', 'extensions' => 'zip, rar'],
            [[
            'identitas_ketua_adat_data'], 'file', 'extensions' => 'pdf, doc, docx, odt, jpg, jpeg'],
            [[
            'modified_date',
            'created_date',
            'tanggal_pengusulan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_pengusul' => Yii::t('app', 'Nama Pengusul (wajib)'),
            'no_ktp_pengusul' => Yii::t('app', 'Nomor KTP Pengusul'),
            'alamat_lengkap_pengusul' => Yii::t('app', 'Alamat Lengkap Pengusul (wajib)'),
            'email_pengusul' => Yii::t('app', 'Email Pengusul'),
            'no_telepon_pengusul' => Yii::t('app', 'Nomor Telepon Pengusul (wajib)'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'id_wilayah' => Yii::t('app', 'Wilayah'),
            'id_provinsi' => Yii::t('app', 'Provinsi (wajib)'),
            'id_kota_kabupaten' => Yii::t('app', 'Kota / Kabupaten'),
            'teks_kota_kabupaten' => Yii::t('app', 'Kota / Kabupaten'),
            'id_kecamatan' => Yii::t('app', 'Kecamatan'),
            'teks_kecamatan' => Yii::t('app', 'Kecamatan'),
            'id_desa_kelurahan' => Yii::t('app', 'Desa / Kelurahan'),
            'teks_desa_kelurahan' => Yii::t('app', 'Desa / Kelurahan'),
            'dusun' => Yii::t('app', 'Dusun'),
            'keterangan' => Yii::t('app', 'Keterangan Lain terkait Lokasi Usulan Hutan Adat'),
            'form_filename' => Yii::t('app', 'Formulir Pengajuan'),
            'profil_mha_filename' => Yii::t('app', 'Profile MHA'),
            'surat_kuasa_filename' => Yii::t('app', 'Surat Kuasa'),
            'surat_pernyataan_filename' => Yii::t('app', 'Surat Pernyataan'),
            'produk_hukum_daerah_filename' => Yii::t('app', 'Produk Hukum Daerah'),
            'peta_filename' => Yii::t('app', 'Peta'),
            'identitas_ketua_adat_filename' => Yii::t('app', 'Identitas Ketua Adat'),
            'kode_pengusulan' => Yii::t('app', 'Kode Pengusulan'),
            'profil_mha' => Yii::t('app', 'Masyarakat Hutan Adat'),
            'tanggal_pengusulan' =>Yii::t('app', 'Tanggal Pengusulan'),
            'nama_mha' => Yii::t('app', 'Nama Masyarakat Hukum Adat')
        ];
    }

    /**
     * @inheritdoc
     * @return PengusulanRegistrasiQuery the active query used by this AR class.
     */
    public static function find() {
        return new PengusulanRegistrasiQuery(get_called_class());
    }

    /**
     * Saves the file resource stored in uploaded_file, which is instantiated
     * in the controller, to updir.
     *
     * @param  string $key
     * @return boolean
     */
    public function upload($key) {
        if ($this->validate()) {
            $name = $this->uploaded_file->baseName . '.' . $this->uploaded_file->extension;
            $path = $this->getPath() . '/' . $key;
            /* Check if a file is already uploaded for this $key and delete */
            $attr = $key . '_filename';
            $cur = $path . '/' . $this->$attr;
            if (is_file($cur)) {
                array_map('unlink', glob($cur));
            }
            // Save file and update filename attribute
            if ($this->uploaded_file->saveAs($path . '/' . $name)) {
                $this->$attr = $name;
                return true;
            }
        }
        return false;
    }

    /**
     * Get path to model's uploaded files. Uses model name, ID, and
     * kode_pengusulan as identifiers of the subdirectory to store files.
     *
     * @return string
     */
    public function getPath() {
        return Yii::getAlias('@updir') . '/' . $this->model_name . '/' . $this->id . '-' . $this->kode_pengusulan;
    }

    /**
     * @inheritdoc
     * @return Tahapan the first unfinished Tahapan for this PengaduanRegistrasi
     */
    public function getTahapanFirstUnfinishedId() {
        $role = Yii::$app->user->identity->role;
        // $wilayah = Yii::$app->user->identity->wilayah;
        $mohon = null;
        $validasi = null;

        /* if ($role < 30) {
          $mohon = PengusulanPermohonan::findOne(['id_pengusulan' => $this->id, 'id_wilayah' => $wilayah]);
          $validasi = PengusulanValidasi::findOne(['id_pengusulan' => $this->id, 'id_wilayah' => $wilayah]);
          } else { */
        $mohon = PengusulanPermohonan::findOne(['id_pengusulan' => $this->id]);
        // $validasi = PengusulanValidasi::findOne(['id_pengusulan' => $this->id]);
        // }
        // An error occured when accessing different pages.
        // NULL indicated, NULL checking for first aid.
        if ((isset($mohon) && $mohon->dikembalikan === true)) {
            return array(
                'nama' => 'Dikembalikan',
                'warna' => 'black');
        }

        $tahap = Tahapan::getTahapanByProses('pengusulan');
        for ($i = 1; $i < count($tahap) - 1; $i++) {
            $row = $tahap[$i]['model']::findOne(['id_pengusulan' => $this->id]);
            // Return first unfinished tahap
            if ($row['finished'] === null || $row['finished'] == false) {
                return $tahap[$i];
            }
        }
        // Return tahap Selesai if all finished
        return $tahap[count($tahap) - 1];
    }

}
