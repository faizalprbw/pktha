<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengaduanPraMediasi]].
 *
 * @see PengaduanPraMediasi
 */
class PengaduanPraMediasiQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * @inheritdoc
     * @return PengaduanPraMediasi[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengaduanPraMediasi|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * 
     * @param type $id id dari record yang dicari
     * @return type record yang dicari. Null jika tidak ditemukan.
     */
    public function getById($id) {
        return $this->select(['*'])->where(['id' => $id])->one();
    }

    /**
     * 
     * @param type $id_pengaduan id
     * @return type assesmen terkait dengan pengaduan sesuai dengan id pengaduan.
     */
    public function getByPengaduanId($id_pengaduan) {
        return $this->select(['*'])->where(['id_pengaduan' => $id_pengaduan])->one();
    }

}
