<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%kecamatan}}".
 *
 * @property string $id_kota_kabupaten
 * @property string $nama_kecamatan
 * @property string $id
 * @property double $latitude
 * @property double $longitude
 */
class Kecamatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kecamatan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id_kota_kabupaten'], 'required'],
            [['id_kota_kabupaten'], 'integer'],
            [['nama_kecamatan'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['id_kota_kabupaten'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['id_kota_kabupaten' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kota_kabupaten' => Yii::t('app', 'Id Kota Kabupaten'),
            'nama_kecamatan' => Yii::t('app', 'Nama kecamatan'),
            'id' => Yii::t('app', 'Id'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
        ];
    }

    /**
     * @inheritdoc
     * @return KecamatanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KecamatanQuery(get_called_class());
    }
}
