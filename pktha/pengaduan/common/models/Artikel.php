<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "artikel".
 *
 * @property integer $id
 * @property integer $kategori_id
 * @property string $judul
 * @property string $headline
 * @property string $konten
 * @property string $image
 * @property integer $publish
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $imageFile;

    public static function tableName()
    {
        return 'artikel';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    // public function relations()
    // {
    //     return array(
    //         'user_id'=>array(self::BELONGS_TO, 'User', 'author_id'),
    //         'categories'=>array(self::MANY_MANY, 'Category',
    //             'tbl_post_category(post_id, category_id)'),
    //     );
    // }

    public function getUser(){
                return $this->hasOne(User::className(), ['id' => 'user_id']);          
     }
     public function getKategori(){
                return $this->hasOne(Kategori::className(), ['id' => 'kategori_id']);          
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori_id', 'judul',  'konten',  'publish'], 'required'],
            [['kategori_id', 'publish', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['konten','caption'], 'string'],
            [['judul'], 'string', 'max' => 100],
            [['headline'], 'string', 'max' => 100],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,jpeg, pdf'],
            // [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],            
            [['image'], 'string', 'max' => 100]
        ];
    }
     public function upload()
    {
        if ($this->validate()) {
            
            $this->image->saveAs(Yii::getAlias('@updir').'/uploads/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){       
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }

    //     public function beforeValidate() {
    //     if(parent::beforeValidate()) {

    //         $os = array("png", 'jpg','jpeg');
    //         // var_dump($this->imageFile->extension);
    //         // exit();

    //         // var_dump($this->isTransactional(self::OP_DELETE));
    //         // exit();

    //         if ($this->isTransactional(self::OP_DELETE)) {


    //                 $x=in_array(strtolower($this->imageFile->extension),$os );
    //                 if($this->kategori_id == 23 && strtolower($this->imageFile->extension) == "pdf" ){
    //                     return true;
    //                 }
    //                 elseif($this->kategori_id != 23 && $x){
    //                     return true;
    //                 }

    //         }
      

    //     }
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kategori_id' => 'Kategori ID',
            'judul' => 'Judul',
            'headline' => 'Headline',
            'konten' => 'Konten',
            'image' => 'Image',
            'publish' => 'Publish',
            'user_id' => 'User ID',
            'caption' => 'Caption',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    // public function search($params)
    // {
    //     $query = Artikel::find();

    //     // add conditions that should always apply here

    //     $dataProvider = new ActiveDataProvider([
    //         'query' => $query,
    //     ]);

    //     $this->load($params);

    //     if (!$this->validate()) {
    //         // uncomment the following line if you do not want to return any records when validation fails
    //         // $query->where('0=1');
    //         return $dataProvider;
    //     }

         
    //     // grid filtering conditions
    //     $query->andFilterWhere([
    //         'id' => $this->id,
    //         'kategori_id' => $this->kategori_id,
    //         'judul' => $this->judul,
    //         'headline' => $this->headline,
    //         'konten' => $this->konten,
    //         'caption' => $this->caption            
    //     ]);

    //     $query->andFilterWhere(['like', 'judul', $this->judul])
    //         ->andFilterWhere(['like', 'headline', $this->headline])
    //         ->andFilterWhere(['like', 'konten', $this->konten])
    //         ->andFilterWhere(['like', 'caption', $this->caption]);
            

    //     return $dataProvider;
    // }
}
