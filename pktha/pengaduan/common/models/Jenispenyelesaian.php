<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jenispenyelesaian".
 *
 * @property integer $id
 * @property string $nama
 * @property string $keterangan
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Jenispenyelesaian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenispenyelesaian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'keterangan'], 'required'],
            [['created_at', 'updated_at', 'user_id'], 'integer'],
            [['nama'], 'string', 'max' => 50],
            [['keterangan'], 'string', 'max' => 30]
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){                
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
}
