<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "kategori".
 *
 * @property integer $id
 * @property integer $kategori
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Kategori extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {        
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){                
                $this->user_id=Yii::$app->user->getId(); 
            }
            return true;
        } else {
            return false;
        }
    }

    // public function relations()
    // {
    //     return array(
    //         "User"  => array(self::BELONGS_TO,"User","user_id")
    //        );
    // }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori'], 'required']            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kategori' => 'Kategori',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }
}
