<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "pengaduan".
 *
 * @property integer $id
 * @property string $kode
 * @property integer $id_tahapan
 * @property integer $status
 * @property integer $tipe_identitas
 * @property string $nama_identitas
 * @property string $telepon_identitas
 * @property string $email_identitas
 * @property string $alamat_identitas
 * @property string $sarana_pengaduan
 * @property string $nomor_surat
 * @property string $tgl_surat
 * @property string $tgl_penerima_pengaduan
 * @property string $tgl_penerima_pktha
 * @property string $penerima_pengaduan
 * @property string $pihak_berkonflik
 * @property string $lokasi_konflik
 * @property integer $kabkota_konflik
 * @property integer $provinsi_konflik
 * @property string $lot_perkiraaan_lokasi
 * @property string $lang_perkiraan_lokasi
 * @property string $fungsi_kawasan
 * @property string $luasan_kawasan
 * @property string $tipologi_kasus
 * @property string $status_kawasan
 * @property string $rentang_waktu
 * @property string $resume
 * @property string $tutuntan_pengaduan
 * @property string $upaya
 * @property string $pihak_terlibat
 * @property integer $created_at
 * @property integer $update_at
 * @property integer $user_id
 */
class Pengaduan extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pengaduan';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        // [['id_tahapan', 'status', 'tipe_identitas', 'nama_identitas', 'telepon_identitas', 'email_identitas', 'alamat_identitas', 'sarana_pengaduan', 'nomor_surat', 'tgl_surat', 'tgl_penerima_pengaduan', 'tgl_penerima_pktha', 'penerima_pengaduan', 'pihak_berkonflik', 'lokasi_konflik', 'kabkota_konflik', 'provinsi_konflik', 'lot_perkiraaan_lokasi', 'lang_perkiraan_lokasi', 'fungsi_kawasan', 'luasan_kawasan', 'tipologi_kasus', 'status_kawasan', 'rentang_waktu', 'resume', 'tutuntan_pengaduan', 'upaya', 'pihak_terlibat'], 'required'],
        return [
                [['tipe_identitas', 'nama_identitas', 'telepon_identitas', 'email_identitas', 'alamat_identitas', 'lokasi_konflik', 'kabkota_konflik', 'provinsi_konflik', 'rentang_waktu', 'tutuntan_pengaduan'], 'required'],
                [['id', 'id_tahapan', 'status', 'tipe_identitas', 'kabkota_konflik', 'provinsi_konflik', 'created_at', 'updated_at', 'user_id', 'wilayah'], 'integer'],
                [['alamat_identitas'], 'string'],
                [['tgl_surat', 'tgl_penerima_pengaduan', 'tgl_penerima_pktha', 'rentang_waktu'], 'safe'],
                [['lot_perkiraaan_lokasi', 'lang_perkiraan_lokasi'], 'number'],
                [['kode', 'nomor_surat'], 'string'],
                [['nama_identitas', 'tipologi_kasus'], 'string'],
                [['telepon_identitas'], 'string', 'max' => 15],
                [['email_identitas', 'sarana_pengaduan', 'fungsi_kawasan', 'pihak_terlibat'], 'string'],
                [['penerima_pengaduan', 'pihak_berkonflik', 'status_kawasan', 'resume'], 'string'],
                [['luasan_kawasan'], 'string'],
                [['tutuntan_pengaduan'], 'string'],
                [['upaya'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'id_tahapan' => 'Id Tahapan',
            'status' => 'Status',
            'tipe_identitas' => 'Tipe Identitas',
            'nama_identitas' => 'Nama Identitas',
            'telepon_identitas' => 'Telepon Identitas',
            'email_identitas' => 'Email Identitas',
            'alamat_identitas' => 'Alamat Identitas',
            'sarana_pengaduan' => 'Sarana Pengaduan',
            'nomor_surat' => 'Nomor Surat',
            'tgl_surat' => 'Tgl Surat',
            'tgl_penerima_pengaduan' => 'Tgl Penerima Pengaduan',
            'tgl_penerima_pktha' => 'Tgl Penerima Pktha',
            'penerima_pengaduan' => 'Penerima Pengaduan',
            'pihak_berkonflik' => 'Pihak Berkonflik',
            'lokasi_konflik' => 'Lokasi Konflik',
            'kabkota_konflik' => 'Kabkota Konflik',
            'provinsi_konflik' => 'Provinsi Konflik',
            'lot_perkiraaan_lokasi' => 'Lot Perkiraaan Lokasi',
            'lang_perkiraan_lokasi' => 'Lang Perkiraan Lokasi',
            'fungsi_kawasan' => 'Fungsi Kawasan',
            'luasan_kawasan' => 'Luasan Kawasan',
            'tipologi_kasus' => 'Tipologi Kasus',
            'status_kawasan' => 'Status Kawasan',
            'rentang_waktu' => 'Perkiraaan lama terjadi konflik', //'Rentang Waktu',
            'resume' => 'Uraian Pengaduan',
            'tutuntan_pengaduan' => 'Tuntutan Pengaduan',
            'upaya' => 'Upaya penanganan yang pernah dilakukan',
            'pihak_terlibat' => 'Aktor yang terlibat',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'wilayah' => 'Wilayah',
            'gakum' => 'Penegakan Hukum',
        ];
    }

    public function getKabkotList($pid) {
        $model = Kabkota::findAll(array('proid' => $pid));
        return \yii\helpers\ArrayHelper::map($model, 'kabid', 'kabkota');
    }

    public function getProvinsi($pid) {
        $model = Provinsi::findOne($pid);
        return $model['provinsi'];
    }

    public function getKabkota($pid) {
        $model = Kabkota::findOne($pid);
        return $model['kabkota'];
    }

    public function getListWilayah() {
        return [1 => 'Sumatera', 2 => "Kalimantan", 3 => 'Sulawesi', 4 => "Jawa Bali Nusa Tenggara", 5 => "Maluku Papua"];
    }

    public function getListKawasan() {
        return [1 => 'Hutan Produksi Tetap', 2 => "Hutan Produksi Terbatas", 3 => 'Hutan Produksi Konversi', 4 => 'Hutan Lindung', 5 => 'Hutan Konservasi'];
    }

    public function getListStatusKawasan() {
        return [1 => 'Kawasan Hutan', 2 => "Bukan Kawasan Hutan"];
    }

    public function getStatusx($id = 0) {
        $z = array('0' => 'Dokumen tidak lengkap', '1' => 'Dokumen Lengkap');
        $idx = ($id == null) ? 0 : $id;
        return $z[$idx];
    }

    public function getDokumen() {
        return $this->hasMany(Dokumen::className(), ['tipe_dokumen' => 'id']);
    }

    public function getDokumenid($id) {
        return $this->hasMany(Dokumen::className(), ['tipe_dokumen' => 'id'])
                        ->where('id_tahap = :id_tahap', [':id_tahap' => $id]);
    }

    public function getDokumenidassesment($id, $ida) {
        return $this->hasMany(Dokumen::className(), ['tipe_dokumen' => 'id'])
                        ->where('id_tahap = :id_tahap and id_assesment = :id_assesment', [':id_tahap' => $id, ':id_assesment' => $ida]);
    }

    public function getTahapan() {
        return $this->hasOne(Tahapan::className(), ['id' => 'id_tahapan']);
    }

    public function getTidaklanjut() {
        return $this->hasOne(Rekomendasi::className(), ['pengaduan_id' => 'id']);
    }

    public function getHasildanrekomendasi() {
        return $this->hasOne(Hasildanrekomendasi::className(), ['pengaduan_id' => 'id']);
    }

    public function getStatusxxx() {
        // $arr=array('1' => 'Tidak Layak','2' => 'Layak');
        $arrayName = array(['id' => 1, 'nama' => 'Layak'], ['id' => 2, 'nama' => 'Tidak Layak']);
        // $cat = \yii\helpers\ArrayHelper::map($arrayName, 'id', 'nama');
        return $arrayName;
    }

    public function getKode($code) {
        $kode = "";
        //Pengaduan akan mendapatkan Nomor Registrasi, dengan format X-YYYYMM-0000
        //Ket: X=A: Website, B: Inputan dari Admin, C: Excel / Loket
        //Pengaduan bisa bersumber dari surat. Pengkodean sumber media pengaduan juga diganti. (surat M, loket B, Sms T, website F, email E). 
        // '1' => 'Website','2' => 'Surat','3' => 'Loket','4' => 'Sms/Email')
        // '1' => 'Website','2' => 'Surat','3' => 'Loket','4' => 'Email','5'=>'SMS'
        switch ($code) {
            // surat M, loket B, Sms T, website F, email E)
            case '1':
                $x = "F";
                break;
            case '2':
                $x = "M";
                break;
            case '3':
                $x = "B";
                break;
            case '4':
                $x = "E";
                break;
            default:
                $x = "T";
                $code = 1;
                break;
        }
        $z = $this->find()->where(['sarana_pengaduan' => $code])->orderBy('created_at DESC')->one();

        if ($z != null) {
            $darray = $z->toArray();

            $dexp = explode("-", $darray['kode']);
            $c = (int) $dexp[2] + 1;
        } else {
            $c = 1;
        }

        $d = date("Ym");
        $kode = $x . "-" . $d . "-" . str_pad($c, 4, '0', STR_PAD_LEFT);
        ;

        return $kode;
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {

                $this->kode = $this->getKode($this->sarana_pengaduan);
                $this->user_id = Yii::$app->user->getId();
            }
            return true;
        } else {
            return false;
        }
    }

    // public function upload()
    // {
    //     if ($this->validate()) {            
    //         $this->url->saveAs(Yii::getAlias('@updir').'/uploads/' . $this->url->baseName . '.' . $this->url->extension);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
