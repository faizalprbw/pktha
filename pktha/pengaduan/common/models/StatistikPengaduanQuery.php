<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StatistikPengaduan]].
 *
 * @see StatistikPengaduan
 */
class StatistikPengaduanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StatistikPengaduan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatistikPengaduan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
