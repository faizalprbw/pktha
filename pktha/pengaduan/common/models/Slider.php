<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $img
 */
class Slider extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['desc'], 'string'],
                [['created_at', 'updated_at'], 'integer'],
                [['name'], 'string', 'max' => 20],
                [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'img' => 'Img',
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function upload() {

        if ($this->validate()) {
            $fname = md5(microtime()) . '.' . $this->img->extension;
            $this->img->saveAs(dirname(dirname(__DIR__)) . '/uploads' . '/slider/' . $fname);
            // $this->img->saveAs(Yii::getAlias('@updir') . '/slider/' . $fname);
            $this->img = $fname;
            $this->save();
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->user_id = Yii::$app->user->getId();
            }
            return true;
        } else {
            return false;
        }
    }

}
