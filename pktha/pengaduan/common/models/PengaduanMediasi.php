<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_mediasi".
 *
 * @property string $id_pengaduan
 * @property string $data_para_pihak
 * @property string $pokok_permasalahan
 * @property string $objek_mediasi
 * @property string $proses_pertemuan
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $berita_acara_filename
 * @property string $foto_filename
 * @property string $surat_usulan_mediator_filename
 * @property string $sk_mediator_filename
 */
class PengaduanMediasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_mediasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['data_para_pihak', 'pokok_permasalahan', 'objek_mediasi', 'proses_pertemuan'], 'string'],
            [['berita_acara_filename', 'foto_filename', 'surat_usulan_mediator_filename', 'sk_mediator_filename'], 'file', 'extensions' => 'pdf, doc, docx, odt'],
            [['foto_filename'], 'file', 'extensions' => 'jpg, jpeg, bmp'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => Yii::t('app', 'Id Pengaduan'),
            'data_para_pihak' => Yii::t('app', 'Data Para Pihak'),
            'pokok_permasalahan' => Yii::t('app', 'Pokok Permasalahan'),
            'objek_mediasi' => Yii::t('app', 'Objek Mediasi'),
            'proses_pertemuan' => Yii::t('app', 'Proses Pertemuan'),
            'id' => Yii::t('app', 'ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Finished'),
            'berita_acara_filename' => Yii::t('app', 'Berita Acara'),
            'foto_filename' => Yii::t('app', 'Foto'),
            'surat_usulan_mediator_filename' => Yii::t('app', 'Surat Usulan Mediator'),
            'sk_mediator_filename' => Yii::t('app', 'SK Mediator'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanMediasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanMediasiQuery(get_called_class());
    }
}
