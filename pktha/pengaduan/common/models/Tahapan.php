<?php

namespace common\models;

use Yii;
use \yii\db\Query;

/**
 * This is the model class for table "tahapan".
 *
 * @property integer $id
 * @property string $name
 * @property string $process
 * @property string $model
 * @property string $table
 * @property string $color
 */
class Tahapan extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'tahapan';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [[
            'nama',
            'proses',
            'order',
            'model',
            'tabel',
            'keterangan',
            'warna'], 'string', 'max' => 20],
            [[
            'urutan'], 'integer'],
            [[
            'nama',
            'proses'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'nama' => Yii::t('app', 'Nama tahapan'),
            'proses' => Yii::t('app', 'Proses'),
            'urutan' => Yii::t('app', 'Urutan tahapan dalam proses bersangkutan'),
            'model' => Yii::t('app', 'Path ke kelas Model'),
            'tabel' => Yii::t('app', 'Nama tabel dalam basis data'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'warna' => Yii::t('app', 'Warna tahapan di View'),
        ];
    }

    /**
     * @inheritdoc
     * @return TahapanQuery the active query used by this AR class.
     */
    public static function find() 
    {
        return new TahapanQuery(get_called_class());
    }

    public static function getTahapanByProses($proses) 
    {
        //$cache = Yii::$app->cache;
        //$key = array('model', 'tahapan', 'getTahapanByProses', $proses);
        //$dbdep = new \yii\caching\DbDependency(['sql' => 'select count(id) from tahapan']);

        $tahap = false;
        //$tahap = $cache->get($key);
        if ($tahap === false) {
            $tahap = Tahapan::find()
                    ->where(['proses' => $proses])
                    ->orderBy('urutan')
                    ->all();
            //$cache->set($key, $tahap, 0, $dbdep);
        }
        return $tahap;
    }

    public static function getAllTahapanById($id, $proses) 
    {
        $tahap = Tahapan::getTahapanByProses($proses);
        $models = array();
        $models[$tahap[0]['tabel']] = array(
            'model' => $tahap[0]['model']::findOne(['id' => $id]),
            'nama' => $tahap[0]['nama'],
            'warna' => $tahap[0]['warna']);

        for ($i = 1; $i < count($tahap); $i++) {
            $ti = $tahap[$i];
            if (isset($ti['model'])) {
                $models[$ti['tabel']] = array(
                    'model' => $ti['model']::findOne(['id_' . $proses => $id]),
                    'nama' => $ti['nama'],
                    'warna' => $ti['warna'],
                );
            }
        }
        return $models;
    }

    public static function getDataByProses($proses) 
    {
        $tahap = Tahapan::getTahapanByProses($proses);
        //$cache = Yii::$app->cache;
        //$key = array('model', 'tahapan', 'getDataByProses', $proses);
        //$data = $cache->get($key);
        $data = false;
        if ($data === false) {
            $data = array(
                'models' => array(),
                'columns' => array()
            );

            $t0 = $tahap[0]['model']::find()->all();
            Yii::getLogger()->log($t0[0], \yii\log\Logger::LEVEL_INFO, __METHOD__);
            $data['models'][] = $tahap[0]['model'];
            $ids = array();
            foreach ($t0 as $row) {
                $ids[] = $row['id'];
            }

            //$cache->set($key, $data);
        }
        return $data;
    }

    public static function countDialihkan($role) 
    {
        //$cache = Yii::$app->cache;
        //$key = array('model', 'tahapan', 'countDialihkan', $role);

        $res = false;
        //$res = $cache->get($key);
        if ($res === false) {
            $rows = \common\models\PengaduanAssesmen::find()
                    ->select('id_pengaduan')
                    ->where(['dialihkan' => true])
                    ->all();
            $assesmen = Tahapan::find()
                    ->select(['nama', 'warna'])
                    ->where(['tabel' => 'pengaduan_assesmen'])
                    ->one();
            $ids = array();
            for ($i = 0; $i < count($rows); $i++) {
                $ids[$i] = $rows[$i]['id_pengaduan'];
            }
            $res = array(
                'pengaduan_assesmen' => array(
                    'count' => count($rows),
                    'nama' => $assesmen['nama'],
                    'warna' => $assesmen['warna'],
                    'rows' => $ids),
            );
            //$cache->set($key, $res, 3600);
        }
        return $res;
    }

    public static function countAllTahapanFinished($role, $proses) 
    {
        //$cache = Yii::$app->cache;
        //$key = array('model', 'tahapan', 'countAllTahapanFinished', $proses);
        // Check if there is user logged in.
        if (isset(Yii::$app->user)) {
            $role = Yii::$app->user->identity->role;
            $wilayah = Yii::$app->user->identity->wilayah;
        } else {

            // Set default to admin.
            $role = 30;
        }

        $res = false;
        //$res = $cache->get($key);
        if ($res === false) {
            $tahap = Tahapan::getTahapanByProses($proses);
            $res = array();

            // Get models and tabels from child table, not registration tables which are pengaduan_registrasi and pengusulan_registrasi.
            for ($i = 1; $i < count($tahap); $i++) {
                if (isset($tahap[$i]['model'])) {

                    $rows = null;
                    /* if ($role < 30) {
                      $rows = $tahap[$i]['model']::find()
                      ->select('id_' . $proses)
                      ->where(['finished' => true])
                      ->join('LEFT JOIN', $proses . '_registrasi', $tahap[$i]['tabel'] . 'id_' . $proses . ' = ' . $proses . 'registrasi.id')
                      ->where([$proses . '_registrasi.id_wilayah' . ($proses === 'pengaduan' ? '_konflik' : '') => $wilayah])
                      ->all();
                      } else { */
                    $rows = $tahap[$i]['model']::find()
                            ->select('id_' . $proses)
                            ->where(['finished' => true])
                            ->all();
                    // }

                    $ids = array();
                    for ($j = 0; $j < count($rows); $j++) {
                        $ids[$j] = $rows[$j]['id_' . $proses];
                    }
                    $res[$tahap[$i]['nama']] = array(
                        'count' => count($rows),
                        'warna' => $tahap[$i]['warna'],
                        'rows' => $ids
                    );
                }
            }
            //$cache->set($key, $res, 3600);
        }
        return $res;
    }

    /**
     * 
     * @param type $role   admin access code
     * @param type $proses either 'pengaduan' or 'pengusulan'.
     * @return 
     */
    public static function countAllTahapanFirstUnfinished($role, $proses) 
    {
        //$cache = Yii::$app->cache;
        //$key = array('model', 'tahapan', 'countAllTahapanFirstUnfinished', $proses, $role);

        $res = false;
        //$res = $cache->get($key);
        if ($res === false) {
            // Query from all Tahapan in $proses
            $tahap = Tahapan::getTahapanByProses($proses);
            $tables = array();
            for ($i = 0; $i < count($tahap); $i++) {
                if (isset($tahap[$i]['tabel'])) {
                    $tables[] = $tahap[$i]['tabel'];
                }
            }

            $res = array();

            // Populate tahap Pendaftaran
            $t0 = $tahap[0];
            $daftar = $t0['model']::find()
                    ->select('id')
                    ->all();
            $res[$t0['nama']] = array(
                'count' => count($daftar),
                'warna' => $t0['warna'],
                'rows' => $daftar);

            // Get current wilayah role

            if (!isset(Yii::$app->user)) {
                $role = Yii::$app->user->identity->role;
                $wilayah = Yii::$app->user->identity->wilayah;
            } else {
                $role = 30;
            }

            // Populate Dialihkan/Dikembalikan
            if ($proses == 'pengaduan') {
                /* $desk = $role < 30 ?
                  PengaduanDeskStudy::find()
                  ->select(PengaduanDeskStudy::tableName() . '.id')
                  ->from(PengaduanDeskStudy::tableName())
                  ->where(['dialihkan' => true])
                  ->join('LEFT JOIN', PengaduanRegistrasi::tableName(), PengaduanDeskStudy::tableName() . '.id_pengaduan = ' . PengaduanRegistrasi::tableName() . '.id')
                  ->where([PengaduanRegistrasi::tableName() . '.id_wilayah_konflik' => $wilayah])
                  ->all() :
                  PengaduanDeskStudy::find()
                  ->select('id')
                  ->where(['dialihkan' => true])
                  ->all(); */
                $desk = PengaduanDeskStudy::find()
                        ->select('id')
                        ->where(['dialihkan' => true])
                        ->all();
                /* $asses = $role < 30 ?
                  PengaduanAssesmen::find()
                  ->select(PengaduanAssesmen::tableName() . '.id')
                  ->from(PengaduanAssesmen::tableName())
                  ->where(['dialihkan' => true])
                  ->join('LEFT JOIN', PengaduanRegistrasi::tableName(), PengaduanAssesmen::tableName() . '.id_pengaduan = ' . PengaduanRegistrasi::tableName() . '.id')
                  ->where([PengaduanRegistrasi::tableName() . '.id_wilayah_konflik' => $wilayah])
                  ->all() :
                  PengaduanAssesmen::find()
                  ->select('id')
                  ->where(['dialihkan' => true])
                  ->all(); */
                $asses = PengaduanAssesmen::find()
                        ->select('id')
                        ->where(['dialihkan' => true])
                        ->all();
                $res['Dialihkan'] = array(
                    'count' => count($desk) + count($asses),
                    'rows' => array_merge($desk, $asses)
                );
            } else if ($proses == 'pengusulan') {
                /* $mohon = $role < 30 ?
                  PengusulanPermohonan::find()
                  ->select(PengusulanPermohonan::tableName() . '.id')
                  ->from(PengusulanPermohonan::tableName())
                  ->where(['dikembalikan' => true])
                  ->join('LEFT JOIN', PengusulanRegistrasi::tableName(), PengusulanPermohonan::tableName() . '.id_pengusulan = ' . PengusulanRegistrasi::tableName() . '.id')
                  ->where([PengusulanRegistrasi::tableName() . '.id_wilayah' => $wilayah])
                  ->all() :
                  PengusulanPermohonan::find()
                  ->select('id')
                  ->where(['dikembalikan' => true])
                  ->all(); */
                $mohon = PengusulanPermohonan::find()
                        ->select('id')
                        ->where(['dikembalikan' => true])
                        ->all();
                /* $valid = $role < 30 ?
                  PengusulanValidasi::find()
                  ->select(PengusulanValidasi::tableName() . '.id')
                  ->from(PengusulanValidasi::tableName())
                  ->where(['dikembalikan' => true])
                  ->join('LEFT JOIN', PengusulanRegistrasi::tableName(), PengusulanValidasi::tableName() . '.id_pengusulan = ' . PengusulanRegistrasi::tableName() . '.id')
                  ->where([PengusulanRegistrasi::tableName() . '.id_wilayah' => $wilayah])
                  ->all() :
                  PengusulanValidasi::find()
                  ->select('id')
                  ->where(['dikembalikan' => true])
                  ->all(); */
                $valid = PengusulanValidasi::find()
                        ->select('id')
                        ->where(['dikembalikan' => true])
                        ->all();
                $res['Dikembalikan'] = array(
                    'count' => count($mohon) + count($valid),
                    'rows' => array_merge($mohon, $valid)
                );
            }

            // For each other Tahapan, get rows in Tahapan's table for which all previous
            // tahapan are finished and all subsequent tahapan are unfinished
            for ($i = 1; $i < count($tahap); $i++) {
                $where = '';
                $ti = $tahap[$i];
                for ($j = 1; $j < count($tahap) - 1; $j++) {
                    $tj = $tahap[$j];

                    $where .= '(' . $t0['tabel'] . '.id = "' . $tj['tabel'] . '".id_' . $proses . ') AND ';
                    if ($j < $i) {
                        $where .= '("' . $tj['tabel'] . '".finished = true)';
                        if (isset($res['Dialihkan']) && ($ti['nama'] == 'Desk Study' || $ti['nama'] == 'Assesmen')) {
                            $where .= ' AND ("' . $tj['tabel'] . '".dialihkan = false OR "' . $tj['tabel'] . '".dialihkan ISNULL )';
                        } else if (isset($res['Dikembalikan']) && ($ti['nama'] == 'Permohonan' || $ti['nama'] == 'Validasi')) {
                            $where .= ' AND ("' . $tj['tabel'] . '".dikembalikan = false OR "' . $tj['tabel'] . '".dikembalikan ISNULL )';
                        }
                    } else {
                        $where .= '("' . $tj['tabel'] . '".finished = false OR "' .
                                $tj['tabel'] . '".finished ISNULL )';
                    }
                    if ($j < count($tahap) - 2) {
                        $where .= ' AND ';
                    }
                }

                $rows = (new Query())
                        ->select($t0['tabel'] . '.id')
                        ->from($tables)
                        ->where($where)
                        ->all();
                $res[$ti['nama']] = array(
                    'count' => count($rows),
                    'warna' => $ti['warna'],
                    'rows' => $rows
                );
            }

            //$cache->set($key, $res, 3600);
        }
        return $res;
    }

}
