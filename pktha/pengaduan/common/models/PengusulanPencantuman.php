<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pengusulan_pencantuman}}".
 *
 * @property string $id
 * @property string $no_sk_penetapan
 * @property string $tanggal_sk_penetapan
 * @property string $perihal_sk_penetapan
 * @property double $luas_sk_penetapan
 * @property string $no_sk_pencantuman
 * @property string $tanggal_sk_pencantuman
 * @property string $perihal_sk_pencantuman
 * @property double $luas_sk_pencantuman
 * @property string $form_verifikasi_filename
 * @property string $id_pengusulan
 * @property boolean $finished
 * @property string $created_date
 * @property string $modified_date
 * @property string $latitude
 * @property string $longitude
 */
class PengusulanPencantuman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pengusulan_pencantuman}}';
    }

    public $form_verifikasi_data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_sk_penetapan', 'perihal_sk_penetapan', 'no_sk_pencantuman', 'perihal_sk_pencantuman',], 'string'],
            [['form_verifikasi_data'], 'file', 'extensions' => 'pdf, doc, docx, odt'],
            [['form_verifikasi_filename'], 'string'],
            [['tanggal_sk_penetapan', 'tanggal_sk_pencantuman', 'created_date', 'modified_date'], 'safe'],
            [['luas_sk_penetapan', 'luas_sk_pencantuman', 'latitude', 'longitude'], 'number'],
            [['id_pengusulan'], 'integer'],
            [['finished'], 'boolean'],
            [['id_pengusulan'], 'exist', 'skipOnError' => true, 'targetClass' => PengusulanRegistrasi::className(), 'targetAttribute' => ['id_pengusulan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'no_sk_penetapan' => Yii::t('app', 'No SK Penetapan'),
            'tanggal_sk_penetapan' => Yii::t('app', 'Tanggal SK Penetapan'),
            'perihal_sk_penetapan' => Yii::t('app', 'Perihal SK Penetapan'),
            'luas_sk_penetapan' => Yii::t('app', 'Luas SK Penetapan'),
            'no_sk_pencantuman' => Yii::t('app', 'No SK Pencantuman'),
            'tanggal_sk_pencantuman' => Yii::t('app', 'Tanggal SK Pencantuman'),
            'perihal_sk_pencantuman' => Yii::t('app', 'Perihal SK Pencantuman'),
            'luas_sk_pencantuman' => Yii::t('app', 'Luas SK Pencantuman'),
            'form_verifikasi_filename' => Yii::t('app', 'Form Verifikasi'),
            'id_pengusulan' => Yii::t('app', 'Id Pengusulan'),
            'finished' => Yii::t('app', 'Selesai'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'latitude' =>  Yii::t('app', 'Latitude'),
            'longitude' =>  Yii::t('app', 'Longitude'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengusulanPencantumanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengusulanPencantumanQuery(get_called_class());
    }
}
