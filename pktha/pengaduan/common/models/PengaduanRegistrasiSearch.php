<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanRegistrasi;
use common\models\User;

/**
 * PengaduanRegistrasiSearch represents the model behind the search form about `\common\models\PengaduanRegistrasi`.
 */
class PengaduanRegistrasiSearch extends PengaduanRegistrasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [[
        'nama_pengadu',
        'alamat_lengkap_pengadu',
        'email_pengadu',
        'pihak_berkonflik_1',
        'pihak_berkonflik_2',
        'pihak_berkonflik_3',
        'teks_provinsi_konflik',
        'teks_kota_kabupaten_konflik',
        'teks_kecamatan_konflik',
        'teks_desa_kelurahan_konflik',
        'fungsi_kawasan_konflik',
        'created_date',
        'modified_date',
        'kode_pengaduan'], 'string'],
        [[
        'id_tipe_konflik',
        'id_wilayah_konflik',
        'id_provinsi_konflik',
        'id_kota_kabupaten_konflik',
        'id_kecamatan_konflik',
        'id_desa_kelurahan_konflik',
        'id_jenis_pelapor'], 'integer'],
        [[
        'no_telepon_pengadu',
        'latitude_daerah_konflik',
        'longitude_daerah_konflik',
        'luas_kawasan_konflik'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     *
     * @var type string
     */
    public $keyword;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $conds = null)
    {
        $query = PengaduanRegistrasi::find();

        // add conditions that should always apply here
        if (isset($conds)) {
            foreach ($conds as $key => $item) {
                $query->andWhere(['in', $key, $item]);
            }
        }

        $dataProvider = new ActiveDataProvider(['query' => $query,]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $this->id_wilayah_konflik = isset($params['id_wilayah_konflik']) ? $params['id_wilayah_konflik'] : $this->id_wilayah_konflik;

        $query->andFilterWhere(
            [
            'id' => $this->id,
            'no_telepon_pengadu' => $this->no_telepon_pengadu,
            'id_tipe_konflik' => $this->id_tipe_konflik,
            'id_wilayah_konflik' => $this->id_wilayah_konflik,
            'id_provinsi_konflik' => $this->id_provinsi_konflik,
            'id_kota_kabupaten_konflik' => $this->id_kota_kabupaten_konflik,
            'id_kecamatan_konflik' => $this->id_kecamatan_konflik,
            'id_desa_kelurahan_konflik' => $this->id_desa_kelurahan_konflik,
            'latitude_daerah_konflik' => $this->latitude_daerah_konflik,
            'longitude_daerah_konflik' => $this->longitude_daerah_konflik,
            'luas_kawasan_konflik' => $this->luas_kawasan_konflik,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'id_jenis_pelapor' => $this->id_jenis_pelapor,
            ]
        );
        
        if (!isset($this->keyword)) {
            $query
            ->andFilterWhere(['ilike', 'nama_pengadu', $this->nama_pengadu])
            ->andFilterWhere(['ilike', 'alamat_lengkap_pengadu', $this->alamat_lengkap_pengadu])
            ->andFilterWhere(['ilike', 'email_pengadu', $this->email_pengadu])
            ->andFilterWhere(['ilike', 'pihak_berkonflik_1', $this->pihak_berkonflik_1])
            ->andFilterWhere(['ilike', 'pihak_berkonflik_2', $this->pihak_berkonflik_2])
            ->andFilterWhere(['ilike', 'pihak_berkonflik_3', $this->pihak_berkonflik_3])
            ->andFilterWhere(['ilike', 'fungsi_kawasan_konflik', $this->fungsi_kawasan_konflik])
            ->andFilterWhere(['ilike', 'kode_pengaduan', $this->kode_pengaduan]);
        } else {
            $query
            ->orFilterWhere(['ilike', 'nama_pengadu', $this->keyword])
            ->orFilterWhere(['ilike', 'alamat_lengkap_pengadu', $this->keyword])
            ->orFilterWhere(['ilike', 'email_pengadu', $this->keyword])
            ->orFilterWhere(['ilike', 'pihak_berkonflik_1', $this->keyword])
            ->orFilterWhere(['ilike', 'pihak_berkonflik_2', $this->keyword])
            ->orFilterWhere(['ilike', 'pihak_berkonflik_3', $this->keyword])
            ->orFilterWhere(['ilike', 'fungsi_kawasan_konflik', $this->keyword])
            ->orFilterWhere(['ilike', 'kode_pengaduan', $this->keyword]);
        }
        
        

        return $dataProvider;
    }
}
