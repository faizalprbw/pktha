<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanAssesmen;

/**
 * PengaduanAssesmenSearch represents the model behind the search form about `\common\models\PengaduanAssesmen`.
 */
class PengaduanAssesmenSearch extends PengaduanAssesmen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan', 'id'], 'integer'],
            [['potensi_konflik_dan_potensi_damai', 'sejarah_konflik', 'akar_konflik', 'pemicu_konflik', 'akselerator_konflik', 'pemetaan_dinamika_aktor', 'sistem_representasi', 'tawaran_tertinggi', 'tawaran_terendah', 'rekomendasi', 'created_date', 'modified_date', 'surat_kesediaan_mediasi_filename', 'pesan_pengalihan_kasus'], 'safe'],
            [['finished', 'dialihkan'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengaduanAssesmen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengaduan' => $this->id_pengaduan,
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'finished' => false,
            'dialihkan' => $this->dialihkan,
        ]);

        $query->andFilterWhere(['like', 'potensi_konflik_dan_potensi_damai', $this->potensi_konflik_dan_potensi_damai])
            ->andFilterWhere(['like', 'sejarah_konflik', $this->sejarah_konflik])
            ->andFilterWhere(['like', 'akar_konflik', $this->akar_konflik])
            ->andFilterWhere(['like', 'pemicu_konflik', $this->pemicu_konflik])
            ->andFilterWhere(['like', 'akselerator_konflik', $this->akselerator_konflik])
            ->andFilterWhere(['like', 'pemetaan_dinamika_aktor', $this->pemetaan_dinamika_aktor])
            ->andFilterWhere(['like', 'sistem_representasi', $this->sistem_representasi])
            ->andFilterWhere(['like', 'tawaran_tertinggi', $this->tawaran_tertinggi])
            ->andFilterWhere(['like', 'tawaran_terendah', $this->tawaran_terendah])
            ->andFilterWhere(['like', 'rekomendasi', $this->rekomendasi])
            ->andFilterWhere(['like', 'surat_kesediaan_mediasi_filename', $this->surat_kesediaan_mediasi_filename])
            ->andFilterWhere(['like', 'pesan_pengalihan_kasus', $this->pesan_pengalihan_kasus]);

        return $dataProvider;
    }
}
