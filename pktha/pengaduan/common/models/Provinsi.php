<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%provinsi}}".
 *
 * @property string $id_wilayah
 * @property string $nama_provinsi
 * @property string $id
 * @property double $latitude
 * @property double $longitude
 * @property double $zoom
 */
class Provinsi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provinsi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_wilayah'], 'required'],
            [['id_wilayah'], 'integer'],
            [['nama_provinsi'], 'string'],
            [['latitude', 'longitude', 'zoom'], 'number'],
            [['id_wilayah'], 'exist', 'skipOnError' => true, 'targetClass' => Wilayah::className(), 'targetAttribute' => ['id_wilayah' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_wilayah' => Yii::t('app', 'Wilayah'),
            'nama_provinsi' => Yii::t('app', 'Nama Provinsi'),
            'id' => Yii::t('app', 'Id, AUTO INCREMENT'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'zoom' => Yii::t('app', 'Zoom'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProvinsiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProvinsiQuery(get_called_class());
    }
}
