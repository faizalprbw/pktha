<?php

namespace common\models;



use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "analisa".
 *
 * @property integer $id
 * @property string $hasil_analisa
 * @property string $pembahasan
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 */
class Piaps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'piaps';
    }

    /**
     * @inheritdoc
     */



    public function rules()
    {
        return [
            // [['hasil_analisa', 'pembahasan'], 'required'],
            [['proid', 'nama','kml','metadata','kabcenter','kabid','procenter'], 'string'],
            [['created_at', 'updated_at', 'id_admin','id_sumber','id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proid' => 'Provinsi ID',
            'nama' => 'Nama',
            'kml' => 'KML',
            'metadata' => 'Metadata',
            'id_sumber' => 'ID Sumber',
            'id_admin' => 'ID Admin',
            'kabcenter' => 'Kabcenter',
            'kabid' => 'Kab ID',
            'procenter' => 'Provinsi center',
        ];
    }
    
    // public function getProvinsi()
    // {
    //     return $this->hasOne(Provinsi::className(), ['id' => 'proid']);
    // }
    // public function getKa()
    // {
    //     return $this->hasOne(Provinsi::className(), ['id' => 'proid']);
    // }

    
}
