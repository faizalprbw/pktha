<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanPermohonan]].
 *
 * @see PengusulanPermohonan
 */
class PengusulanPermohonanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanPermohonan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanPermohonan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
