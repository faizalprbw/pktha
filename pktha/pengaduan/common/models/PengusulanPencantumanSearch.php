<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengusulanPencantuman;

/**
 * PengusulanPencantumanSearch represents the model behind the search form about `\common\models\PengusulanPencantuman`.
 */
class PengusulanPencantumanSearch extends PengusulanPencantuman
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pengusulan'], 'integer'],
            [['no_sk_penetapan', 'tanggal_sk_penetapan', 'perihal_sk_penetapan', 'no_sk_pencantuman', 'tanggal_sk_pencantuman', 'perihal_sk_pencantuman', 'form_verifikasi_filename'], 'safe'],
            [['luas_sk_penetapan', 'luas_sk_pencantuman'], 'number'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengusulanPencantuman::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tanggal_sk_penetapan' => $this->tanggal_sk_penetapan,
            'luas_sk_penetapan' => $this->luas_sk_penetapan,
            'tanggal_sk_pencantuman' => $this->tanggal_sk_pencantuman,
            'luas_sk_pencantuman' => $this->luas_sk_pencantuman,
            'id_pengusulan' => $this->id_pengusulan,
            'finished' => $this->finished,
        ]);
        
        $query->join('LEFT JOIN', '{{%pengusulan_hutan_adat_II}}', '{{%pengusulan_hutan_adat_II}}.id_pengusulan = {{%pengusulan_hutan_adat_IV}}.id_pengusulan');
        $query->join('LEFT JOIN', '{{%pengusulan_hutan_adat_III}}', '{{%pengusulan_hutan_adat_III}}.id_pengusulan = {{%pengusulan_hutan_adat_IV}}.id_pengusulan');
        $query->where(['{{%pengusulan_hutan_adat_II}}.finished' => true]);
        $query->where(['{{%pengusulan_hutan_adat_III}}.finished' => true]);

        $query->andFilterWhere(['like', 'no_sk_penetapan', $this->no_sk_penetapan])
            ->andFilterWhere(['like', 'perihal_sk_penetapan', $this->perihal_sk_penetapan])
            ->andFilterWhere(['like', 'no_sk_pencantuman', $this->no_sk_pencantuman])
            ->andFilterWhere(['like', 'perihal_sk_pencantuman', $this->perihal_sk_pencantuman])
            ->andFilterWhere(['like', 'form_verifikasi_filename', $this->form_verifikasi_filename]);

        return $dataProvider;
    }
}
