<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kabkota".
 *
 * @property string $kabid
 * @property string $proid
 * @property string $kabkota
 * @property integer $id_sumber
 *
 * @property Provinsi $pro
 */
class Hasildanrekomendasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hasildanrekomendasi';
    }

    /**
     * @inheritdoc
     */




// id
// hasil   
// hasil_rekomedasi
// tanggal 
// foto    
// pengaduan_id

    public function rules()
    {
        return [
            // [['id'], 'required'],
            [['id','pengaduan_id'], 'integer'],
            [['hasil', 'hasil_rekomedasi','tanggal'], 'string'],
            // [['kabkota'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hasil' => 'Hasil',
            'hasil_rekomedasi' => 'Hasil dan rekomedasi',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPro()
    {
        return $this->hasOne(Provinsi::className(), ['proid' => 'proid']);
    }
    public function getPiaps()
    {
        return $this->hasOne(Piaps::className(), ['kabid' => 'kabid']);
    }
}
