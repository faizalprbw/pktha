<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengusulanValidasi;

/**
 * PengusulanValidasiSearch represents the model behind the search form about `\common\models\PengusulanValidasi`.
 */
class PengusulanValidasiSearch extends PengusulanValidasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pengusulan'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengusulanValidasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(
            [
            'query' => $query,
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(
            [
            'id' => $this->id,
            'id_pengusulan' => $this->id_pengusulan,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'finished' => $this->finished,
            ]
        );

        return $dataProvider;
    }
}
