<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanDraftingMou;

/**
 * PengaduanDraftingMouSearch represents the model behind the search form about `common\models\PengaduanDraftingMou`.
 */
class PengaduanDraftingMouSearch extends PengaduanDraftingMou
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan', 'id'], 'integer'],
            [['berita_acara_filename', 'foto_filename', 'created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengaduanDraftingMou::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengaduan' => $this->id_pengaduan,
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            // 'finished' => $this->finished,
            'finished' => false,
        ]);
        
        $query->join('LEFT JOIN', 'pengaduan_assesmen', 'pengaduan_assesmen.id_pengaduan = pengaduan_drafting_mou.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_pra_mediasi', 'pengaduan_pra_mediasi.id_pengaduan = pengaduan_drafting_mou.id_pengaduan');
        $query->join('LEFT JOIN', 'pengaduan_mediasi', 'pengaduan_mediasi.id_pengaduan = pengaduan_drafting_mou.id_pengaduan');
        $query->where(['pengaduan_assesmen.finished' => true]);
        $query->where(['pengaduan_pra_mediasi.finished' => true]);
        $query->where(['pengaduan_mediasi.finished' => true]);

        $query->andFilterWhere(['like', 'berita_acara_filename', $this->berita_acara_filename])
                ->andFilterWhere(['like', 'foto_filename', $this->foto_filename]);

        return $dataProvider;
    }
}
