<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Kecamatan]].
 *
 * @see Kecamatan
 */
class KecamatanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Kecamatan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Kecamatan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getById($id) {
       return $this->select(['*'])->where(['id'=>$id])->one();
    }
    
    /**
     * 
     * @param type $id_kota_kabupaten
     * @return type
     */
    public function getByKotaKabupatenId($id_kota_kabupaten) {
        return $this->select(['*'])->where(['id_kota_kabupaten'=>$id_kota_kabupaten])->all();
    }
}
