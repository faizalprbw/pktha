<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pengaduan_tanda_tangan_mou".
 *
 * @property string $id_pengaduan
 * @property string $id
 * @property string $created_date
 * @property string $modified_date
 * @property boolean $finished
 * @property string $dokumen_mou_filename
 */
class PengaduanTandaTanganMou extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengaduan_tanda_tangan_mou';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengaduan'], 'required'],
            [['id_pengaduan'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
            [['dokumen_mou_filename'], 'file', 'extensions' => 'doc, docx, pdf, odt'],
            [['id_pengaduan'], 'exist', 'skipOnError' => true, 'targetClass' => PengaduanRegistrasi::className(), 'targetAttribute' => ['id_pengaduan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengaduan' => Yii::t('app', 'Id Pengaduan'),
            'id' => Yii::t('app', 'ID'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
            'finished' => Yii::t('app', 'Finished'),
            'dokumen_mou_filename' => Yii::t('app', 'Dokumen MoU'),
        ];
    }

    /**
     * @inheritdoc
     * @return PengaduanTandaTanganMouQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PengaduanTandaTanganMouQuery(get_called_class());
    }
}
