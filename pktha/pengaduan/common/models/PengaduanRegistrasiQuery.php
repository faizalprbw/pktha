<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengaduanRegistrasi]].
 *
 * @see PengaduanRegistrasi
 */
class PengaduanRegistrasiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengaduanRegistrasi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengaduanRegistrasi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * 
     * @param type $id id dari pengaduan yang terdaftar
     * @return type record yang tercatat. null jika tidak ditemukan.
     */
    public function getById($id) {
        return $this->select(['*'])->where(['id'=>$id])->one();
    }
}
