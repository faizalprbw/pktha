<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PengaduanPraMediasi;

/**
 * PengaduanPraMediasiSearch represents the model behind the search form about `common\models\PengaduanPraMediasi`.
 */
class PengaduanPraMediasiSearch extends PengaduanPraMediasi {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_pengaduan', 'id'], 'integer'],
            [['data_para_pihak', 'pokok_permasalahan', 'objek_mediasi', 'created_date', 'modified_date'], 'safe'],
            [['finished'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PengaduanPraMediasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengaduan' => $this->id_pengaduan,
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            // 'finished' => $this->finished,
            'finished' => false,
        ]);

        $query->join('LEFT JOIN', 'pengaduan_assesmen', 'pengaduan_assesmen.id_pengaduan = pengaduan_pra_mediasi.id_pengaduan');
        $query->where(['pengaduan_assesmen.finished' => true]);

        $query->andFilterWhere(['like', 'data_para_pihak', $this->data_para_pihak])
                ->andFilterWhere(['like', 'pokok_permasalahan', $this->pokok_permasalahan])
                ->andFilterWhere(['like', 'objek_mediasi', $this->objek_mediasi]);

        return $dataProvider;
    }

}
