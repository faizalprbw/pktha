<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PengusulanMonev]].
 *
 * @see PengusulanMonev
 */
class PengusulanMonevQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PengusulanMonev[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PengusulanMonev|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
