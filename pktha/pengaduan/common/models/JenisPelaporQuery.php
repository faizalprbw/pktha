<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[JenisPelapor]].
 *
 * @see JenisPelapor
 */
class JenisPelaporQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return JenisPelapor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return JenisPelapor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
