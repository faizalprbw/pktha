<?php

namespace common\viewmodels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PengaduanViewModel
 *
 * @property kode_pengaduan
 * @property nama_pengadu
 * @property tanggal_pengaduan
 * @property status_pengaduan
 * @property tanggal_status_pengaduan
 * 
 * @author wizard
 */
class PengaduanViewModel extends \yii\base\Model {
    //put your code here
    
    public $kode_pengaduan;
    public $nama_pengadu;
    public $tanggal_pengaduan;
    public $status_pengaduan;
    public $tanggal_status_pengaduan;
    
    public function rules() {
        return [
            [['kode_pengaduan', 'nama_pengadu', 'status_pengaduan'], 'string'],
            [['tanggal_pengaduan', 'tanggal_status_pengaduan'], 'safe']
        ];
    }
    
    public function __construct($_kode_pengaduan = "", $_nama_pengadu = "", $_tanggal_pengaduan = "", $_status_pengaduan = "", $_tanggal_status_pengaduan = "") {
        $this->kode_pengaduan = $_kode_pengaduan;
        $this->nama_pengadu = $_nama_pengadu;
        $this->tanggal_pengaduan = $_tanggal_pengaduan;
        $this->tanggal_status_pengaduan = $_tanggal_status_pengaduan;
        $this->status_pengaduan = $_status_pengaduan;
    }
    
}
    
