<?php

namespace common\viewmodels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PengusulanViewModel
 * @property kode_pengusulan
 * @property nama_pengusul
 * @property tanggal_pengusulan
 * @property status_pengusulan
 * @property tanggal_status_pengusulan
 * 
 * @author wizard
 */
class PengusulanViewModel extends \yii\base\Model {
    //put your code here
   
    public $kode_pengusulan;
    public $nama_pengusul;
    public $tanggal_pengusulan;
    public $status_pengusulan;
    public $tanggal_status_pengusulan;
    
    public function rules() {
        return [
            [['kode_pengusulan', 'nama_pengusul', 'status_pengusulan'], 'string',],
            [['tanggal_pengusulan', 'tanggal_status_pengusulan'], 'safe',]
        ];
    }
    
    public function __construct($_kode_pengusulan = "", $_nama_pengusul = "", $_tanggal_pengusulan= "", $_status_pengusulan = "", $_tanggal_status_pengusulan = "") {
        $this->kode_pengusulan = $_kode_pengusulan;
        $this->nama_pengusul = $_nama_pengusul;
        $this->tanggal_pengusulan = $_tanggal_pengusulan;
        $this->status_pengusulan = $_status_pengusulan;
        $this->tanggal_status_pengusulan = $_tanggal_status_pengusulan;
    }
    
}
