<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\viewmodels;

/**
 * Description of PengusulanCreatedInfoViewModel
 *
 * @author wizard
 */
class PengusulanCreatedInfoViewModel extends \yii\base\Model {
    //put your code here
    
    public $id_pengusulan;
    public $kode_pengusulan;
    public $kunci_pengusulan;
    
    public function rules() {
        return [
            [['id_pengusulan', 'kode_pengusulan', 'kunci_pengusulan'], 'string'],
            [['id_pengusulan', 'kode_pengusulan', 'kunci_pengusulan'], 'safe'],
        ];
    }
    
}
