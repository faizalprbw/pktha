<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

/**
 * Description of FileHelper
 *
 * @author wizard
 */
class FileHelper {
    //put your code here
    
    /**
     * 
     * @param type $fileHandler return value from UploadedFile::getInstance($model, 'field')
     */
    public static function GetFileContent(\yii\web\UploadedFile $fileHandler) {
        $fileSize = filesize($fileHandler->tempName);
        $fileOpen = fopen($fileHandler->tempName, 'r');
        $fileContent = fread($fileOpen, $fileSize);
        fclose($fileOpen);
        
        return fileContent;
    }
    
    
    public static function GetFileFromBinary($binary) {
        return null;
    }
}
