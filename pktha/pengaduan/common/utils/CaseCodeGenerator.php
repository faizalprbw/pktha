<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace common\utils;

/**
 * Description of CaseCodeGenerator
 *
 * @author wizard
 */
class CaseCodeGenerator {
//put your code here

    /**
     * Generate basic code for a pengaduan case.
     * @return string -[year][month]-[case number], i.e. -201709-0099
     */
    public static function GetBasicCode($role = null) {
        $year = \date('Y');
        $month = \date('m');
        $date = \date('d');

        $code = '';
        if (isset($role)) {
            if ($role >= 30) {
// Administrator and Administrator Subdit
// Administrator P-0
// Administrator P-X, X : Subdit X
                $code = $code . 'KT-P-' . ($role - 30) . '-';
            } else if ($role < 30 && $role >= 20) {
// Administrator Wilayah
                $code = $code . 'KT-D-' . ($role - 20) . '-';
            } else {
// Assume Text
                $code = $code . 'KT-T-0-';
            }

            $code = $code . $year . $month . $date . '-';
        }

        return $code;
    }

    /**
     * Generate kode konflik dari website. 
     * @return string Kode Konflik, i.e. KT-A-0-20170112-
     */
    public static function GetCode() {
        $year = \date('Y');
        $month = \date('m');
        $date = \date('d');
        return 'KT-A-0-' . $year . $month . $date . '-';
    }

    /**
     * 
     * @return string 
     */
    public static function GetHABasicCode($role = null) {
        $year = \date('Y');
        $month = \date('m');
        $date = \date('d');

        $code = '';
        if (isset($role)) {
            if ($role >= 30) {
// Administrator and Administrator Subdit
// Administrator P-0
// Administrator P-X, X : Subdit X
                $code .= 'HA-P-' . ($role - 30) . '-';
            } else if ($role < 30 && $role >= 20) {
// Administrator Wilayah
                $code .= 'HA-D-' . ($role - 20) . '-';
            } else {
// Assume Text
                $code .= 'HA-T-0-';
            }

            $code .= $year . $month . $date . '-';
        }

        return $code;
    }

    /**
     * 
     * @return string HA-A-[year][month][date]-, ie 'HA-A-0-20170917-'
     */
    public static function GetHACode() {
        $year = \date('Y');
        $month = \date('m');
        $date = \date('d');

        return 'HA-A-0-' . $year . $month . $date . '-';
    }

    /**
     * Generate a random string, using a cryptographically secure 
     * pseudorandom number generator (random_int)
     * 
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     * 
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

}
