<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

/**
 * Description of DateHelper
 *
 * @author wizard
 */
class DateHelper {
    //put your code here
    
    const DATE_FORMAT = 'Y-m-d G:i:s';
    
    /**
     * 
     */
    public static function GetCurrentDate() {
       $current_date = date(DateHelper::DATE_FORMAT);

       return \date($current_date);
    }
    
}
