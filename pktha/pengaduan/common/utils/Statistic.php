<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

/**
 * Description of Statistic
 *
 * @author wizard
 */
class Statistic
{
    public static $queryTahapan = array(
        'pengaduan' => array(
            'select' => 'pengaduan_registrasi',
            'from' => array(
                'pengaduan_registrasi',
                'pengaduan_assesmen',
                'pengaduan_pra_mediasi',
                'pengaduan_mediasi',
                'pengaduan_drafting_mou',
                'pengaduan_tanda_tangan_mou'
                ),
        ),
        'pengusulan' => array(
            'select' => 'pengusulan_hutan_adat',
            'from' => array(
                "pengusulan_hutan_adat",
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"
            ),
        ),
    );

    public static function getQueryTahapan($role, $tahapan)
    {
        $type = explode('_', $tahapan, 2)[0];
        $queryType = Statistic::$queryTahapan[$type];

        // Build WHERE portion of query
        $pos = array_search($tahapan, $queryType['from']);
        $where = '';
        for ($i = 1; $i < count($queryType['from']); $i++) {
            $table = $queryType['from'][$i];
            $where .= '(' . $queryType['select'] . '.id = "' . $table .
            '".id_' . $type . ') AND ';
            if ($i <= $pos) {
                // All tables before specified tahapan must be finished
                $where .= '("' . $table . '".finished = true)';
            } else {
                // All tables after specified tahapan must be unfinished or undefined
                $where .= '("' . $table . '".finished = false OR "' . $table .
                '".finished ISNULL )';
            }
            if ($i < count($queryType['from']) - 1) {
                $where .= ' AND ';
            }
        }

        // Consider role
        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                if ($type == 'pengaduan') {
                    $where .= ' AND (' . $queryType['select'] .
                        '.id_wilayah_konflik = ' . $id_wilayah . ')';
                } else {
                    $where .= ' AND (' . $queryType['select'] .
                        '.id_wilayah = ' . $id_wilayah . ')';
                }
            }
        }

        // Build full SQL query
        $sql = 'SELECT ' . $queryType['select'] . '.kode_' . $type . ' FROM "' .
            implode('","', $queryType['from']) . '" WHERE ' . $where;

        return $sql;
    }

    public static function getAllQueryTahapan($role)
    {
        $tables = array_merge(
            Statistic::$queryTahapan['pengaduan']['from'],
            Statistic::$queryTahapan['pengusulan']['from']
        );
        $queries = array();
        foreach ($tables as $table) {
            $sql = Statistic::getQueryTahapan($role, $table);
            $res = \Yii::$app->db->createCommand($sql)->queryAll();
            $count = count($res);
            $queries[$table] = array($res, $count);
        }
        return $queries;
    }

    /**
     * Return number of pengaduan which has completed Assesment for public consumption.
     */
    public static function getCompletedPengaduanAssesmentCount($role)
    {
        $sql = '
        SELECT
        pengaduan_registrasi.kode_pengaduan

        FROM
        pengaduan_registrasi,
        pengaduan_assesmen,
        pengaduan_pra_mediasi,
        pengaduan_mediasi,
        pengaduan_drafting_mou,
        pengaduan_tanda_tangan_mou

        WHERE
        (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
        AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
        AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
        AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
        AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
        AND (pengaduan_assesmen.finished = true)
        AND (pengaduan_pra_mediasi.finished = false OR pengaduan_pra_mediasi.finished IS NULL)
        AND (pengaduan_mediasi.finished = false OR pengaduan_mediasi.finished IS NULL)
        AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished IS NULL)
        AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getCompletedPengaduanPraMediasiCount($role)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = false OR pengaduan_mediasi.finished IS NULL)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished IS NULL)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getCompletedPengaduanMediasiCount($role)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished IS NULL)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getCompletedPengaduanDraftingMoUCount($role)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = true)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getCompletedPengaduanTandaTanganMoUCount($role)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = true)
                AND (pengaduan_tanda_tangan_mou.finished = true)';


        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    //put your code here
    public static function getPengaduanAssesmenCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = false OR pengaduan_assesmen.finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanPraMediasiCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true AND pengaduan_assesmen.kasus_dialihkan = false)
                AND (pengaduan_pra_mediasi.finished = false OR pengaduan_pra_mediasi.finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanMediasiCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = false OR pengaduan_mediasi.finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanDraftingMouCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanTandaTanganMouCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true)
                AND (pengaduan_mediasi.finished = true)
                AND (pengaduan_drafting_mou.finished = true)
                AND (pengaduan_tanda_tangan_mou.finished = false OR pengaduan_tanda_tangan_mou.finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanCompleted($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true)
                AND (pengaduan_pra_mediasi.finished = true OR pengaduan_assesmen.kasus_dialihkan = true)
                AND (pengaduan_mediasi.finished = true OR pengaduan_assesmen.kasus_dialihkan = true)
                AND (pengaduan_drafting_mou.finished = true OR pengaduan_assesmen.kasus_dialihkan = true)
                AND (pengaduan_tanda_tangan_mou.finished = true OR pengaduan_assesmen.kasus_dialihkan = true)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanDialihkan($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = true AND pengaduan_assesmen.kasus_dialihkan = true)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengaduanTerdaftarCount($role = null)
    {
        $sql = '
            SELECT
                pengaduan_registrasi.kode_pengaduan

            FROM
                pengaduan_registrasi,
                pengaduan_assesmen,
                pengaduan_pra_mediasi,
                pengaduan_mediasi,
                pengaduan_drafting_mou,
                pengaduan_tanda_tangan_mou

            WHERE
                (pengaduan_registrasi.id = pengaduan_assesmen.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_pra_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_mediasi.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_drafting_mou.id_pengaduan)
                AND (pengaduan_registrasi.id = pengaduan_tanda_tangan_mou.id_pengaduan)
                AND (pengaduan_assesmen.finished = false OR pengaduan_assesmen.finished ISNULL)
                AND (pengaduan_pra_mediasi.finished = false OR pengaduan_pra_mediasi.finished ISNULL)
                AND (pengaduan_mediasi.finished = false OR pengaduan_mediasi.finished ISNULL)
                AND (pengaduan_drafting_mou.finished = false OR pengaduan_drafting_mou.finished ISNULL)
                AND (pengaduan_tanda_tangan_mou.finished = true OR pengaduan_tanda_tangan_mou.finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    /**
     *
     * @return \common\models\StatistikPengaduan $model
     */
    public static function getPengaduan()
    {
        $statPengaduan = \common\models\StatistikPengaduan::findOne(['created_date' => DateHelper::getCurrentDate()]);
        if (!isset($statPengaduan)) {
            return $statPengaduan;
        } else {
            $model = new \common\models\StatistikPengaduan();
            $model->assesmen = Statistic::getPengaduanAssesmenCount();
            $model->pra_mediasi = Statistic::getPengaduanPraMediasiCount();
            $model->mediasi = Statistic::getPengaduanMediasiCount();
            $model->drafting_mou = Statistic::getPengaduanDraftingMouCount();
            $model->dialihkan = Statistic::getPengaduanDialihkan();
            $model->created_date = DateHelper::getCurrentDate();

            if ($model->save()) {
                return $model->id;
            } else {
                return 0;
            }
        }
    }

    public static function getCountPengaduan($role = null)
    {
        $sql = 'SELECT COUNT (*) FROM pengaduan_registrasi ';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' WHERE pengaduan_registrasi.id_wilayah_konflik = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        return $result[0]['count'];
    }

    public static function getPengusulanValidasi($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = false OR "pengusulan_hutan_adat_II".finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanCompletedValidasi($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = false OR "pengusulan_hutan_adat_III".finished IS NULL)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished IS NULL)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanVerifikasi($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat.id

            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = false OR "pengusulan_hutan_adat_III".finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanCompletedVerifikasi($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished IS NULL)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanPenetapanPencantuman($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat.id

            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanCompletedPenetapanPencantuman($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = true)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanMonevPemberdayaan($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat.id

            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = true)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished IS NULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanCompletedMonevPemberdayaan($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = true)
                AND ("pengusulan_hutan_adat_III".finished = true)
                AND ("pengusulan_hutan_adat_IV".finished = true)
                AND ("pengusulan_hutan_adat_V".finished = true)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    public static function getPengusulanTerdaftarCount($role = null)
    {
        $sql = '
            SELECT
                pengusulan_hutan_adat."kode_pengusulan"
            FROM
                pengusulan_hutan_adat,
                "pengusulan_hutan_adat_II",
                "pengusulan_hutan_adat_III",
                "pengusulan_hutan_adat_IV",
                "pengusulan_hutan_adat_V"

            WHERE
                (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_II"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_III"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_IV"."id_pengusulan")
                AND (pengusulan_hutan_adat.id = "pengusulan_hutan_adat_V"."id_pengusulan")
                AND ("pengusulan_hutan_adat_II".finished = false OR "pengusulan_hutan_adat_II".finished ISNULL)
                AND ("pengusulan_hutan_adat_III".finished = false OR "pengusulan_hutan_adat_III".finished ISNULL)
                AND ("pengusulan_hutan_adat_IV".finished = false OR "pengusulan_hutan_adat_IV".finished ISNULL)
                AND ("pengusulan_hutan_adat_V".finished = false OR "pengusulan_hutan_adat_V".finished ISNULL)';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' AND pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        $count = count($result);

        return $count;
    }

    /**
     *
     * @return \common\models\StatistikPengusulan $model
     */
    public static function getPengusulan()
    {
        $statPengusulan = \common\models\StatistikPengusulan::findOne(['created_date' => DateHelper::getCurrentDate()]);
        if (isset($statPengusulan)) {
            return $statePengusulan;
        } else {

            $countValidasi = Statistic::getPengusulanValidasi();
            $countVerifikasi = Statistic::getPengusulanVerifikasi();
            $countPenetapanPencantuman = Statistic::getPengusulanPenetapanPencantuman();
            $countMonev = Statistic::getPengusulanMonevPemberdayaan();

            $model = new \common\models\StatistikPengusulan();
            $model->validasi = $countValidasi;
            $model->verifikasi = $countVerifikasi;
            $model->penetapan_pencantuman = $countPenetapanPencantuman;
            $model->monev_pemberdayaan = $countMonev;
            $model->created_date = DateHelper::getCurrentDate();

            if ($model->save()) {
                return $model->id;
            } else {
                return 0;
            }
        }
    }

    public static function getCountPengusulan($role = null)
    {
        $sql = 'SELECT COUNT(*) FROM pengusulan_hutan_adat ';

        if (isset($role)) {
            if ($role >= 30) {
                // Administrator Pusat
            } else if ($role <= 29 && $role >= 20) {
                // Administrator Wilayah
                $id_wilayah = $role - 20;
                $sql .= (' WHERE pengusulan_hutan_adat.id_wilayah = ' . $id_wilayah);
            }
        }

        $result = \Yii::$app->db->createCommand($sql)->queryAll();

        return $result[0]['count'];
    }

}

