<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\utils;

use common\models\PengusulanHutanAdat;

/**
 * Description of PengusulanHutanAdat
 *
 * @author wizard
 */
class PengusulanHutanAdatUtils {

    //put your code here

    public static function GetUploadPath(PengusulanHutanAdat $model) {
        return \Yii::getAlias('@updir') . '/' . $model->tableName() . '/' . $model->id . '-' . $model->kode_pengusulan;
    }
}
