<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\widgets\Alert;

$this->title = 'USULAN HUTAN ADAT';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact site-page">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="alert alert-success" role="alert">
            <p>Tata Cara Permohonan Hutan Adat:
            <div style="margin-left:10px;">
                <ol>
                    <li>Mengunduh formulir lengkap <a href="../../uploads/permohonan-hutan-hak.docx" target="_blank">PERMOHONAN HUTAN ADAT</a>, sesuai dengan lampiran P.1/PSKL/Set/KUM.1/2/2016</li>
                    <li>Mengisi formulir tersebut diatas</li>
                    <li>Menyampaikan formulir tersebut ke Direktotat PKTHA, dan bisa melalui media upload di bawah ini dalam format <strong>PDF</strong></li>
                </ol>
            </div>
            </p>
        </div>   
        <div class="row">
            <div class="col-lg-5">
                <br/>
                <?= Alert::widget() ?>
                <br/>
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>  
                <?= $form->errorSummary($model); ?>
                <br/>
                <?= $form->field($model, 'name')->label('Nama'); ?>
                <?= $form->field($model, 'email')->label('Alamat Email'); ?>
                <?= $form->field($model, 'tlp')->label('Telepone'); ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6])->label('Isi'); ?>                
                <?= $form->field($model, 'upload')->fileInput() ?>
                <?=
                $form->field($model, 'verifyCode')->label('Kode Verifikasi')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ])
                ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
