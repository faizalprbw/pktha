<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengaduanRegistrasi */
/* @var $form ActiveForm */
?>

<?php
Yii::$app->view->registerCSSFile('css/map/leaflet.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.ZoomBar.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.Locate.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet.groupedlayercontrol.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.BetterScale.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/iconLayers.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.Default.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet_geosearch.css', ['position' => yii\web\View::POS_HEAD]);

Yii::$app->view->registerJsFile('js/map/leaflet-src.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.ZoomBar.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.Locate.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-color-markers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.groupedlayercontrol.min.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.BetterScale.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/iconLayers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-providers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.markercluster.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/jquery.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/feature_group.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet_geosearch2.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-fill-Pattern.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/panggil_peta_form.js', ['position' => yii\web\View::POS_END]);
Yii::$app->view->registerJs('var link_lokasi_pengaduan = ' .json_encode(yii\helpers\Url::to(['/site/lokasipengaduan'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_pencantuman_HA = ' .json_encode(yii\helpers\Url::to(['/site/pencantumanhutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_potensi_HA = ' .json_encode(yii\helpers\Url::to(['/site/potensihutanadat5'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2016 = ' .json_encode(yii\helpers\Url::to(['/site/kasus2016'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2017 = ' .json_encode(yii\helpers\Url::to(['/site/kasus2017'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_provinsi = ' .json_encode(yii\helpers\Url::to(['/site/produkhukumprovinsi'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_kabupaten = ' .json_encode(yii\helpers\Url::to(['/site/produkhukumkabupaten'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_pencantuman = ' .$jumlah_pencantuman . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_potensi = ' .$jumlah_potensi . ';', yii\web\View::POS_HEAD);
?>

<!-- multiple form js -->
<script type="text/javascript">
    $(document).ready(function () {
        var s_1 = true;
        var s_2, s_3 = false;
        showForm();
        function multiForm(a_form, x_form, a_step, x_step) {
            $(a_form).show();
            $(x_form).hide();
            $(a_step).css({
                "background-color": "#43984A",
            });
            $(x_step).css({
                "background-color": "#a2e5a8",
            });
        }
        ;
        function showForm() {
            if (s_1 == true) {
                multiForm(".form_1", ".form_2, .form_3", ".step_1", ".step_2, .step_3")
            } else if (s_2 == true) {
                multiForm(".form_2", ".form_1, .form_3", ".step_2", ".step_1, .step_3")
            } else {
                multiForm(".form_3", ".form_1, .form_2", ".step_3", ".step_2, .step_1")
            }
            ;
        }
        ;

        $(".step_1, .prev_2").click(function () {
            s_1 = true;
            s_2 = false;
            s_3 = false;
            showForm();
        });
        $(".step_2, .next_1, .prev_3").click(function () {
            s_1 = false;
            s_2 = true;
            s_3 = false;
            showForm();
        });
        $(".step_3, .next_2").click(function () {
            s_1 = false;
            s_2 = false;
            s_3 = true;
            showForm();
        });

        $(".map_marking").hide();
        $(".show_map").click(function () {
            $(".map_marking").toggle("slow");
        })

    });

    function ajaxprovinsi(id) {
        ajaxku = buatajax();
        var url = "http://localhost/pktha/pengaduan/frontend/views/site/getprovinsi_pg.php?id=" + id;
        ajaxku.onreadystatechange = wilayahChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function ajaxkota(id) {
        ajaxku = buatajax();
        var url = "http://localhost/pktha/pengaduan/frontend/views/site/getkota_pg.php?id=" + id;
        ajaxku.onreadystatechange = stateChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function ajaxkecamatan(id) {
        ajaxku = buatajax();
        var url = "http://localhost/pktha/pengaduan/frontend/views/site/getkecamatan_pg.php?id=" + id;
        ajaxku.onreadystatechange = districtChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function ajaxkelurahan(id) {
        ajaxku = buatajax();
        var url = "http://localhost/pktha/pengaduan/frontend/views/site/getkelurahan_pg.php?id=" + id;
        ajaxku.onreadystatechange = villageChanged;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);
    }

    function buatajax() {
        if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        }

        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        }

        return null;
    }

    function wilayahChanged() {
        var data;
        document.getElementById("kabkota").value = "<option selected>-- Silahkan Pilih Kabupaten / Kota --</option>";
        document.getElementById("kecamatan").value = "<option selected>-- Silahkan Pilih Kecamatan --</option>";
        document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan / Desa --</option>";
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("provinsi").innerHTML = data
            } else {
                document.getElementById("provinsi").value = "<option selected>-- Silahkan Pilih Provinsi --</option>";
            }
        }
    }

    function stateChanged() {
        var data;
        document.getElementById("kecamatan").value = "<option selected>-- Silahkan Pilih Kecamatan --</option>";
        document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan / Desa --</option>";
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("kabkota").innerHTML = data
            } else {
                document.getElementById("kabkota").value = "<option selected>-- Silahkan Pilih Kabupaten / Kota --</option>";
            }
        }
    }

    function districtChanged() {
        var data;
        document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan / Desa --</option>";
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("kecamatan").innerHTML = data
            } else {
                document.getElementById("kecamatan").value = "<option selected>-- Silahkan Pilih Kecamatan --</option>";
            }
        }
    }

    function villageChanged() {
        var data;
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("kelurahan").innerHTML = data
            } else {
                document.getElementById("kelurahan").value = "<option selected>-- Silahkan Pilih Kelurahan / Desa --</option>";
            }
        }
    }

    function onWilayahChange() {
        alert('Hello');
    }

</script>

<div class="title_wrapper2">
    <div class="ikon2 ikon_6" style="width:100px;height:145px;">
    </div>
    <h2 style="top:50px;">Form Pengaduan Konflik Tenurial</h2>
</div>
<div class="clear">

</div>
<div class="">
    <div class="FormPengaduanKonflik">
        <div class="step_cnt">
            <button class="form_step col-md-4 step_1">
                DATA PELAPOR
            </button>
            <button class="form_step col-md-4 step_2">
                KONFLIK
            </button>
            <button class="form_step col-md-4 step_3">
                LOKASI KONFLIK
            </button>
            <div class="clear"></div>
        </div>
        <!-- content form -->
        <?php $form = ActiveForm::begin(); ?>
        <div class="form_cnt form_1 box_wrapper">
            <div class="form_head ">
                DATA PELAPOR
            </div>
            <div class="form_input">
                <?php echo $form->field($model, 'nama_pengadu') ?>
                <?php echo $form->field($model, 'alamat_lengkap_pengadu') ?>
                <?php echo $form->field($model, 'email_pengadu') ?>
                <?php echo $form->field($model, 'no_telepon_pengadu')->textInput(['type' => 'text'])->label('No Telepon (wajib') ?>
                <?php echo $form->field($model, 'id_jenis_pelapor')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\JenisPelapor::find()->all(), 'id', 'jenis_pelapor'))->label('Jenis Pelapor') ?>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-success next_1 pull-right">Selanjutnya</button>
                <div class="clear" style="border-color:white;"></div>
            </div>
        </div>
        <div class="form_cnt form_2 box_wrapper">
            <div class="form_head">
                KONFLIK
            </div>
            <div class="form_input">
                <?php echo $form->field($model, 'pihak_berkonflik_1') ?>
                <?php echo $form->field($model, 'pihak_berkonflik_2') ?>
                <?php echo $form->field($model, 'pihak_berkonflik_3') ?>
                <?php echo $form->field($model, 'fungsi_kawasan_konflik') ?>
                <?php echo $form->field($model, 'id_tipe_konflik')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\TipeKonflik::find()->all(), 'id', 'nama_konflik'))->label('Jenis Konflik') ?>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-success prev_2 pull-left">Sebelumnya</button>
                <button type="button" class="btn btn-success next_2 pull-right">Selanjutnya</button>
                <div class="clear" style="border-color:white;"></div>
            </div>
        </div>
        <div class="form_cnt form_3 box_wrapper">
            <div class="form_head">
                LOKASI KONFLIK
            </div>
            <div class="form_input">
                <?php echo
                $form->field($model, 'id_wilayah_konflik', ['options' => ['id' => 'select-wilayah-konflik']])->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
                    'prompt' => 'Pilih Wilayah Konflik',
                    'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_provinsi_konflik").html(data); })',
                        ]
                )->label('Wilayah Konflik')
                ?>

                <div class="row">
                    <div class="col-md-5">
                        <?php echo
                        $form->field($model, 'id_provinsi_konflik', ['options' => ['id' => 'select-provinsi-konflik']])->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->where(['=', 'id', 1])->all(), 'id', 'nama_provinsi'), [
                            'prompt' => 'Pilih Provinsi Konflik',
                            'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kota_kabupaten_konflik").html(data) })'
                                ]
                        )->label('Provinsi Konflik')
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?php echo $form->field($model, 'teks_provinsi_konflik', ['options' => ['id' => 'teks-provinsi-konflik']])->textInput()->label('Lainnya') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <?php echo
                        $form->field($model, 'id_kota_kabupaten_konflik', ['options' => ['id' => 'select-kota-kabupaten-konflik']])->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['=', 'id', 1])->all(), 'id', 'nama_kota_kabupaten'), [
                            'prompt' => 'Pilih Kota / Kabupaten Konflik',
                            'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_kecamatan_konflik").html(data) })'
                                ]
                        )->label('Kota / Kabupaten Konflik')
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?php echo $form->field($model, 'teks_kota_kabupaten_konflik', ['options' => ['id' => 'teks-kota-kabupaten-konflik']])->textInput()->label('Lainnya') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <?php echo
                        $form->field($model, 'id_kecamatan_konflik', ['options' => ['id' => 'select-kecamatan-konflik']])->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['=', 'id', 1])->all(), 'id', 'nama_kecamatan'), [
                            'prompt' => 'Pilih Kecamatan',
                            'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengaduanregistrasi-id_desa_kelurahan_konflik").html(data) })'
                                ]
                        )->label('Kecamatan')
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?php echo $form->field($model, 'teks_kecamatan_konflik', ['options' => ['id' => 'teks-kecamatan-konflik']])->textInput()->label('Lainnya') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <?php echo
                        $form->field($model, 'id_desa_kelurahan_konflik')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['=', 'id', 1])->all(), 'id', 'nama_desa_kelurahan'), [
                            'prompt' => 'Pilih Desa / Kelurahan Konflik',
                                ]
                        )->label('Desa / Kelurahan')
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?php echo $form->field($model, 'teks_desa_kelurahan_konflik', ['options' => ['id' => 'teks-desa-kelurahan-konflik']])->textInput()->label('Lainnya') ?>
                    </div>
                </div>

                <?php echo $form->field($model, 'latitude_daerah_konflik')->textInput(['id' => 'latitude_daerah_konflik']); ?>
                <?php echo $form->field($model, 'longitude_daerah_konflik')->textInput(['id' => 'longitude_daerah_konflik']); ?>
                <?php echo $form->field($model, 'luas_kawasan_konflik') ?>
                <button type="button" class="btn btn-default show_map pull-left">Tampilkan Peta</button>
                <div class="clear" style="border-color:white;"></div>
                <div class="map_marking">
                    <p style="color:grey;">Klik pada peta atau Geser marker untuk menentukan koordinat</p>
                    <div class="map_wrapper">
                        <div id="map" style="width:100%;height:500px;background:white"></div>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 20px;">
                    <button type="button" class="btn btn-success prev_3 pull-left">Sebelumnya</button>
                    <?php echo Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success pull-right']) ?>
                    <div class="clear" style="border-color:white;"></div>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div><!-- FormPengaduanKonflik -->

</div>
