<head>
    <script src="js/map/jquery.js"></script>
</head>

<div class="site-page">
    <div class="row">
        <div class="title_wrapper2">
            <div class="ikon2 ikon_8" style="width: 100px; margin-right: 10px;"></div>
            <?php if ($model && $model->judul) : ?>
                <h2><?php echo $model->judul ?></h2>
            <?php else : ?>
                <h2>Struktur Organisasi</h2>
            <?php endif ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->image) : ?>
                <img src="<?php echo Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="100%" class="img-responsive" />
            <?php endif ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if ($model && $model->konten) : ?>
                <?php echo $model->konten ?>
            <?php else : ?>
                <div class="under_construction cl_4">
                    <div class="ikon_uc"></div>
                    THIS PAGE IS UNDER CONSTRUCTION
                </div>
            <?php endif ?>
        </div>
    </div>
    <div class="content2 cl_4" style="padding: 20px 20px 50px;">
        <figure class="org-chart cf">
            <ul class="administration">
                <li>
                    <ul class="director">
                        <li>
                            <a class="pointer2">
                                <p>DIREKTUR PENANGANAN KONFLIK, TENURIAL, DAN HUTAN ADAT</p>
                                <!-- <p>Dr. Ir. Eka W. Soegiri, MM. </p> -->
                            </a>
                            <ul class="departments cf">
                                <li></li>

                                <li class="department dep-a">
                                    <a class="pointer2 sub_1">
                                        <p>Kepala Sub Direktorat Pemetaan Konflik</p>
                                        <!-- <p>Ratnasari, SH, M.Si</p> -->
                                    </a>
                                    <ul class="sections">
                                        <li class="section"><a class="pointer2 sub_11">
                                                <p>Kepala Seksi Tipologi Jenis dan Sumber Konflik</p>
                                                <!--
                                                <p>Syafda Roswandi, S.Hut, M.Si</p>
                                                <div class="anggota">
                                                    <p>Tirza Carol Gracia Tompodung, S.Kom, MSc </p>
                                                    <p>Muamar, SH</p>
                                                </div>
                                                -->
                                            </a>
                                        </li>
                                        <li class="section"><a class="pointer2 sub_12">
                                                <p>Kepala Seksi Perumusan Metodologi</p>
                                                <!--
                                                <p>Wahyu Trimurti ,S.Hut, M.Sc</p>
                                                <div class="anggota">
                                                    <p>Fitri Permana Sari, S.Ant</p>
                                                    <p>Thezar Isbandi, SE., MM</p>
                                                    <p>Kusmana, SH</p>
                                                </div>
                                                -->
                                            </a></li>
                                    </ul>
                                </li>
                                <li class="department dep-b">
                                    <a class="pointer2 sub_2">
                                        <p>Kepala Sub Direktorat Penanganan Konflik</p>
                                        <!-- <p>Sugasri, SH</p> -->
                                    </a>
                                    <ul class="sections">
                                        <li class="section"><a class="pointer2 sub_21">
                                                <p>Kepala Seksi Negosiasi dan Mediasi Konflik</p>
                                                <!--
                                                <p>Daru Adianto, SH, MT.</p>
                                                <div class="anggota">
                                                    <p>Sharon Quamila Korompot, SH</p>
                                                    <p>Yoga Gulvi Pratama</p>
                                                </div>
                                                -->
                                            </a></li>
                                        <li class="section"><a class="pointer2 sub_22">
                                                <!-- <span>Kepala Seksi Advokasi Pendampingan Masyarakat</span> -->
                                                <p>Kepala Seksi Advokasi Pendampingan Masyarakat</p>
                                                <!--
                                                <p>Isep Ridwan Somantri, SAP</p>
                                                <div class="anggota">
                                                    <p>Sidik Prayitno, SE </p>
                                                    <p>Fauzie, SH </p>
                                                    <p>Ardani Hasan, SH</p>
                                                </div>
                                                -->
                                            </a></li>
                                    </ul>
                                </li>
                                <li class="department dep-c">
                                    <a class="pointer2 sub_3">
                                        <p>Kepala Sub Direktorat Penanganan Tenurial</p>
                                        <!-- <p>Drs. Saronto Prayogo Oetomo, M.Si</p> -->
                                    </a>
                                    <ul class="sections">
                                        <li class="section"><a class="pointer2 sub_31">
                                                <p>Kepala Seksi Tenurial Horizontal</p>
                                                <!--
                                                <p>Ir. Kendariany Lethe</p>
                                                <div class="anggota">
                                                    <p>Dina Arifa, S.Hut </p>
                                                    <p>Aniza Fitriani, SH </p>
                                                    <p>Aden Andriansyah, SE</p>
                                                </div>
                                                -->
                                            </a></li>
                                        <li class="section"><a class="pointer2 sub_32">
                                                <p>Kepala Seksi Tenurial Struktural</p>
                                                <!--
                                                <p>Eko Nopriadi, S.Hut, MT</p>
                                                <div class="anggota">
                                                    <p>Finauly Hadiyantini, S.Sos </p>
                                                    <p>Andina Dyah Ratnasari, S.Kom</p>
                                                    <p>Aden Andriansyah, SE</p>
                                                </div>
                                                -->
                                            </a></li>
                                    </ul>
                                </li>
                                <li class="department dep-d">
                                    <a class="pointer2 sub_4">
                                        <span>Kepala Sub Direktorat Pengakuan Hutan Adat dan Perlindungan Kearifan Lokal</span>
                                        <!-- <p>Yuli Prasetyo Nugroho, S.Sos, M.Si</p> -->
                                    </a>
                                    <ul class="sections">
                                        <li class="section">
                                            <a class="pointer2 sub_41">
                                                <p>Kepala Seksi Pencadangan Hutan Adat dan Perlindungan Kearifan Lokal</p>
                                                <!--
                                                <p>Ir. April Harini</p>
                                                <div class="anggota">
                                                    <p>Nelson Perdy Siahaan, S.Sos </p>
                                                    <p>Nisa Ni'mah Utami</p>
                                                </div>
                                                -->
                                            </a>
                                        </li>
                                        <li class="section"><a class="pointer2 sub_42">
                                                <p>Kepala Seksi Pengukuhan Hutan Adat dan Perlindungan Pengetahuan Tradisional</p>
                                                <!--
                                                <p>Agung Pambudi , S.Sos</p>
                                                <div class="anggota">
                                                    <p>Rina Nurhaeni, A.Md</p>
                                                    <p>Adi Saputro, S.Sos</p>
                                                </div>
                                                -->
                                            </a></li>
                                    </ul>
                                </li>
                                <li class="department dep-e">
                                    <a class="pointer2 sub_5">
                                        <p>Kepala Sub Bagian Tata Usaha</p>
                                        <!--
                                        <p>Sri Rejeki, SE</p>
                                        <div class="anggota">
                                            <p>Erika Taruli Sinaga, Amd </p>
                                            <p>Sumarna, S.Pd</p>
                                            <p>Dian Nurlia, A.Md</p>
                                            <p>Rahmat</p>
                                            <p>Chaya Mega</p>
                                            <p>Hamid</p>
                                        </div>
                                        -->
                                    </a>
                                    <ul class="sections">
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </figure>
        <div class="clear"> </div>
    </div>
    <div class="clear">

    </div>
</div>
