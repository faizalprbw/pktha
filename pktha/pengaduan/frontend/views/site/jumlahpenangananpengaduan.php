<div class="site-page">
    <div class="row">
        <div class="title_wrapper201">
          <div class="ikon2 ikon_1" style="height:120px">
          </div>
          <h2>Jumlah Penanganan Pengaduan</h2>
        </div>
        <div id="statpengaduan" style="margin-bottom: 0;">
            <div class="statpeng">
                <div class="row statwrap cl_4">
                    <?php
                        $boxNumber = array(
                            'grey' => 'one',
                            'red' => 'two',
                            'yellow' => 'three',
                            'orange' => 'four',
                            'green' => 'five',
                            'blue' => 'six',
                            'purple' => 'seven');
                    ?>
                    <?php $keys = array_keys($tahapPengaduan);
                    foreach (array_values($tahapPengaduan) as $i => $val) {
                        if ($keys[$i] != 'Registrasi' && $keys[$i] != 'Dialihkan') {
                            echo '
                        <div class="col-sm-1 col-md-1 boxed">
                            <div class="boxnumber bg-'.$val['warna'].'">
                                <div class="icon">'.$val['count'].'</div>
                                <div id="triangle-down"></div>
                                <div id="circle"></div>
                            </div>
                            <div class="boxtext">
                                <h5><a>Tahap '.($i - 1).'</a> <br/>'.$keys[$i].'<br/></h5>
                            </div>
                        </div>';
                        }
                    } ?>
                    <div class="col-sm-1 col-md-1 boxed">
                        <div class="boxnumber one">
                            <div class="icon"><?php echo $countDialihkan ?></div>
                            <div id="triangle-down"></div>
                            <div id="circle"></div>
                        </div>
                        <div class="boxtext">
                            <h5><a>Dialihkan</a> <br/><br/></h5>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div id="statgaris-pengaduan-konflik"></div>
                </div>
                <div class="row statwrap2">
                    <div id="totpeng" class="col-sm-12 col-md-12" >
                    <h3 >TOTAL PENGADUAN : <b><?php echo $tahapPengaduan['Registrasi']['count'] ?></b> </h3>
                    </div>
                </div><br/>
            </div>
            <div class="clear">
            </div>
        </div>
        <div id="statpengaduan" style="margin-top: 0;">
            <div class="statpeng">
                <div class="row statwrap_1 cl_4">
                    <?php $keys = array_keys($tahapPengusulan);
                    foreach (array_values($tahapPengusulan) as $i => $val) {
                        if ($keys[$i] != 'Registrasi' && $keys[$i] != 'Dikembalikan') {
                            echo '
                        <div class="col-sm-2 col-md-2 boxed200">
                            <div class="boxnumber '.$boxNumber[$val['warna']].'">
                                <div class="icon">'.$val['count'].'</div>
                                <div id="triangle-down200"></div>
                                <div id="circle200"></div>
                            </div>
                            <div class="boxtext">
                                <h5><a>Tahap '.($i - 1).'</a> <br/>'.$keys[$i].'<br/></h5>
                            </div>
                        </div>';
                        }
                    } ?>
                    <div class="clear"></div>
                    <div id="statgaris-pengaduan-konflik"></div>
                </div>
                <div class="row statwrap2_1">
                    <div id="totpeng" class="col-sm-12 col-md-12" >
                    <h3 >TOTAL USULAN : <b><?php echo $tahapPengusulan['Registrasi']['count'] ?></b> </h3>
                    </div>
                </div><br/>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
