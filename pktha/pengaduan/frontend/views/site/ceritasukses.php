<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Cerita sukses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-ceritasukses site-page">
    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_view', ['model' => $model]);
            },
            'layout' => '{items}<div class="pager-wrapper">{pager}</div>'
        ]);
        ?>
        
    </div>

</div>
