<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="utf-8">

<link rel="stylesheet" href="css/map/leaflet.css" />
<link rel="stylesheet" href="css/map/leaflet_geosearch.css"  />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/map/L.Control.ZoomBar.css" />
<link rel="stylesheet" href="css/map/L.Control.Locate.css" />
<link rel="stylesheet" href="css/map/leaflet.groupedlayercontrol.min.css" />
<link rel="stylesheet" href="css/map/L.Control.BetterScale.css" />
<link rel="stylesheet" href="css/map/iconLayers.css" />
<link rel="stylesheet" href="css/map/MarkerCluster.css" />
<link rel="stylesheet" href="css/map/MarkerCluster.Default.css" />


<script src="js/map/leaflet-src.js"></script>
<script src="js/map/leaflet_geosearch2.js"></script>
<script src="js/map/L.Control.ZoomBar.js"></script>
<script src="js/map/L.Control.Locate.js" charset="utf-8"></script>
<script src="js/map/leaflet-color-markers.js"></script>
<script src="js/map/leaflet.groupedlayercontrol.min.js"></script>
<script src="js/map/L.Control.BetterScale.js"></script>
<script src="js/map/iconLayers.js"></script>
<script src="js/map/leaflet-providers.js"></script>
<script src="js/map/leaflet.markercluster.js"></script>
<script src="js/map/esri_leaflet.js"></script>
<script src="js/map/esri_leaflet_renderer.js"></script>
<script src="js/map/jquery.js"></script>

</head>

<div class="site-page">
    <div class="row">
        <div class="title_wrapper2">
            <div class="ikon2 ikon_6" style="width:100px;height:145px;">
            </div>
            <h2 style="top:50px;">Form Pengaduan</h2>
        </div>
        <div id= "top_header_menu_r2">
            <h6><i class="fa fa-user"></i>&nbsp;&nbsp;IDENTITAS PENGGUNA</h6>
        </div>
        <div class="" style="margin:0;">
            <div class="contbox_st1">
                <div class="form-group field-pengaduan-nama_identitas required">
                    <label class="control-label" for="pengaduan-nama_identitas">Nama</label>
                    <input type="text" id="pengaduan-nama_identitas" class="form-control" name="Pengaduan[nama_identitas]">
                    <!-- <div class="help-block">Nama Identitas tidak boleh kosong.</div> -->
                </div>
                <div class="form-group field-pengaduan-telepon_identitas required">
                    <label class="control-label" for="pengaduan-telepon_identitas">Telepon</label>
                    <input type="tel" id="pengaduan-telepon_identitas" class="form-control" name="Pengaduan[telepon_identitas]" maxlength="15">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-email_identitas required">
                    <label class="control-label" for="pengaduan-email_identitas">Email</label>
                    <input type="email" id="pengaduan-email_identitas" class="form-control" name="Pengaduan[email_identitas]">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-alamat_identitas required">
                    <label class="control-label" for="pengaduan-alamat_identitas">Alamat</label>
                    <textarea id="pengaduan-alamat_identitas" class="form-control" name="Pengaduan[alamat_identitas]" rows="2"></textarea>
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-tipe_identitas required">
                    <label class="control-label" for="pengaduan-tipe_identitas">Identitas Pelapor</label>
                    <select id="pengaduan-tipe_identitas" class="form-control" name="Pengaduan[tipe_identitas]">
                        <option value="1">Kementerian/Lembaga</option>
                        <option value="2">Individu</option>
                        <option value="3">Perusahaan</option>
                        <option value="4">Kelompok/Organisasi Masyarakat</option>
                        <option value="5">Masyarakat Adat</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div id= "top_header_menu_r2">
            <h6><i class="fa fa-file-text"></i>&nbsp;&nbsp;PENGADUAN</h6>
        </div>
        <div class="contbox_st1">
            <div class="form-group field-pengaduan-tutuntan_pengaduan required">
                <label class="control-label" for="pengaduan-tutuntan_pengaduan">Tema</label>
                <input type="text" id="pengaduan-tutuntan_pengaduan" class="form-control" name="Pengaduan[tutuntan_pengaduan]">
                <div class="help-block"></div>
            </div>
            <div class="form-group field-pengaduan-pihak_berkonflik">
                <label class="control-label" for="pengaduan-pihak_berkonflik">Pihak Berkonflik</label>
                <input type="text" id="pengaduan-pihak_berkonflik" class="form-control" name="Pengaduan[pihak_berkonflik]">
                <div class="help-block"></div>
            </div>
            <div class="form-group field-pengaduan-rentang_waktu required">
                <label class="control-label" for="pengaduan-rentang_waktu">Perkiraaan lama terjadi konflik</label>
                <input type="text" id="pengaduan-rentang_waktu" class="form-control" name="Pengaduan[rentang_waktu]">
                <div class="help-block"></div>
            </div>

        </div>
        <div id= "top_header_menu_r">
            <h6><i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;KASUS</h6>
        </div>
        <div class="container">
            <div class="row">
                <div class="contbox_st1">

                    <div class="form-group field-pengaduan-tutuntan_pengaduan required">
                        <label class="control-label" for="pengaduan-tutuntan_pengaduan">Tema</label>
                        <select type="text" id="pengaduan-tutuntan_pengaduan" class="form-control" name="Pengaduan[tutuntan_pengaduan]">
                            <option>Kementerian/Lembaga - Kementerian/Lembaga</option>
                            <option>Kementerian/Lembaga - Individu</option>
                            <option>Kementerian/Lembaga - Perusahaan</option>
                            <option>Kementerian/Lembaga - Kelompok/Organisasi Masyarakat</option>
                            <option>Kementerian/Lembaga - Masyarakat Adat</option>
                            <option>Individu - Individu</option>
                            <option>Individu - Perusahaan</option>
                            <option>Individu - Kelompok/Organisasi Masyarakat</option>
                            <option>Individu - Masyarakat Adat</option>
                            <option>Perusahaan - Perusahaan</option>
                            <option>Perusahaan - Kelompok/Organisasi Masyarakat</option>
                            <option>Perusahaan - Masyarakat Adat</option>
                            <option>Kelompok/Organisasi Masyarakat - Kelompok/Organisasi Masyarakat</option>
                            <option>Kelompok/Organisasi Masyarakat - Masyarakat Adat</option>
                            <option>Masyarakat Adat - Masyarakat Adat</option>
                        </select>
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id= "top_header_menu_r">
            <h6><i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;PIHAK BERKONFLIK</h6>
        </div>
        <div class="container">
            <div class="row">
                <div class="form-group field-pihak-berkonflik-1">
                    <label class="control-label" for="pihak-berkonflik-1">Pihak Berkonflik 1</label>
                    <input type="text" id="pihak-berkonflik-1" class="form-control" name="pihak-berkonflik-1">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pihak-berkonflik-2">
                    <label class="control-label" for="pihak-berkonflik-2">Pihak Berkonflik 2</label>
                    <input type="text" id="pihak-berkonflik-2" class="form-control" name="pihak-berkonflik-2">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pihak-berkonflik-3">
                    <label class="control-label" for="pihak-berkonflik-3">Pihak Berkonflik 3</label>
                    <input type="text" id="pihak-berkonflik-3" class="form-control" name="pihak-berkonflik-3">
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div id="top_header_menu_r">
            <h6><i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;LOKASI KONFLIK</h6>
        </div>
        <div class="container">
            <div class="row">
                <div class="form-group field-wilayah">
                    <label class="control-label" for="wilayah">Wilayah</label>
                    <input type="text" id="wilayah" class="form-control" name="wilayah">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-wilayah">
                    <label class="control-label" for="pengaduan-wilayah">Wilayah</label>
                    <select id="pengaduan-wilayah" class="form-control" name="Pengaduan[wilayah]">
                        <option value="">pilih wilayah...</option>
                        <option value="1">Sumatera</option>
                        <option value="2">Kalimantan</option>
                        <option value="3">Sulawesi</option>
                        <option value="4">Jawa Bali Nusa Tenggara</option>
                        <option value="5">Maluku Papua</option>
                    </select>
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-provinsi_konflik required">
                    <label class="control-label" for="pengaduan-provinsi_konflik">Provinsi</label>
                    <select id="pengaduan-provinsi_konflik" class="form-control" name="Pengaduan[provinsi_konflik]" onchange="">
                        <option value="">--Provinsi--</option>
                        <option value="62">KALIMANTAN TENGAH</option>
                        <option value="16">SUMATERA SELATAN</option>
                        <option value="14">RIAU</option>
                        <option value="11">ACEH</option>
                        <option value="12">SUMATERA UTARA</option>
                        <option value="13">SUMATERA BARAT</option>
                        <option value="17">BENGKULU</option>
                        <option value="18">LAMPUNG</option>
                        <option value="19">KEP BANGKA BELITUNG</option>
                        <option value="21">KEP RIAU</option>
                        <option value="32">JAWA BARAT</option>
                        <option value="33">JAWA TENGAH</option>
                        <option value="35">JAWA TIMUR</option>
                        <option value="36">BANTEN</option>
                        <option value="51">BALI</option>
                        <option value="52">NUSA TENGGARA BARAT</option>
                        <option value="53">NUSA TENGGARA TIMUR</option>
                        <option value="61">KALIMANTAN BARAT</option>
                        <option value="63">KALIMANTAN SELATAN</option>
                        <option value="64">KALIMANTAN TIMUR</option>
                        <option value="65">KALIMANTAN UTARA</option>
                        <option value="71">SULAWESI UTARA</option>
                        <option value="72">SULAWESI TENGAH</option>
                        <option value="73">SULAWESI SELATAN</option>
                        <option value="74">SULAWESI TENGGARA</option>
                        <option value="75">GORONTALO</option>
                        <option value="76">SULAWESI BARAT</option>
                        <option value="81">MALUKU</option>
                        <option value="82">MALUKU UTARA</option>
                        <option value="91">PAPUA BARAT</option>
                        <option value="94">PAPUA</option>
                        <option value="31">JAKARTA</option>
                        <option value="34">YOGYAKARTA</option>
                        <option value="15">JAMBI</option>
                    </select>
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-kabkota_konflik required">
                    <label class="control-label" for="pengaduan-kabkota_konflik">Kabkota</label>
                    <select id="pengaduan-kabkota_konflik" class="form-control" name="Pengaduan[kabkota_konflik]">
                        <option value="">--Pilih Kabupaten / Kota--</option>
                    </select>
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-kecamatan">
                    <label class="control-label" for="kecamatan">Kecamatan</label>
                    <input type="text" id="kecamatan" class="form-control" name="kecamatan">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-desa-kelurahan">
                    <label class="control-label" for="desa-kelurahan">Desa/Kelurahan</label>
                    <input type="text" id="desa-kelurahan" class="form-control" name="desa-kelurahan">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-koordinat-latitude">
                    <label class="control-label" for="koordinat-latitude">Latitude</label>
                    <input type="text" id="koordinat-latitude" class="form-control" name="koordinat-latitude">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-koordinat-longitude">
                    <label class="control-label" for="koordinat-longitude">Longitude</label>
                    <input type="text" id="koordinat-longitude" class="form-control" name="koordinat-longitude">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-lokasi_konflik required">
                    <label class="control-label" for="pengaduan-lokasi_konflik">Lokasi Lengkap</label>
                    <input type="text" id="pengaduan-lokasi_konflik" class="form-control" name="Pengaduan[lokasi_konflik]">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-fungsi_kawasan">
                    <label class="control-label" for="pengaduan-fungsi_kawasan">Fungsi Kawasan</label>
                    <input type="text" id="pengaduan-fungsi_kawasan" class="form-control" name="Pengaduan[fungsi_kawasan]">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pengaduan-luasan_kawasan">
                    <label class="control-label" for="pengaduan-luasan_kawasan">Luasan Kawasan</label>
                    <input type="text" id="pengaduan-luasan_kawasan" class="form-control" name="Pengaduan[luasan_kawasan]">
                    <div class="help-block"></div>
                </div>
				<div class="map_wrapper">
                    <div id="map" style="width:100%;height:500px;background:yellow"></div>
                        <script>
                            var rememberLat = document.getElementById('latitude').value;
							var rememberLong = document.getElementById('longitude').value;
							if( !rememberLat || !rememberLong ) { rememberLat = 0.301; rememberLong = 118.821;}
							
							var map = L.map('map', {
	                            zoomControl: false
                                }).setView([0.301,118.821], 5);

                            var zoom_bar = new L.Control.ZoomBar({position: 'topleft'}).addTo(map);

                            var marker = L.marker([rememberLat, rememberLong],{
								draggable: true
							}).addTo(map);

							marker.on('dragend', function (e) {
							updateLatLng(marker.getLatLng().lat, marker.getLatLng().lng);
							});
							
							map.on('click', function (e) {
							marker.setLatLng(e.latlng);
							updateLatLng(marker.getLatLng().lat, marker.getLatLng().lng);
							});

							function updateLatLng(lat,lng,reverse) {
								if(reverse) {
								marker.setLatLng([lat,lng]);
								map.panTo([lat,lng]);
							} else {
								document.getElementById('latitude').value = marker.getLatLng().lat;
								document.getElementById('longitude').value = marker.getLatLng().lng;
								map.panTo([lat,lng]);
							}
							}
				
				            var google_hybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
                                maxZoom: 20,
                                subdomains:['mt0','mt1','mt2','mt3']
                            });

                            var google_streetmap = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
                                maxZoom: 20,
                                subdomains:['mt0','mt1','mt2','mt3']
                            });

                            var google_terrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
                                maxZoom: 20,
                                subdomains:['mt0','mt1','mt2','mt3']
                            });

                            var google_satellite = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
                                maxZoom: 20,
                                subdomains:['mt0','mt1','mt2','mt3']
                            });
				
				            var iconLayersControl = new L.Control.IconLayers(
                                [
                                    {
                                       title: 'Google Streets',
                                       layer: google_streetmap,
                                       icon: 'images/map/Google_Street.png'
                                    },
		                            {
                                       title: 'Google Satellite',
                                       layer: google_satellite,
                                       icon: 'images/map/esri_Satellite.png'
                                    },        
		                            {
                                       title: 'Google Hybrid',
                                       layer: google_hybrid,
                                       icon: 'images/map/google_hybrid2.png'
                                    },
		                            {
                                       title: 'Google Terrain',
                                       layer: google_terrain,
                                       icon: 'images/map/google_terrain.png'
                                    },
                                ], {
                                       position: 'topright',
                                       maxLayersInRow: 5
                                   }
                            );

                            iconLayersControl.addTo(map);

                            var kawasan_hutan = L.esri.tiledMapLayer({
		                        url: "http://139.255.83.75:6080/arcgis/rest/services/Forest/Kawasan_Hutan/MapServer/"
		                    });

                            var hutan_adat = L.esri.featureLayer({
                                url: 'http://geoportal.menlhk.go.id/arcgis/rest/services/test/Penetapan_Hutan_Adat2/FeatureServer/0'
                            });

                            hutan_adat.bindPopup(function (layer) {
                            return L.Util.template('<h4>Keterangan Hutan Adat</h4>' +
	                            '<table style="font-size:100%" width="250">' +
	                            '<tr><td width="100">Kode Provinsi</td><td>:</td><td>{kode_prov}</tr></td>' +
	                            '<tr><td>Masyarakat Adat</td><td>:</td><td>{masy_adat}</tr></td>' +
	                            '<tr><td>Fungsi Kawasan</td><td>:</td><td>{fungsi_kws}</tr></td>' +
	                            '<tr><td>SK Adat</td><td>:</td><td>{sk_adat}</tr></td>' +
	                            '<tr><td>Tanggal Adat</td><td>:</td><td>{tgl_adat}</tr></td>' +
	                            '<tr><td>Luas Hutan Adat</td><td>:</td><td>{ls_adat}</tr></td>' +
	                            '<tr><td>Keternagan</td><td>:</td><td>{keterangan}</tr></td>' +
	                            '</table>', layer.feature.properties);
                            });

							var sebaran_konflik = L.layerGroup();
								var jabalnus = L.esri.featureLayer({
									url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Jabalnus2/FeatureServer/0"
								}).addTo(sebaran_konflik);
                
								jabalnus.bindPopup(function (layer) {
								return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
									'<table style="font-size:100%" width="250">' +
									'<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
									'<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
									'<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
									'<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
									'</table>', layer.feature.properties);
								});

								var kalimantan = L.esri.featureLayer({
									url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Kalimantan2/FeatureServer/0"
								}).addTo(sebaran_konflik);
                
								kalimantan.bindPopup(function (layer) {
								return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
									'<table style="font-size:100%" width="250">' +
									'<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
									'<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
									'<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
									'<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
									'</table>', layer.feature.properties);
								});

								var maluku_papua = L.esri.featureLayer({
									url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Maluku_Papua2/FeatureServer/0"
									}).addTo(sebaran_konflik);
								
								maluku_papua.bindPopup(function (layer) {
								return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
									'<table style="font-size:100%" width="250">' +
									'<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
									'<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
									'<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
									'<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
									'</table>', layer.feature.properties);
								});

								var sulawesi = L.esri.featureLayer({
									url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sulawesi2/FeatureServer/0"
								}).addTo(sebaran_konflik);
                
								sulawesi.bindPopup(function (layer) {
								return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
									'<table style="font-size:100%" width="250">' +
									'<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
									'<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
									'<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
									'<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
									'</table>', layer.feature.properties);
								});

								var sumatra = L.esri.featureLayer({
									url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sumatera2/FeatureServer/0"
								}).addTo(sebaran_konflik);
                
								sumatra.bindPopup(function (layer) {
								return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
									'<table style="font-size:100%" width="250">' +
									'<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
									'<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
									'<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
									'<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
									'</table>', layer.feature.properties);
								});

							var overlayLayers = {
								"Data Hutan": {
								"Hutan Adat": hutan_adat,
								"Potensi Sebaran Konflik": sebaran_konflik,
								"Fungsi Kawasan Hutan": kawasan_hutan
								}
							};

							L.control.groupedLayers(null, overlayLayers).addTo(map);

							var GeoSearchControl = window.GeoSearch.GeoSearchControl;
							var GoogleProvider = window.GeoSearch.GoogleProvider;

							var provider = new GoogleProvider({
								params: {
								key: 'AIzaSyDW1hC9P9SLkyaynoCE-mvJhfNRKYrZcK4',
								},
							});

							var searchControl = new GeoSearchControl({
								provider: provider,
								position: 'topright',
								showMarker: false,
								showPopup: true,
								marker: {
									icon: new L.Icon.Default(),
									draggable: true,
								},
								popupFormat: ({ query, result }) => result.label,
								maxMarkers: 1,
								retainZoomLevel: false,
								animateZoom: true,
								autoComplete: true,
								autoCompleteDelay: 150,
								autoClose: false,
								searchLabel: 'Cari Alamat',
								keepResult: false
							}).addTo(map);

							var lc = L.control.locate({
								position: 'topleft',
								icon: 'fa fa-map-marker fa-2x',
								iconLoading: 'fa fa-spinner fa-spin fa-2x',
								locateOptions: {
									enableHighAccuracy: true
								},
								strings: {
									title: "Cari Lokasi Anda Saat Ini",
									popup: "Anda Berada Disekitar Sini",
									outsideMapBoundsMsg: "Anda Berada di Luar Wilayah Negara Republik Indonesia"
								}}).addTo(map);

							L.control.betterscale({
								imperial: false,
								metric: true
							}).addTo(map);
					  </script>
				</div>
            </div>
        </div>
        <div class="form-group pull-right">
            <button type="submit" class="btn btn-success">Lapor</button>        <br>
        </div>

    </div>
</div>
