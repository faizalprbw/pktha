<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $model->judul;

$url = 'site/' . strtolower(str_replace(" ", "", $model->kategori->kategori));

if ($model->kategori->kategori == "Kegiatan") {
    $url = 'site/kegiatandanberita';
}

$this->params['breadcrumbs'][] = ['label' => $model->kategori->kategori, 'url' => [$url]];
$this->params['breadcrumbs'][] = $this->title;

// var_dump($model->created_at);
?>



<div class="site-single site-page">
    <div class="container">
        <div class="section_holder52">
            <div class="post_text">
                <?= Yii::$app->formatter->asDatetime($model->created_at, "php:D,M Y"); ?>
                <a href="#"><h3 class="uppercase padd_top1"><?= Html::encode($model->judul) ?></h3></a>
                <span><?= $model->headline ?></span>
                <p><?= $model->konten ?></p><br>

                <?php if (isset($model->image)): ?>
                    <span><a href="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" target="_blank" title="<?= $model->caption ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            Download PDF File</a></span>
                                 <!--<img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image">-->

          <!--  <div class="content-news-caption"><?= $model->caption ?></div>-->
                <?php endif; ?>

                <div class="divider_line"></div>

            </div>
        </div>





    </div>
</div>