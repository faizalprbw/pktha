<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Kegiatan dan Berita';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-kegiatanberita site-page">
    <div class="container">
        <h1><?php echo Html::encode($this->title) ?></h1>
        <div class="row">
            <div class="col-md-12">
                <?php
                echo ListView::widget(
                    [
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('_kegiata_berita', ['model' => $model]);
                    },
                    'layout' => '{items}<div class="pager-wrapper">{pager}</div>'
                    ]
                );
                ?>
            </div>
        </div>
    </div>
</div>
