<?php
Yii::$app->view->registerCSSFile('css/map/leaflet.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.ZoomBar.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.Locate.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet.groupedlayercontrol.min.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/L.Control.BetterScale.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/iconLayers.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/MarkerCluster.Default.css', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerCSSFile('css/map/leaflet_geosearch.css', ['position' => yii\web\View::POS_HEAD]);

Yii::$app->view->registerJsFile('js/map/leaflet-src.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.ZoomBar.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.Locate.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-color-markers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.groupedlayercontrol.min.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/L.Control.BetterScale.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/iconLayers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-providers.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet.markercluster.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/esri_leaflet.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/jquery.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/feature_group.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet_geosearch2.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/leaflet-fill-Pattern.js', ['position' => yii\web\View::POS_HEAD]);
Yii::$app->view->registerJsFile('js/map/panggil_peta.js', ['position' => yii\web\View::POS_END]);
Yii::$app->view->registerJs('var link_lokasi_pengaduan = ' .json_encode(yii\helpers\Url::to(['/site/lokasipengaduan'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_pencantuman_HA = ' .json_encode(yii\helpers\Url::to(['/site/pencantumanhutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_potensi_HA = ' .json_encode(yii\helpers\Url::to(['/site/potensihutanadat'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2016 = ' .json_encode(yii\helpers\Url::to(['/site/kasus2016'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_kasus2017 = ' .json_encode(yii\helpers\Url::to(['/site/kasus2017'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_provinsi = ' .json_encode(yii\helpers\Url::to(['/site/produkhukumprovinsi'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var link_produk_hukum_kabupaten = ' .json_encode(yii\helpers\Url::to(['/site/produkhukumkabupaten'])) . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_pencantuman = ' .$jumlah_pencantuman . ';', yii\web\View::POS_HEAD);
Yii::$app->view->registerJs('var jumlah_potensi = ' .$jumlah_potensi . ';', yii\web\View::POS_HEAD);
?>


</head>

<div class="site-page">
    <div class="row">
      <div class="title_wrapper202">
            <div class="ikon2 ikon_2" style="margin-top: -15px;">
            </div>
            <h2>Peta Potensi Konflik</h2>
          </div>
        <div class="map_wrapper">
                    <div id="map" style="width:100%;height:500px;background:white"></div>

        </div>

    </div>
</div>
