<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UsulanForm */
/* @var $form ActiveForm */
?>
<div class="site-FormPengusulanHutanAdat">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'tlp') ?>
        <?= $form->field($model, 'body') ?>
        <?= $form->field($model, 'verifyCode') ?>
        <?= $form->field($model, 'upload') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-FormPengusulanHutanAdat -->
