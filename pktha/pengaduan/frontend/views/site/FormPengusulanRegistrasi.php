<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PengusulanRegistrasi */
/* @var $form ActiveForm */

/**
 * Please refer JS file to js/pengusulan-hutan-adat/script.js
 */
?>

<div class="title_wrapper2">
    <div class="ikon2 ikon_6" style="width:100px;height:145px;">
    </div>
    <h2 style="top:50px;">Form Usulan Hutan Adat</h2>
</div>
<div class="clear">

</div>

<div class="FormPengusulanRegistrasi box_wrapper">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php echo $form->field($model, 'nama_pengusul') ?>
    <?php echo $form->field($model, 'nama_mha') ?>
    <?php echo $form->field($model, 'no_ktp_pengusul') ?>
    <?php echo $form->field($model, 'alamat_lengkap_pengusul') ?>
    <?php echo $form->field($model, 'email_pengusul') ?>
    <?php echo $form->field($model, 'no_telepon_pengusul')->textInput(['type' => 'numeric']) ?>

    <?php echo
    $form->field($model, 'id_wilayah')->dropDownList(
        \yii\helpers\ArrayHelper::map(\common\models\Wilayah::find()->all(), 'id', 'nama_wilayah'), [
        'prompt' => 'Pilih Wilayah',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/provinsi"]) . '", {id_wilayah: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_provinsi").html(data); })',
            ]
    )->label('Wilayah')
    ?>
    <?php echo
    $form->field($model, 'id_provinsi')->dropDownList(
        \yii\helpers\ArrayHelper::map(\common\models\Provinsi::find()->where(['id' => 1])->all(), 'id', 'nama_provinsi'), [
        'prompt' => 'Pilih Provinsi',
        'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kota-kabupaten"]) . '", {id_provinsi: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_kota_kabupaten").html(data) })'
            ]
    )->label('Provinsi (wajib)')
    ?>

    <!-- Kota Kabupaten -->
    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_kota_kabupaten')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\KotaKabupaten::find()->where(['id' => 1])->all(), 'id', 'nama_kota_kabupaten'), [
                'prompt' => 'Pilih Kota / Kabupaten',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/kecamatan"]) . '", {id_kota_kabupaten: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_kecamatan").html(data) })'
                    ]
            )->label('Kota / Kabupaten')
            ?>
        </div>
        <div class="col-md-7">
            <?php echo
            $form->field($model, 'teks_kota_kabupaten')->textInput()->label('Lainnya')
            ?>
        </div>
    </div>
    <!-- END -->

    <!-- Kecamatan -->
    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_kecamatan')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\Kecamatan::find()->where(['id' => 1])->all(), 'id', 'nama_kecamatan'), [
                'prompt' => 'Pilih Kecamatan',
                'onchange' => '$.post("' . \yii\helpers\Url::to(["location/desa-kelurahan"]) . '", {id_kecamatan: $(this).val()}, function(data){ $("#pengusulanregistrasi-id_desa_kelurahan").html(data) })'
                    ]
            )->label('Kecamatan')
            ?>
        </div>
        <div class="col-md-7">
            <?php echo
            $form->field($model, 'teks_kecamatan')->textInput()->label('Lainnya')
            ?>
        </div>
    </div>
    <!-- END -->

    <!-- Desa Kelurahan -->
    <div class="row">
        <div class="col-md-5">
            <?php echo
            $form->field($model, 'id_desa_kelurahan')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\DesaKelurahan::find()->where(['id' => 1])->all(), 'id', 'nama_desa_kelurahan'), [
                'prompt' => 'Pilih Desa / Kelurahan',
                    ]
            )->label('Desa / Kelurahan')
            ?>
        </div>
        <div class="col-md-7">
            <?php echo
            $form->field($model, 'teks_desa_kelurahan')->textInput()->label('Lainnya')
            ?>
        </div>
    </div>
    <!-- END -->

    <?php echo $form->field($model, 'dusun')->textarea() ?>
    <?php echo $form->field($model, 'keterangan')->textarea() ?>

    <?php echo $form->field($model, 'form_data')->fileInput()->label('Form Pengusulan') ?>
    <?php echo $form->field($model, 'profil_mha_data')->fileInput()->label('Profil Masyarakat Hutan Adat') ?>
    <?php echo $form->field($model, 'surat_kuasa_data')->fileInput()->label('Surat Kuasa Masyarakat Hutan Adat') ?>
    <?php echo $form->field($model, 'surat_pernyataan_data')->fileInput()->label('Surat Pernyataan') ?>
    <?php echo $form->field($model, 'produk_hukum_daerah_data')->fileInput()->label('Produk Hukum Daerah') ?>
    <?php echo $form->field($model, 'peta_data')->fileInput()->label('Peta') ?>
    <?php echo $form->field($model, 'identitas_ketua_adat_data')->fileInput()->label('Identitas Ketua Adat') ?>


    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- FormPengusulanRegistrasi -->
