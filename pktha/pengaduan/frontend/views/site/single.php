<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Single';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-single">
    <div class="row">
        <div class="col-md-7">
            <img src="img/gallery/gallery-img-1-full.jpg" class="align-left thumbnail" alt="image">
        </div>
        <div class="col-md-4">
            <h3>Single Post</h3>
            <p class="lead">For an international ad campaign. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus</p>
            <ul class="project-info">
                <li><h6>Date:</h6> 09/12/15</li>
                <li><h6>Client:</h6> John Doe, Inc.</li>
                <li><h6>Services:</h6> Design, Illustration</li>
                <li><h6>Art Director:</h6> Jane Doe</li>
                <li><h6>Designer:</h6> Jimmy Doe</li>
            </ul>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. Vivamus tincidunt sem eu magna varius elementum. Maecenas felis tellus, fermentum vitae laoreet vitae, volutpat et urna. Nulla faucibus ligula eget ante varius ac euismod odio placerat. Nam sit amet felis non lorem faucibus rhoncus vitae id dui.</p>
        </div>
    </div>
</div>
