<?php

use yii\helpers\Html;
use yii\helpers\Url;

$st = Yii::$app->controller->action->id;
?>

<div class="section_holder52">
    <div class="container">

        <div class="comment_info">
            <div class="left">
                <div class="img"><img src="<?= Yii::getAlias('@web') . '/../../uploads/' . $model->image; ?>" alt="image" width="150%" class="img-responsive"></div>
            </div>
            <div class="right">
                <h4 class="name"><?= Html::a(Html::encode($model->judul), ['detail', 'id' => $model->id]); ?> </h4>
                <span class="date"><?= Yii::$app->formatter->asDatetime($model->created_at, "php:d M Y"); ?></span>
                <p class="fl_left"><?= $model->headline ?>.</p>
                <br/>
                <br/>
                <br/>
                <?= Html::a('Read more...', ['detail', 'id' => $model->id], ['class' => "readmore"]); ?>
            </div>
        </div>
    </div>
</div>
