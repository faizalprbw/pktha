<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="container">
	<div class="row header"><!-- Begin Header -->

        <!-- Logo
        ================================================== -->
        <div class="col-md-12 logo">
        	<a href="index.htm"><img src="img/logo.png" alt="" /></a>
        	<h5>Pengaduan Konflik Tenurial dan Hutan Adat<br>Direktorat Jenderal Perhutanan Sosial dan Kemitraan Lingkungan<br>Kementerian Lingkungan Hidup dan Kehutanan</h5>
        </div>

        <div class="clear"></div>
        <br/>
        <br/>




    <?php
    NavBar::begin([
        // 'brandLabel' => Yii::$app->name,
        // 'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar hidden-phone ',
        ],
    ]);
    $menuItems = [
        ['label' => 'Beranda', 'url' => ['/site/index']],
        ['label' => 'Tentang', 'url' => ['/site/about']],        
        ['label' => 'Tata Cara', 'url' => ['/site/tatacara']],
        ['label' => 'Cerita Sukses', 'url' => ['/site/ceritasukses']],
        // ['label' => 'Form Pengaduan', 'url' => ['/pengaduan/index']],
        ['label' => 'Kegiatan dan Berita', 'url' => ['/site/kegiatandanberita']],
        ['label' => 'Hubungi Kami', 'url' => ['/site/contact']],
    ];
    // if (Yii::$app->user->isGuest) {
    //     $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    //     $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    // } else {
    //     $menuItems[] = [
    //         'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
    //         'url' => ['/site/logout'],
    //         'linkOptions' => ['data-method' => 'post']
    //     ];
    // }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    </div>
    <div class="row">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        <br/>
    </div>
  </div>


<footer class="footer-container"><!-- Begin Footer -->
        <div class="container">
            <div class="row footer-row">
                <div class="col-md-4 footer-col">
                    <h5>Hubungi Kami</h5>
                    <address>
                        
                        <strong> Direktorat Penanganan Konflik Tenurial dan Hutan Adat</strong><br/>
                        Kementerian Lingkungan Hidup dan Kehutanan<br/>
                        Gedung Manggala Wanabakti Wing B Blok IV Lantai 4<br/>
                        Jln. Jend. Gatot Subroto, Jakarta 10270<br/>
                        Ph/Fax. 021-57854198<br/>


                    </address>
                </div>
                <div class="col-md-4 footer-col righty">


                <?php
                $session = Yii::$app->session;
        // var_dump($session['tahap']);
                ?>
                    <h5>Stastistik Pengaduan </h5>
                    <h4>TOTAL PENGADUAN  : <?= $session['tahap']['t1'] + $session['tahap']['t2'] + $session['tahap']['t3'] +$session['tahap']['t4']?> </h4>

                    <table>
                        <tr>
                            <td>Tahap Registrasi</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?=$session['tahap']['t1']?></td>
                        </tr>
                        <tr>
                            <td>Tahap Assesment Lapangan</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?=$session['tahap']['t2']?></td>
                        </tr>
                    </table>
                    
                    

                  <!-- <div>                  
                    <h5>Total Pengaduan: <span></span></h5>
                    <p>Tahap Registrasi: <span>XX</span><br>
                    Tahap Verifikasi Dokumen: <span>XX</span><br>
                    Tahap Assesment Lapangan: <span>XX</span><br>
                    Tahap Analisa Permasalahan: <span>XX</span><br>
                    Tahap Rekomendasi: <span>XX</span></p>
                  </div> -->
                </div>
                <div class="col-md-4 footer-col righty">
                    <h5>&nbsp</h5>
                    <h4>&nbsp</h4>
                    
                    <table>
                        <tr>
                            <td>Tahap Analisa Permasalahan</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?=$session['tahap']['t3']?></td>
                        </tr>
                        <tr>
                            <td>Tahap Rekomendasi</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td><?=$session['tahap']['t4']?></td>
                        </tr>
                    </table>
                    
                    
                </div>
            </div>



            <div class="row"><!-- Begin Sub Footer -->
                <div class="col-md-12 footer-col footer-sub">
                    <div class="row no-margin">
                        <div><span class="left">Copyright &copy; 2016 Direktorat Penanganan Konflik Tenurial dan Hutan Adat.</span></div>
                        <div>
                            <span class="right">


                            <a href="<?=Url::to(['site/index']);?>">Beranda</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <a href="<?=Url::to(['site/about']);?>">Tentang</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <a href="<?=Url::to(['site/tatacara']);?>">Cerita Sukses</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <a href="<?=Url::to(['site/kegiatandanberita']);?>">Kegiatan &amp; Berita</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                            <a href="<?=Url::to(['site/contact']);?>">Hubungi Kami</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div><!-- End Sub Footer -->

            <br/>

        </div>

    </footer><!-- End Footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
