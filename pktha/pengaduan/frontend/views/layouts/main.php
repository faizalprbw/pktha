<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\assets\AppAsset;

AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <head>
        <title>Pengaduan Konflik, Tenurial dan Hutan Adat</title>
        <meta charset="utf-8">
        <!-- Meta -->
        <meta name="keywords" content="" />
        <meta name="author" content="">
        <meta name="robots" content="" />
        <meta name="description" content="" />


        <!-- this styles only adds some repairs on idevices  -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <?= Html::csrfMetaTags() ?>

        <?php $this->head() ?>


        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Google fonts - witch you want to use - (rest you can just remove) -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- ######### CSS STYLES ######### -->

        <link rel="stylesheet" href="css/reset.css" type="text/css" />
        <!-- <link rel="stylesheet" href="css/style.css" type="text/css" /> -->
        <link rel="stylesheet" href="css/style2.css" type="text/css" />


        <!-- responsive devices styles -->
        <link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

        <!-- mega menu -->
        <link href="js/mainmenu/sticky.css" rel="stylesheet">

        <link href="js/mainmenu/demo.css" rel="stylesheet">
        <link href="js/mainmenu/menu.css" rel="stylesheet">
        <style type="text/css">
            .navbar-default {
                background-color: transparent !important;
                border-color: transparent;
            }
            .navbar {
                margin-bottom: 0px;
            }
            #trueHeader .wrapper {
                overflow: visible;
            }
        </style>

        <!-- revolution slider -->

        <!-- CSS STYLE-->
        <link rel="stylesheet" type="text/css" href="js/revolutionslider/css/style.css" media="screen" />

        <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="js/revolutionslider/css/extralayers.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="js/revolutionslider/rs-plugin/css/settings.css" media="screen" />

        <!-- simple line icons -->
        <link rel="stylesheet" type="text/css" href="css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />

        <!-- progressbar -->
        <link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">

        <link rel="stylesheet" href="css/colors/bridge.css" />

        <!-- forms -->
        <link rel="stylesheet" href="js/form/sky-forms.css" type="text/css" media="all">

        <!-- just remove the below comments witch bg patterns you want to use -->
        <link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />

        <!-- css baru untuk pktha update (hardcoded) -->
        <link rel="stylesheet" href="css/style_new.css" type="text/css" />

    </head>

    <body style="background-image:none; background-color: #fff">
        <header id="header" >
            <div id="topHeader">
                <div class="wrapper">
                    <div class="top_nav" >
                        <div class="container" >
                            <div class="row">
                                <div class="col-sm-12 col-md-12" >
                                    <a href="/pktha/pengaduan/frontend/web/index.php?r=site%2Findex">
                                        <div class="logo padd_top1" >
                                            <img  width="72px" alt="" src="images/colors/bridge/logo.png">
                                        </div>
                                        <span>
                                            <h4 class="padd_top1" style="color:#fff; margin-bottom:2px;"><strong>Pengaduan Konflik, Tenurial dan Hutan Adat</strong></h4>
                                            <p style="color:#fff;">Direktorat Jenderal Perhutanan Sosial dan Kemitraan Lingkungan<br/>Kementerian Lingkungan Hidup dan Kehutanan</p>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <!-- end right social links -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="trueHeader" style="width:100% ; background-color : #39B54A">
                <div class="wrapper nav_menu " style="background-color:#39B54A">
                    <div class="container  " >

                        <!-- Logo -->
                        <!--div class="logo"><a href="index.html" id="logo"></a></div-->
                        <!-- Menu -->
                        <div class="menu_main">
                            <div class="navbar yamm navbar-default">
                                <div class="container">
                                    <?php
                                    NavBar::begin([
                                        'options' => [
                                            'class' => 'nav navbar-nav',
                                        ],
                                        'screenReaderToggleText' => 'Menu',
                                    ]);
                                    $menuItems = [
                                            [
                                            'label' => 'Beranda',
                                            // 'url' => ['/site/index'],
                                            'items' => [
                                                    ['label' => "Foto Kegiatan", 'url' => ["/site/foto-kegiatan"]],
                                                    ['label' => "Link Pengaduan", 'url' => ["/site/link-pengaduan"]],
                                                    ['label' => "Jumlah Penanganan Pengaduan", 'url' => ["/site/jumlah-penanganan-pengaduan"]],
                                                    ['label' => "Peta Potensi ", 'url' => ["/site/peta-potensi"]],
                                            ],
                                        ],
                                            [
                                            'label' => 'Tentang',
                                            // 'url' => ['/site/about'],
                                            'items' => [
                                                    ['label' => "Direktorat PKTHA", 'url' => ["/site/direktorat-pktha"]],
                                                    ['label' => "Struktur Organisasi", 'url' => ["/site/struktur-organisasi"]],
                                                    ['label' => "Tugas Pokok dan Fungsi", 'url' => ["/site/tugas-pokok-dan-fungsi"]],
                                                    ['label' => "Peraturan Perundangan Terkait", 'url' => ["/site/peraturandanperundangan"]],
                                                    ['label' => "Lain-Lain", 'url' => ["/site/lain-lain"]],
                                            ]
                                        ],
                                            [
                                            'label' => 'Tata Cara',
                                            'items' => [
                                                    ['label' => "Pengaduan Konflik", 'url' => ["/site/tatacara-pengaduan-konflik"]],
                                                    ['label' => "Pengusulan Hutan Adat", 'url' => ["/site/tatacara-pengusulan-hutan-adat"]],
                                                    ['label' => "Masyarakat Hukum Adat", 'url' => ["/site/tatacara-registrasi-masyarakat-hukum-adat"]],
                                            ]
                                        ],
                                            [
                                            'label' => 'Berita',
                                            // 'url' => ['/site/ceritasukses'],
                                            'items' => [
                                                    ['label' => "Kegiatan", 'url' => ["/site/kegiatandanberita"]],
                                                    ['label' => "Cerita Sukses", 'url' => ["/site/ceritasukses"]],
                                                    ['label' => "Peraturan Terbaru", 'url' => ["/site/peraturan-terbaru"]],
                                                    ['label' => "Buku", 'url' => ["/site/buku"]],
                                                    ['label' => "Peta", 'url' => ["/site/peta"]],
                                            ]
                                        ],
                                            [
                                            'label' => 'Kontak',
                                            // 'url' => ['/site/kegiatandanberita'],
                                            'items' => [
                                                    ['label' => "Direktorat PKTHA", 'url' => ["site/kontak-direktorat-pktha"]],
                                                    ['label' => "Dirjen PSKL", 'url' => ["site/kontak-dirjen-pskl"]],
                                            ]
                                        ],
                                        [
                                            'label' => 'Cek Status Pengaduan / Pengusulan',
                                            'url' => ['/status/status-check'],
                                        ],
                                            [
                                            'label' => 'Login',
                                            'url' => ['/site/login-dashboard']
                                        ]
                                    ];
                                    echo Nav::widget([
                                        'options' => ['class' => 'navbar-nav'],
                                        'items' => $menuItems,
                                    ]);
                                    NavBar::end();
                                    ?>
                                </div>
                            </div>
                            <!-- end menu -->

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="wrapper_boxed">
            <div class="site_wrapper">
                <div class="clear" style="border-color:white;">
                </div>
                <div class="content" id="content2">
                    <!-- CONTENT -->
                    <?= $content ?>
                    <!-- CONTENT -->
                </div>
                <div class="clear"></div>

                <a href="#" class="scrollup" id="scroller"></a><!-- end scroll to top of the page-->
            </div></div>
        <!-- <div class="top_line">
        <div id= "top_line_l"></div>
        <div id= "top_line_r"></div>
        <div id= "top_line_c"></div>
    </div> -->
        <div class="footer" >
            <div class="container" id="foot">
                <div class="three_fourth" >
                    <span class="title">Direktorat Penanganan Konflik Tenurial dan Hutan Adat</span><br/><br/>
                    <!-- <div class="title_line"></div> -->
                    <p class="smalltext">
                        Kementerian Lingkungan Hidup dan Kehutanan<br/>
                        Gedung Manggala Wanabakti Wing B Blok IV Lantai 4<br/>
                        Jln. Jend. Gatot Subroto, Jakarta 10270<br/>
                        Ph/Fax. 021-57854198<br/>
                    </p>
                </div>
                <!--end item-->

                <div class="one_fourth last">
                    <div class="social_icons_holder">
                        <div class="margin_top10"></div>
                    </div>
                </div>
                <!--end item-->

            </div>
        </div>
        <!--end footer-->

        <div class="copyrights">
            <div class="container"><span>Copyright © <?php echo date("Y"); ?> Direktorat Penanganan Konflik Tenurial dan Hutan Adat.</span></div>
        </div>
        <!--end copyrights-->
        <?php
        Yii::$app->view->registerJsFile('@web/js/revolutionslider/rs-plugin/js/jquery.themepunch.tools.min.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/revolutionslider/custom1.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        ?>
        <!-- mega menu -->
        <?php
        Yii::$app->view->registerJsFile('@web/js/mainmenu/customeUI.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/mainmenu/sticky.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/mainmenu/modernizr.custom.75180.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/scrolltotop/totop.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        Yii::$app->view->registerJsFile('@web/js/progressbar/progress.js', ['position' => yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
