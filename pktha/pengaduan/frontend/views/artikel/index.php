<div class="artikel-ceritasukses">
    <div class="body-content">        
        <div class="row">
            <div class="col-lg-12">
                <?php
                foreach ($models as $model):
                    ?>
                    <header class="entry-header page-header">
                        <h1 class="entry-title"><a href="#" rel="bookmark"><?= $model->judul; ?></a></h1>
                        <div class="entry-meta">
                            <span class="posted-on"><i class="fa fa-calendar"></i>                            
                                <time class="entry-date published"><?= date("M j, Y h:i A", $model->created_at); ?></time>                                                        
                            </span>
                            <span class="byline"> </span>                        
                        </div><!-- .entry-meta -->
                    </header>
                    <?php
                endforeach;
                ?>  
            </div>
        </div>
    </div>
</div>