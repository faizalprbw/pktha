<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Sistem Database Konflik Tenurial',
    'language' => 'ID',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
	'urlManager' => [
            'class' => '\yii\web\UrlManager',
            'baseUrl' => '/pktha/pengaduan/frontend/web',
            'enablePrettyUrl' => false,
            'showScriptName' => false,
        ],
        'urlManagerBackend' => [
            'class' => '\yii\web\UrlManager',
            'baseUrl' => '/pktha/pengaduan/backend/web',
            'enablePrettyUrl' => false,
            'showScriptName' => false,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            // [
            // 'class' => 'yii\log\EmailTarget',
            // 'levels' => ['error', 'warning'],
            // 'message' => [
            //    'from' => ['pengaduan.pktha@gmail.com'],
            //    'to' => ['vei_btx@yahoo.com'],
            //    'subject' => 'Database errors at example.com',
            // ],
            // ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    // 'mailer' => [
    //     'class' => 'yii\swiftmailer\Mailer',
    //     'transport' => [
    //         'class' => 'Swift_SmtpTransport',
    //         'host' => 'smtp.gmail.com',
    //         'username' => 'pengaduan.pktha@gmail.com',
    //         'password' => 'pktha@2016',
    //         'port' => '587',
    //         'encryption' => 'tls',
    //     ],
    // ],
    ],
    'params' => $params,
];

