<?php

namespace frontend\controllers;

use common\models\PengaduanRegistrasi;
use common\models\PengaduanAssesmen;
use common\models\PengaduanPraMediasi;
use common\models\PengaduanMediasi;
use common\models\PengaduanDraftingMou;
use common\models\PengaduanTandaTanganMou;
use common\models\PengusulanRegistrasi;
use common\models\PengusulanValidasi;
use common\models\PengusulanVerifikasi;
use common\models\PengusulanPencantuman;
use common\models\PengusulanMonev;
use common\viewmodels\PengaduanCreatedInfoViewModel;
use common\viewmodels\PengaduanNotFoundViewModel;
use common\viewmodels\PengaduanViewModel;
use common\viewmodels\PengusulanCreatedInfoViewModel;
use common\viewmodels\PengusulanNotFoundViewModel;
use common\viewmodels\PengusulanViewModel;

class StatusController extends \yii\web\Controller {

    public function actionStatusCheck() {
        $model = new \common\viewmodels\StatusCheckForm();

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here

                $caseCode = $model->kode_kasus;
                $caseKey = $model->kunci_kasus;

                if (isset($caseCode) && isset($caseKey)) {
                    $caseCodeArr = explode('-', $caseCode);

                    if ($caseCodeArr[0] === 'HA') {
                        // Proceed with Hutan Adat table

                        $pengusulan = PengusulanRegistrasi::findOne(['kode_pengusulan' => $caseCode, 'kunci_pengusulan' => sha1($caseKey)]);
                        if (isset($pengusulan)) {

                            $pengusulanVerifikasi = PengusulanValidasi::findOne(['id_pengusulan' => $pengusulan->id]);
                            $pengusulanValidasi = PengusulanVerifikasi::findOne(['id_pengusulan' => $pengusulan->id]);
                            $pengusulanPenetapanPencantuman = PengusulanPencantuman::findOne(['id_pengusulan' => $pengusulan->id]);
                            $pengusulanMonev = PengusulanMonev::findOne(['id_pengusulan' => $pengusulan->id]);
                            $statusPengusulan = 'Unknown';
                            if (!$pengusulanVerifikasi->finished) {
                                $statusPengusulan = 'Pengusulan Sedang Di Verifikasi';
                            } else if ($pengusulanVerifikasi->finished && !$pengusulanValidasi->finished) {
                                $statusPengusulan = 'Pengusulan Sedang Di Tahap Validasi';
                            } else if ($pengusulanVerifikasi->finished && $pengusulanValidasi->finished && !$pengusulanPenetapanPencantuman->finished) {
                                $statusPengusulan = 'Pengusulan Sedang Di Tahap Penetapan dan Pencantuman';
                            } else if ($pengusulanVerifikasi->finished && $pengusulanValidasi->finished && $pengusulanPenetapanPencantuman->finished && !$pengusulanMonev->finished) {
                                $statusPengusulan = 'Pengusulan Sedang Di Tahap Monitoring, Evaluasi, dan Pengembangan';
                            } else if ($pengusulanVerifikasi->finished && $pengusulanValidasi->finished && $pengusulanPenetapanPencantuman->finished && $pengusulanMonev->finished) {
                                $statusPengusulan = 'Pengusulan Telah Selesai Ditangani';
                            }

                            $pengusulanViewModel = new PengusulanViewModel($pengusulan->kode_pengusulan, $pengusulan->nama_pengusul, $pengusulan->created_date, $statusPengusulan, $pengusulan->modified_date);

                            return $this->render('detail', ['model' => $pengusulanViewModel]);
                        } else {
                            $notFoundPengusulan = new PengusulanNotFoundViewModel($caseCode);

                            return $this->render('not-found', ['model' => $notFoundPengusulan]);
                        }
                    } else {
                        // Proceed with Pengaduan table
                        $pengaduan = PengaduanRegistrasi::findOne(['kode_pengaduan' => $caseCode, 'kunci_pengaduan' => sha1($caseKey)]);

                        if (isset($pengaduan)) {

                            $statusPengaduan = 'Unknown';
                            $pengaduanAssesmen = PengaduanAssesmen::findOne(['id_pengaduan' => $pengaduan->id]);
                            $pengaduanPraMediasi = PengaduanPraMediasi::findOne(['id_pengaduan' => $pengaduan->id]);
                            $pengaduanMediasi = PengaduanMediasi::findOne(['id_pengaduan' => $pengaduan->id]);
                            $pengaduanDraftingMou = PengaduanDraftingMou::findOne(['id_pengaduan' => $pengaduan->id]);
                            $pengaduanTandaTanganMou = PengaduanTandaTanganMou::findOne(['id_pengaduan' => $pengaduan->id]);

                            if (!$pengaduanAssesmen->finished) {
                                $statusPengaduan = 'Dalam Assesmen';
                            } else if ($pengaduanAssesmen->finished && !$pengaduanAssesmen->dialihkan && !$pengaduanPraMediasi->finished) {
                                $statusPengaduan = 'Dalam Pra Mediasi';
                            } else if ($pengaduanAssesmen->finished && $pengaduanAssesmen->dialihkan) {
                                $statusPengaduan = 'Kasus Dialihkan';
                            } else if ($pengaduanAssesmen->finished && !$pengaduanAssesmen->dialihkan && $pengaduanPraMediasi->finished) {
                                $statusPengaduan = 'Dalam Mediasi';
                            } else if ($pengaduanAssesmen->finished && !$pengaduanAssesmen->dialihkan && $pengaduanPraMediasi->finished && $pengaduanMediasi->finished) {
                                $statusPengaduan = 'Dalam Penyusunan MoU';
                            } else if ($pengaduanAssesmen->finished && !$pengaduanAssesmen->dialihkan && $pengaduanPraMediasi->finished && $pengaduanMediasi->finished && $pengaduanDraftingMou->finished) {
                                $statusPengaduan = 'Dalam Proses Penandatanganan MoU';
                            } else if ($pengaduanAssesmen->finished && !$pengaduanAssesmen->dialihkan && $pengaduanPraMediasi->finished && $pengaduanMediasi->finished && $pengaduanDraftingMou->finished && $pengaduanTandaTanganMou->finished) {
                                $statusPengaduan = 'Kasus Telah Selesai Ditangani';
                            }

                            $pengaduanViewModel = new PengaduanViewModel($pengaduan->kode_pengaduan, $pengaduan->nama_pengadu, $pengaduan->created_date, $statusPengaduan, $pengaduan->modified_date);
                            $pengaduanViewModel->kode_pengaduan = $pengaduan->kode_pengaduan;
                            $pengaduanViewModel->nama_pengadu = $pengaduan->nama_pengadu;
                            $pengaduanViewModel->tanggal_pengaduan = $pengaduan->created_date;
                            $pengaduanViewModel->status_pengaduan = $statusPengaduan;
                            $pengaduanViewModel->tanggal_status_pengaduan = $pengaduan->modified_date;

                            return $this->render('detail', ['model' => $pengaduanViewModel]);
                        } else {
                            $notFoundPengaduan = new PengaduanNotFoundViewModel();
                            $notFoundPengaduan->kode_pengaduan = $caseCode;

                            return $this->render('not-found', ['model' => $notFoundPengaduan]);
                        }
                    }
                } else {
                    return $this->render('not-found');
                }
            }
        }

        return $this->render('status-check', [
                    'model' => $model,
        ]);
    }

}
