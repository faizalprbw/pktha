<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%desa_kelurahan}}".
 *
 * @property string $id_kecamatan
 * @property string $nama_desa_kelurahan
 * @property string $id
 * @property double $latitude
 * @property double $longitude
 */
class DesaKelurahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%desa_kelurahan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kecamatan'], 'required'],
            [['id_kecamatan'], 'integer'],
            [['nama_desa_kelurahan'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['id_kecamatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['id_kecamatan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kecamatan' => Yii::t('app', 'Id Kecamatan'),
            'nama_desa_kelurahan' => Yii::t('app', 'Nama Desa/Kelurahan'),
            'id' => Yii::t('app', 'ID'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
        ];
    }

    /**
     * @inheritdoc
     * @return DesaKelurahanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DesaKelurahanQuery(get_called_class());
    }
}
