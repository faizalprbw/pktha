
var map = L.map('map', {
    zoomControl: false,
    maxZoom: 20,
    minZoom: 5
}).setView([0.39550467153201946, 116.89453125000001], 5);

map.setMaxBounds([[8.100892668623654, 94.13085937500001], [-10.34033830684679, 142.47070312500003]]);

var zoom_bar = new L.Control.ZoomBar({position: 'topleft'}).addTo(map);

var assesmen_icon = new L.Icon({
    iconUrl: 'images/map/assesmen.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var pra_mediasi_icon = new L.Icon({
    iconUrl: 'images/map/pra_mediasi.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var mediasi_icon = new L.Icon({
    iconUrl: 'images/map/mediasi.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var drafting_mou_icon = new L.Icon({
    iconUrl: 'images/map/drafting_mou.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var tanda_tangan_mou_icon = new L.Icon({
    iconUrl: 'images/map/tanda_tangan_mou.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var google_hybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_streetmap = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_terrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var google_satellite = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
});

var iconLayersControl = new L.Control.IconLayers(
        [
            {
                title: 'Google Streets',
                layer: google_streetmap,
                icon: 'images/map/Google_Street.png'
            },
            {
                title: 'Google Satellite',
                layer: google_satellite,
                icon: 'images/map/esri_Satellite.png'
            },
            {
                title: 'Google Hybrid',
                layer: google_hybrid,
                icon: 'images/map/google_hybrid2.png'
            },
            {
                title: 'Google Terrain',
                layer: google_terrain,
                icon: 'images/map/google_terrain.png'
            },
        ], {
    position: 'topright',
    maxLayersInRow: 5
}
);

iconLayersControl.addTo(map);

var mcg = L.markerClusterGroup().addTo(map);

var lokasiSub = L.featureGroup.subGroup(mcg).addTo(map);

var lokasi = L.geoJson(null, {

    pointToLayer: function (feature, latlng) {
        if (feature.properties.finished_assesmen === "f" && feature.properties.finished_pra_mediasi === "f" && feature.properties.finished_mediasi === "f" && feature.properties.finished_drafting_mou === "f" && feature.properties.finished_tanda_tangan_mou === "f") {
            return L.marker(latlng, {icon: assesmen_icon});
        }
        if (feature.properties.finished_assesmen === "t" && feature.properties.finished_pra_mediasi === "f" && feature.properties.finished_mediasi === "f" && feature.properties.finished_drafting_mou === "f" && feature.properties.finished_tanda_tangan_mou == "f") {
            return L.marker(latlng, {icon: pra_mediasi_icon});
        }
        if (feature.properties.finished_assesmen === "t" && feature.properties.finished_pra_mediasi === "t" && feature.properties.finished_mediasi === "f" && feature.properties.finished_drafting_mou === "f" && feature.properties.finished_tanda_tangan_mou === "f") {
            return L.marker(latlng, {icon: mediasi_icon});
        }
        if (feature.properties.finished_assesmen === "t" && feature.properties.finished_pra_mediasi === "t" && feature.properties.finished_mediasi === "t" && feature.properties.finished_drafting_mou === "f" && feature.properties.finished_tanda_tangan_mou === "f") {
            return L.marker(latlng, {icon: drafting_mou_icon});
        }
        if (feature.properties.finished_assesmen === "t" && feature.properties.finished_pra_mediasi === "t" && feature.properties.finished_mediasi === "t" && feature.properties.finished_drafting_mou === "t" && feature.properties.finished_tanda_tangan_mou === "f") {
            return L.marker(latlng, {icon: tanda_tangan_mou_icon});
        }
        if (feature.properties.finished_assesmen === "t" && feature.properties.finished_pra_mediasi === "t" && feature.properties.finished_mediasi === "t" && feature.properties.finished_drafting_mou === "t" && feature.properties.finished_tanda_tangan_mou === "t") {
            return L.marker(latlng, {icon: tanda_tangan_mou_icon});
        }
    },

    onEachFeature: function (feature, layer) {
        layer.bindPopup('<h4>Keterangan</h4>' +
                '<table style="font-size:95%" width="400">' +
                '<tr><td width="150">Lokasi Konflik</td><td>:</td><td width="250">' + feature.properties.nama_desa_kelurahan + ', ' + feature.properties.nama_kecamatan + ', ' + feature.properties.nama_kota_kabupaten + ', ' + feature.properties.nama_provinsi + ' </tr></td>' +
                '<tr><td width="150">Tipologi Konflik</td><td>:</td><td width="250">' + feature.properties.Tipologi_Konflik + '</tr></td>' +
                '<tr><td width="150">Pihak Berkonflik</td><td>:</td><td width="250">' + feature.properties.pihak_berkonflik_1 + '-' + feature.properties.pihak_berkonflik_2 + '-' + feature.properties.pihak_berkonflik_3 + ' </tr></td>' +
                '</table>', {maxWidth: 450});
    }
});

$.getJSON("http://localhost/PKTHA/pengaduan/backend/views/site/sebaran_konflik.php", function (data) {
    
    alert(JSON.stringify(data));
    lokasi.addData(data);
    lokasi.eachLayer(function (layer) {
        layer.addTo(lokasiSub);
    });
});

var kawasan_hutan = L.esri.tiledMapLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/KLHK/Kawasan_Hutan_0917/MapServer"
});

var hutan_adat = L.esri.featureLayer({
    url: 'http://geoportal.menlhk.go.id/arcgis/rest/services/test/Penetapan_Hutan_Adat2/FeatureServer/0'
});

hutan_adat.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Hutan Adat</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Kode Provinsi</td><td>:</td><td>{kode_prov}</tr></td>' +
            '<tr><td>Masyarakat Adat</td><td>:</td><td>{masy_adat}</tr></td>' +
            '<tr><td>Fungsi Kawasan</td><td>:</td><td>{fungsi_kws}</tr></td>' +
            '<tr><td>SK Adat</td><td>:</td><td>{sk_adat}</tr></td>' +
            '<tr><td>Tanggal Adat</td><td>:</td><td>{tgl_adat}</tr></td>' +
            '<tr><td>Luas Hutan Adat</td><td>:</td><td>{ls_adat}</tr></td>' +
            '<tr><td>Keternagan</td><td>:</td><td>{keterangan}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sebaran_konflik = L.layerGroup();
var jabalnus = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Jabalnus2/FeatureServer/0"
}).addTo(sebaran_konflik);

jabalnus.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var kalimantan = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Kalimantan2/FeatureServer/0"
}).addTo(sebaran_konflik);

kalimantan.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var maluku_papua = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Maluku_Papua2/FeatureServer/0"
}).addTo(sebaran_konflik);

maluku_papua.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sulawesi = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sulawesi2/FeatureServer/0"
}).addTo(sebaran_konflik);

sulawesi.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var sumatra = L.esri.featureLayer({
    url: "http://geoportal.menlhk.go.id/arcgis/rest/services/test/Potensi_Konflik_Sumatera2/FeatureServer/0"
}).addTo(sebaran_konflik);

sumatra.bindPopup(function (layer) {
    return L.Util.template('<h4>Keterangan Potensi Sebaran Konflik</h4>' +
            '<table style="font-size:100%" width="250">' +
            '<tr><td width="100">Keterangan</td><td>:</td><td>{keterangan}</tr></td>' +
            '<tr><td>Fungsi</td><td>:</td><td>{fungsi}</tr></td>' +
            '<tr><td>Kode Kawasan</td><td>:</td><td>{kd_kws}</tr></td>' +
            '<tr><td>Izin</td><td>:</td><td>{ijin}</tr></td>' +
            '</table>', layer.feature.properties);
});

var overlayLayers = {
    "Data Peta": {
        "Sebaran Konflik": mcg,
        "Penetapan Hutan Adat": hutan_adat,
        "Indikatif Potensi Konflik": sebaran_konflik,
        "Fungsi Kawasan Hutan": kawasan_hutan
    }
};

L.control.groupedLayers(null, overlayLayers).addTo(map);

// Only secured origins are allowed. Must be served through HTTPS, thus this function below will be functional.
// var lc = L.control.locate({
//     position: 'topleft',
//     icon: 'fa fa-map-marker fa-2x',
//     iconLoading: 'fa fa-spinner fa-spin fa-2x',
//     locateOptions: {
//         enableHighAccuracy: true
//     },
//     strings: {
//         title: "Cari Lokasi Anda Saat Ini",
//         popup: "Anda Berada Disekitar Sini",
//         outsideMapBoundsMsg: "Anda Berada di Luar Wilayah Negara Republik Indonesia"
//     }}).addTo(map);

var GeoSearchControl = window.GeoSearch.GeoSearchControl;
var GoogleProvider = window.GeoSearch.GoogleProvider;

var provider = new GoogleProvider({
    params: {
        key: 'AIzaSyDW1hC9P9SLkyaynoCE-mvJhfNRKYrZcK4',
    },
});

var searchControl = new GeoSearchControl({
    provider: provider,
    position: 'topright',
    showMarker: true,
    showPopup: true,
    marker: {
        icon: new L.Icon.Default(),
        draggable: true,
    },
    popupFormat: ({ query, result }) => result.label,
    maxMarkers: 1,
    retainZoomLevel: false,
    animateZoom: true,
    autoComplete: true,
    autoCompleteDelay: 150,
    autoClose: false,
    searchLabel: 'Cari Alamat',
    keepResult: false
}).addTo(map);

L.control.betterscale({
    imperial: false,
    metric: true
}).addTo(map);

var khLegend = L.control({position: 'bottomleft'});

map.on('overlayadd', function (eventLayer) {

    if (eventLayer.name === "Fungsi Kawasan Hutan") {

        khLegend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'info legend'),
                    labels = [];

            $.ajax({
                url: "http://geoportal.menlhk.go.id/arcgis/rest/services/KLHK/Kawasan_Hutan_0917/MapServer/legend",
                data: {f: 'pjson'},
                dataType: 'jsonp',
                success: function (data) {
                    if (typeof data.layers[0].legend !== 'undefined') {
                        for (var i = 0, len = data.layers.length; i < len; i++) {
                            div.innerHTML += '<strong style="font-size: 14px">' + data.layers[i].layerName + '</strong></br>';
                            for (var j = 0, jj = data.layers[0].legend.length; j < jj; j++) {
                                div.innerHTML += L.Util.template('<img width="{width}" height="{height}" src="data:{contentType};base64,{imageData}"><span>{label}</span></br>', data.layers[i].legend[j]);
                            }
                        }
                    } else {
                        div.innerHTML += '<strong style="font-size: 14px">' + "Tidak ada legenda" + '</strong></br>';
                    }
                }
            });
            return div;
        };
        khLegend.addTo(map);

    }

});

map.on('overlayremove', function (eventLayer) {

    if (eventLayer.name === "Fungsi Kawasan Hutan")
    {
        this.removeControl(khLegend);
    }

})