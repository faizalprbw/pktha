/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    function onKecamatanChanged() {
        if ($(this).val() === '0') {
            $('#pengusulanhutanadat-teks_kecamatan-container').removeClass('hidden');
        } else {
            if (!$('#pengusulanhutanadat-teks_kecamatan-container').hasClass('hidden')) {
                $('#pengusulanhutanadat-teks_kecamatan-container').addClass('hidden');
            }
        }

        $.post("/pktha/pengaduan/frontend/web/index.php?r=location/desa-kelurahan", {id_kecamatan: $(this).val()}, function (data) {
            $("#pengusulanhutanadat-id_desa_kelurahan").html(data);

            if ($("#pengusulanhutanadat-id_desa_kelurahan").val() === '0') {
                $('#pengusulanhutanadat-teks_desa_kelurahan-container').removeClass('hidden');
            } else {
                if (!$('#pengusulanhutanadat-teks_desa_kelurahan-container').hasClass('hidden')) {
                    $('#pengusulanhutanadat-teks_desa_kelurahan-container').addClass('hidden');
                }
            }
        });
    }

    function onKotaKabupatenChanged() {
        if ($(this).val() === '0') {
            $('#pengusulanhutanadat-teks_kota_kabupaten-container').removeClass('hidden');
        } else {
            if (!$('#pengusulanhutanadat-teks_kota_kabupaten-container').hasClass('hidden')) {
                $('#pengusulanhutanadat-teks_kota_kabupaten-container').addClass('hidden');
            }
        }

        $.post("/pktha/pengaduan/frontend/web/index.php?r=location/kecamatan", {id_kota_kabupaten: $(this).val()}, function (data) {
            $("#pengusulanhutanadat-id_kecamatan").html(data)

            if ($("#pengusulanhutanadat-id_kecamatan").val() === '0') {
                $('#pengusulanhutanadat-teks_kecamatan-container').removeClass('hidden');
            } else {
                if (!$('#pengusulanhutanadat-teks_kecamatan-container').hasClass('hidden')) {
                    $('#pengusulanhutanadat-teks_kecamatan-container').addClass('hidden');
                }
            }

            $.post("/pktha/pengaduan/frontend/web/index.php?r=location/desa-kelurahan", {id_kecamatan: $("#pengusulanhutanadat-id_kecamatan").val()}, function (data) {
                $("#pengusulanhutanadat-id_desa_kelurahan").html(data);

                if ($("#pengusulanhutanadat-id_desa_kelurahan").val() === '0') {
                    $('#pengusulanhutanadat-teks_desa_kelurahan-container').removeClass('hidden');
                } else {
                    if (!$('#pengusulanhutanadat-teks_desa_kelurahan-container').hasClass('hidden')) {
                        $('#pengusulanhutanadat-teks_desa_kelurahan-container').addClass('hidden');
                    }
                }
            });
        });
    }

    function onProvinsiChanged() {
        $.post("/pktha/pengaduan/frontend/web/index.php?r=location/kota-kabupaten", {id_provinsi: $(this).val()}, function (data) {
            $("#pengusulanhutanadat-id_kota_kabupaten").html(data);

            if ($("#pengusulanhutanadat-id_kota_kabupaten").val() === '0') {
                $("#pengusulanhutanadat-teks_kota_kabupaten-container").removeClass('hidden');
            } else {
                if (!$("#pengusulanhutanadat-teks_kota_kabupaten-container").hasClass('hidden')) {
                    $("#pengusulanhutanadat-teks_kota_kabupaten-container").addClass('hidden');
                }
            }

            $.post("/pktha/pengaduan/frontend/web/index.php?r=location/kecamatan", {id_kota_kabupaten: $("#pengusulanhutanadat-id_kota_kabupaten").val()}, function (data) {
                $("#pengusulanhutanadat-id_kecamatan").html(data);

                if ($("#pengusulanhutanadat-id_kecamatan").val() === '0') {
                    $('#pengusulanhutanadat-teks_kecamatan-container').removeClass('hidden');
                } else {
                    if (!$('#pengusulanhutanadat-teks_kecamatan-container').hasClass('hidden')) {
                        $('#pengusulanhutanadat-teks_kecamatan-container').addClass('hidden');
                    }
                }

                $.post("/pktha/pengaduan/frontend/web/index.php?r=location/desa-kelurahan", {id_kecamatan: $("#pengusulanhutanadat-id_kecamatan").val()}, function (data) {
                    $("#pengusulanhutanadat-id_desa_kelurahan").html(data);

                    if ($("#pengusulanhutanadat-id_desa_kelurahan").val() === '0') {
                        $('#pengusulanhutanadat-teks_desa_kelurahan-container').removeClass('hidden');
                    } else {
                        if (!$('#pengusulanhutanadat-teks_desa_kelurahan-container').hasClass('hidden')) {
                            $('#pengusulanhutanadat-teks_desa_kelurahan-container').addClass('hidden');
                        }
                    }
                });
            });
        });
    }

    function onWilayahChanged() {
        $.post("/pktha/pengaduan/frontend/web/index.php?r=location/provinsi", {id_wilayah: $(this).val()}, function (data) {
            $("#pengusulanhutanadat-id_provinsi").html(data);

            $.post("/pktha/pengaduan/frontend/web/index.php?r=location/kota-kabupaten", {id_provinsi: $("#pengusulanhutanadat-id_provinsi").val()}, function (data) {
                $("#pengusulanhutanadat-id_kota_kabupaten").html(data);

                if ($("#pengusulanhutanadat-id_kota_kabupaten").val() === '0') {
                    $("#pengusulanhutanadat-teks_kota_kabupaten-container").removeClass('hidden');
                } else {
                    if (!$("#pengusulanhutanadat-teks_kota_kabupaten-container").hasClass('hidden')) {
                        $("#pengusulanhutanadat-teks_kota_kabupaten-container").addClass('hidden');
                    }
                }

                $.post("/pktha/pengaduan/frontend/web/index.php?r=location/kecamatan", {id_kota_kabupaten: $("#pengusulanhutanadat-id_kota_kabupaten").val()}, function (data) {
                    $("#pengusulanhutanadat-id_kecamatan").html(data);

                    if ($("#pengusulanhutanadat-id_kecamatan").val() === '0') {
                        $('#pengusulanhutanadat-teks_kecamatan-container').removeClass('hidden');
                    } else {
                        if (!$('#pengusulanhutanadat-teks_kecamatan-container').hasClass('hidden')) {
                            $('#pengusulanhutanadat-teks_kecamatan-container').addClass('hidden');
                        }
                    }

                    $.post("/pktha/pengaduan/frontend/web/index.php?r=location/desa-kelurahan", {id_kecamatan: $("#pengusulanhutanadat-id_kecamatan").val()}, function (data) {
                        $("#pengusulanhutanadat-id_desa_kelurahan").html(data);

                        if ($("#pengusulanhutanadat-id_desa_kelurahan").val() === '0') {
                            $('#pengusulanhutanadat-teks_desa_kelurahan-container').removeClass('hidden');
                        } else {
                            if (!$('#pengusulanhutanadat-teks_desa_kelurahan-container').hasClass('hidden')) {
                                $('#pengusulanhutanadat-teks_desa_kelurahan-container').addClass('hidden');
                            }
                        }
                    });
                });
            });
        });
    }

    /**
     *  $('#pengusulanhutanadat-id_wilayah').on('change', onWilayahChanged);
     $('#pengusulanhutanadat-id_provinsi').on('change', onProvinsiChanged);
     $('#pengusulanhutanadat-id_kota_kabupaten').on('change', onKotaKabupatenChanged);
     $('#pengusulanhutanadat-id_kecamatan').on('change', onKecamatanChanged);
     */

});

